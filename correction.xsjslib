$.import('timp.core.server.api', 'api');
const coreApi = $.timp.core.server.api.api;
const _ = coreApi.lodash;
const util = coreApi.util;
$.import('timp.bsc.server.models.tables', 'correction');
const correctionModel = $.timp.bsc.server.models.tables.correction.correctionModel;
const correctionEditModel = $.timp.bsc.server.models.tables.correction.correctionEditModel;
const fieldByCorrectionModel = $.timp.bsc.server.models.tables.correction.fieldByCorrectionModel;
const messageByCorrectionModel = $.timp.bsc.server.models.tables.correction.messageByCorrectionModel;
const commentByCorrectionModel = $.timp.bsc.server.models.tables.correction.commentByCorrectionModel;
const linkByCorrectionModel = $.timp.bsc.server.models.tables.correction.linkByCorrectionModel;
const headerFieldModel = $.timp.bsc.server.models.tables.correction.headerFieldModel;
const accountingFieldModel = $.timp.bsc.server.models.tables.correction.accountingFieldModel;
const itemGroupModel = $.timp.bsc.server.models.tables.correction.itemGroupModel;
const itemFieldModel = $.timp.bsc.server.models.tables.correction.itemFieldModel;
const variantModel = $.timp.bsc.server.models.tables.correction.variantModel;
$.import('timp.bsc.server.models.views', 'cvCorrectionFieldsConfiguration');
const cvCorrectionFieldsConfigurationModel = $.timp.bsc.server.models.views.cvCorrectionFieldsConfiguration.cvCorrectionFieldsConfigurationModel;
$.import('timp.bsc.server.controllers', 'LogSupervisor');
var Supervisor = $.timp.bsc.server.controllers.LogSupervisor.Supervisor;
$.import('timp.bsc.server.models.views', 'cvCorrection');
const cvCorrection = $.timp.bsc.server.models.views.cvCorrection.cvCorrection;
const _this = this;
const taxFields = [
    'VL_BC_PREV', 'VL_BC_ISSQN', 'VL_BC_PIS', 'VL_BC_ICMS', 'VL_BC_ICMSST', 'VL_BC_COFINS', 'VL_BC_ICMS_DIFAL', 'VL_BC_IRRF',
    'VL_BC_IPI', 'VL_BC_ICMS_ZF', 'VL_BC_PIS_OIL', 'VL_BC_COFINS_OIL',
    'VL_BC_OUTRAS_ICMSST', 'VL_BC_OUTRAS_ICMS', 'VL_BC_OUTRAS_IPI', 'VL_BC_OUTRAS_ICMS_DIFAL', 'VL_OUTRAS_ICMS', 'VL_OUTRAS_ICMS_ST',
    'VL_OUTRAS_DIFAL', 'VL_OUTRAS_IPI', 'VL_CONT_ICMS', 'VL_CONT_ICMSST', 'VL_CONT_DIFAL', 'VL_CONT_IPI',
    'VL_BC_EXC_ICMS', 'VL_BC_EXC_ICMSST', 'VL_BC_EXC_IPI', 'VL_BC_EXC_ICMS_DIFAL',
    'SYS_DIR_COFINS', 'SYS_DIR_ICMS', 'SYS_DIR_IPI', 'SYS_DIR_ISS', 'SYS_DIR_PIS',
    'VL_PREV', 'VL_ICMS', 'VL_ICMSST', 'VL_IRRF', 'VL_ISSQN', 'VL_PIS_RET', 'VL_PISST', 'VL_COFINS', 'VL_COFINS_RET', 'VL_COFINSST',
    'VL_ICMS_DIFAL', 'VL_IPI', 'VL_PIS', 'VL_ICMS_ZF', 'VL_PIS_OIL', 'VL_COFINS_OIL', 'VL_ICMS_ORI', 'VL_ICMS_DES', 'VL_ICMS_FUN',
    'ALIQ_ICMS_DIFAL', 'ALIQ_ICMS', 'ALIQ_ICMSST', 'ALIQ_COFINS', 'ALIQ_PIS', 'ALIQ_IPI', 'ALIQ_ISSQN', 'ALIQ_ICMS_ZF',
    'ALIQ_PIS_OIL', 'ALIQ_COFINS_OIL', 'ALIQ_ICMS_AJUSTADA'
];

this.ppids = new Map();
this.lvdocumentType = new Map();
this.lvcorrectionHist = new Map();
this.canApproveByJobs = new Map();

const CORRECTION_STRUCTURE = 'Nota Fiscal (Itens com Impostos)';
const CORRECTION_VIEW = 'timp.atr.modeling.ctr.nota_fiscal.item_com_imposto/CTR_NF';
const GROUP_IMPOSTO_VIEW = 'timp.bsc.modeling/CV_J_1BNFSTX';
const MAP_FIELD_TABLE = 'BSC::CORRECTION_FIELD_MAPPING';
const REFERENCE_VIEW = 'timp.bsc.modeling/CV_REFERENCE_FIELDS';
//const CORRECTION_STRUCTURE_OLD = 'Nota Fiscal (Itens com Impostos)';
const CORRECTION_VIEW_OLD = 'timp.atr.modeling.ctr.nota_fiscal.item_com_imposto/CTR_NF';
const SAP_NF_VIEW = 'sap.glo.tmflocbr.ctr/PRV_NF_IMPOSTO';
let NUMBER_OF_CORRECTIONS_PER_JOB = 50;
let NUMBER_OF_ACCOUNTINGS_PER_JOB = 10;

/* 
    Usage: this function receives an object like
    object = [{
        scenarioHierarchyId: 1 (required),
        nfId: "0000004300" (required),
        numItem: "000010" (required),
        reason: "" (required),
        justification: "" (optional),
        job: 1 or 0 (optional),
        fields:[{
            fieldName:"CST_ICMS" (required (hanaName)),
            oldValue: "" (required),
            newValue: "" (required)
        }] (optional),
        correctionType: 0 (required),
        headerFields: [{
            fieldName: "", (required)
            value: "" (required)
        }] (optional, depends on correctionType),
        itemGroups: [
            [{
                fieldName: "",
                value: ""
            },{
                fieldName: "",
                value: ""
            },{
                fieldName: "",
                value: ""
            }],[{
                fieldName: "",
                value: ""
            }],[{
                fieldName: "",
                value: ""
            }]
        ] (optional, depends on correctionType)
    },...]
    returns the created instance + the createdFields
*/
this.create = function(object) {
	var response;
	var simulateJobRetention = false;
	var existingCorrections = [];
	try {
		var creationUser = $.getUserID();
		let accounting = [];
		if (_.isArray(object)) {
			response = {
				created: []
			};
			// response = [];
			_.forEach(object, function(correction) {
				//check if there is another correction with the same rule
				if (correction.accountingDirectly) {
					correction.correctionType = 4;
					correction.isCorrection = 0;
				}
				if (correction.correctionType === 4) {
					correction.nfId = correction.idDocCont || correction.matricula || correction.accountReference || '';
					correction.year = correction.anoExercicio || (correction.dtComp && correction.dtComp.substring(2)) || correction.year || '';
					correction.company = correction.codEmp || correction.company || '';
					correction.numItem = correction.idItem || correction.accountReferenceDescription || '';
				}
				if (correction.idDocumento && correction.idItem) {
					correction.nfId = correction.idDocumento;
					correction.numItem = correction.idItem;
				}

				if (correction.account && correction.idStructure == 247 && correction.accountingDirectly) {
					correction.nfId = correction.account;
					correction.numItem = '001';
				}
				if (correction.id) {
					let referenceId = Number(correction.id.replace(/\./g, '').replace(/,/g, ''));
					correction.referenceId = referenceId;
				}
				var where = [{
					field: 'nfId',
					oper: '=',
					value: correction.nfId
                }, {
					field: 'numItem',
					oper: '=',
					value: correction.numItem
                }, {
					field: 'rulePath',
					oper: '=',
					value: correction.rulePath
                }, {
					field: 'status',
					oper: '=',
					value: [1, 5]
                }];
				let join = [];
				if (correction.fields && correction.fields.length && Number(correction.idStructure) === 301 && _.has(correction, 'accounting')) {
					let joinStm = {
						table: fieldByCorrectionModel,
						alias: 'fields',
						on: [{
							left: 'id',
							right: 'correctionId'
                        }]
					};
					let onStm = [];
					_.forEach(correction.fields, function(field) {
						onStm.push([{
							field: 'fieldName',
							oper: '=',
							value: field.fieldName
                        }, {
							field: 'oldValue',
							oper: '=',
							value: field.oldValue
                        }, {
							field: 'newValue',
							oper: '=',
							value: field.newValue
                        }]);
					});
					if (onStm.length) {
						joinStm.on.push(onStm);
					}
					join.push(joinStm);
				}
				let existing = correctionModel.READ({
					where: where,
					join: join
				});
				// if there is this correction cannot be created
				if (existing.length > 0) {
					existingCorrections.push(correction);
					return;
				}
				correction.creationUser = creationUser;
				correction.creationDate = new Date();
				correction.modificationUser = creationUser;
				correction.modificationDate = new Date();
				correction.isCorrection = correction.isCorrection || 0;
				correction.wasCorrected = correction.wasCorrected || 0;
				if (correction.job) {
					if (correction.correctionType === 5) {
						simulateJobRetention = true;
						correction.status = 1;
					} else {
						correction.pendingJob = 1;
						correction.status = 5;
					}
				}
				if (_.has(correction, 'accounting') && ((_.has(correction.accounting, 'headerFields') && correction.accounting.headerFields.length) ||
					(_.has(correction.accounting, 'itemFields') && correction.accounting.itemFields.length))) {
					if (correction.correctionType === 5) {
						accounting = correction.accounting;
						correction.accounting = 'R';
					} else {
						accounting = correction.accounting;
						correction.accounting = 'R';
						correction.correctionType = 1;
					}
				} else {
					correction.accounting = '';
				}
				if (correction.accountingDirectly) {
					correction.correctionType = 4;
				}
				var created = correctionModel.CREATE(correction);
				if (created && _.has(correction, 'fields')) {
					created.createdFields = _this.createFieldsByCorrection(created.id, correction.fields);
				}
				if (created && _.has(correction, 'headerFields')) {
					created.createdHeaderFields = _this.createHeaderFields(created.id, correction.headerFields);
				}
				if (created && _.has(correction, 'itemGroups')) {
					created.itemGroups = _this.createItemGroups(created.id, correction.itemGroups);
				}
				if (created && !_.isNil(accounting)) {
					let accountingItems = [];
					if (!_.isNil(accounting.fields) && _.isArray(accounting.fields)) {
						if (_.isNil(accounting.headerFields)) {
							accounting.headerFields = [];
						}
						accounting.headerFields = _.concat(accounting.headerFields, accounting.fields);
					}
					_.forEach(['headerFields', 'itemFields'], item => {
						if (_.has(accounting, item) && !_.isEmpty(accounting[item])) {
							_.forEach(accounting[item], (field, key) => {
								accountingItems.push({
									fieldName: field && !_.isNil(field.field) ? field.field : key,
									value: field && !_.isNil(field.value) ? field.value : field,
									group: field && !_.isNil(field.group) ? field.group : null,
									isHeader: item === 'headerFields' ? 1 : 0,
									useReference: field && !_.isNil(field.useReference) ? field.useReference : 0
								});
							});
						}
					});
					created.accountingFields = _this.createAccountingFields(created.id, accountingItems);
				}
				response.created.push(created);
				//log action
				Supervisor.createCorrection(created);
			});
		} else if (_.isPlainObject(object)) {
			object.creationUser = creationUser;
			object.creationDate = new Date();
			object.modificationUser = creationUser;
			object.modificationDate = new Date();
			object.isCorrection = object.isCorrection || 0;
			object.wasCorrected = object.wasCorrected || 0;
			// if (_.has(correction, 'accounting')) {
			//     accounting = correction.accounting;
			//     correction.accounting = 'R';
			// }
			response = correctionModel.CREATE(object);
			if (response && _.has(object, 'fields')) {
				response.createdFields = _this.createFieldsByCorrection(response.id, object.fields);
				//log action
				Supervisor.createCorrection(response);
			}
			if (response && _.has(object, 'headerFields')) {
				response.createdHeaderFields = _this.createHeaderFields(response.id, object.headerFields);
			}
			if (response && _.has(object, 'itemGroups')) {
				response.itemGroups = _this.createItemGroups(response.id, object.itemGroups);
			}
			// if (response && !_.isNil(accounting) && !_.isEmpty(accounting)) {
			//     let accountingItems = [];
			//     _.forEach(['headerFields', 'itemFields'], item => {
			//         if (_.has(accounting, item) && !_.isEmpty(accounting[item])) {
			//             _.forEach(accounting[item], (field, key) => {
			//                 accountingItems.push({
			//                     fieldName: field && field.field ? field.field : key,
			//                     value: field && field.value ? field.value : field,
			//                     group: field && field.group ? field.group : null,
			//                     isHeader: item === 'headerFields' ? 1 : 0
			//                 });
			//             });
			//         }
			//     });
			//     response.accountingFields = _this.createAccountingFields(response.id, accountingItems);
			// }
		}
		response.existingCorrections = existingCorrections;
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201062',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		//log error
		Supervisor.createCorrectionError(util.parseError(e));
	}
	if (simulateJobRetention) {
		if (response && response.created && response.created.length) {
			let objToApproveRetention = {
				id: [Number(response.created[0].id)],
				filters: {},
				status: 'PENDING'
			};
			let responseCorrection = this.approve(objToApproveRetention);
			response.approveAutomatic = responseCorrection;
		}
	}
	return response;
};

/*
    Usage: this function receives an id(number) and an object like
    {
        fields:[{
            fieldName:"CST_ICMS" (required (hanaName)),
            oldValue: "" (required),
            newValue: "" (required)
        }] (required not empty)
    }
    returrns an array of the created instances
*/
this.createFieldsByCorrection = function(correctionId, fields) {
	var response = [];
	try {
		if (_.isArray(fields) && !_.isNil(correctionId)) {
			_.forEach(fields, function(field) {
				var created = fieldByCorrectionModel.CREATE({
					correctionId: correctionId,
					fieldName: field.fieldName,
					oldValue: field.oldValue,
					newValue: field.newValue
				});
				if (created) {
					response.push(created);
				}
			});
		}
	} catch (e) {
		$.trace.error(e);
	}
	return response;
};

/*
    Usage: this function receives an id(number) and an array like
    headerFields = [{
        fieldName: "" (required),
        value: "" (required)
    }] (required not empty)
    returrns an array of the created instances
*/
this.createHeaderFields = function(correctionId, headerFields) {
	var response = [];
	try {
		if (_.isArray(headerFields) && !_.isNil(correctionId)) {
			_.forEach(headerFields, function(headerField) {
				var value = headerField.value;
				if ((['PSTNG_DATE', 'DOC_DATE', 'POST_DATE'].indexOf(headerField.fieldName) !== -1) && $.lodash.isString(value) && value.match(/\//g)) {
					var m = $.moment(value, 'DD/MM/YYYY');
					if (m.isValid()) {
						value = m.format('YYYYMMDD');
					}
				}
				var created = headerFieldModel.CREATE({
					correctionId: correctionId,
					fieldName: headerField.fieldName,
					value: value
				});
				if (created) {
					response.push(created);
				}
			});
		}
	} catch (e) {
		$.trace.error(e);
	}
	return response;
};

/*
    Usage: this function receives an id(number) and an array like
    itemGroups = [[{
        fieldName: "" (required),
        value: "" (required)
    },{
        fieldName: "" (required),
        value: "" (required)
    }],[{
        fieldName: "" (required),
        value: "" (required)
    },{
        fieldName: "" (required),
        value: "" (required)
    }],[{
        fieldName: "" (required),
        value: "" (required)
    },{
        fieldName: "" (required),
        value: "" (required)
    }]] (required not empty)
    returrns an array of the created instances
*/
this.createItemGroups = function(correctionId, itemGroups) {
	var response = [];
	try {
		if (_.isArray(itemGroups) && !_.isNil(correctionId)) {
			_.forEach(itemGroups, function(itemGroup, position) {
				var createdItemGroup = itemGroupModel.CREATE({
					correctionId: correctionId,
					position: position
				});
				createdItemGroup.itemFields = [];
				_.forEach(itemGroup, function(item) {
					var created = itemFieldModel.CREATE({
						itemGroupId: createdItemGroup.id,
						fieldName: item.fieldName,
						value: item.value
					});
					if (created) {
						createdItemGroup.itemFields.push(created);
					}
				});
				response.push(createdItemGroup);
			});
		}
	} catch (e) {
		$.trace.error(e);
	}
	return response;
};

/*
    Usage: this function receives an id(number) and an array like
    accountingFields = [{
        fieldName: "" (required),
        value: "" (required),
        isHeader: true
    },{
        fieldName: "" (required),
        value: "" (required),
        isHeader: false
    }] (required not empty)
    returrns an array of the created instances
*/
this.createAccountingFields = function(correctionId, accountingFields) {
	var response = [];
	try {
		if (_.isArray(accountingFields) && !_.isNil(correctionId)) {
			_.forEach(accountingFields, function(accountingField) {
				var value = accountingField.value;
				if ((['PSTNG_DATE', 'DOC_DATE', 'POST_DATE'].indexOf(accountingField.fieldName) !== -1) && $.lodash.isString(value) && value.match(
					/\//g)) {
					var m = $.moment(value, 'DD/MM/YYYY');
					if (m.isValid()) {
						value = m.format('YYYYMMDD');
					}
				}
				var created = accountingFieldModel.CREATE({
					correctionId: correctionId,
					fieldName: accountingField.fieldName,
					value: value,
					isHeader: accountingField.isHeader,
					group: accountingField.group,
					useReference: accountingField.useReference
				});
				if (created) {
					response.push(created);
				}
			});
		}
	} catch (e) {
		$.trace.error(e);
	}
	return response;
};

/*
Usage: this function receives an object like
{
    id: <integer> (required),
    fields:[{id:<integer>(required),newValue:<string>(optional)}] (optional),
    headerFields:[{id:<integer>(required),value:<string>(optional)}] (optional),
    itemGroups:[
        [{id:<integer(required),value:<string>(optional)>}],
        [{id:<integer(required),value:<string>(optional)>}]
    ](optional)
}
*/
this.update = function(object) {
	var response = {
		updated: false,
		resultAccountFields: []
	};
	try {
		if (object && _.isPlainObject(object)) {
			if (_.has(object, 'id')) {
				var oldObject = this.read({
					id: object.id
				});
				var modificationUser = $.getUserID();
				var modificationDate = new Date();
				correctionModel.UPDATE({
					id: object.id,
					modificationUser: modificationUser,
					modificationDate: modificationDate
				});
				_.forEach(object.fields, function(field) {
					var fieldObj = {
						id: field.id,
						newValue: field.newValue
					};
					if (!_.isNil(field.oldValue)) {
						fieldObj.oldValue = field.oldValue;
					}
					response.updated = fieldByCorrectionModel.UPDATE(fieldObj);
				});
				_.forEach(object.headerFields, function(headerField) {
					var fieldObj = {
						id: headerField.id,
						value: headerField.value
					};
					headerFieldModel.UPDATE(fieldObj);
				});
				_.forEach(object.itemFields, function(itemField) {
					var fieldObj = {
						id: itemField.id,
						value: itemField.value
					};
					itemFieldModel.UPDATE(fieldObj);
				});
				if (_.has(object, 'comment')) {
					commentByCorrectionModel.DELETEWHERE([{
						field: 'correctionId',
						oper: '=',
						value: object.id
                    }]);
					this.createComments([object.comment], object.id);
				}
				if (_.has(object, 'link')) {
					linkByCorrectionModel.DELETEWHERE([{
						field: 'correctionId',
						oper: '=',
						value: object.id
                    }]);
					this.createLinks([object.link], object.id);
				}
				//update accountingFieldModel
				_.forEach(object.accountFields, function(accountField) {
					var obj = {
						id: accountField.id,
						value: accountField.value
					};
					response.resultAccountFields.push(accountingFieldModel.UPDATE(obj));
				});

				var newObject = this.read({
					id: object.id
				});
				Supervisor.updateCorrection(oldObject, newObject);
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201064',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		//log error
		Supervisor.updateCorrectionError(object && object.id ? object.id : null, util.parseError(e));
	}
	return response;
};

/*
    Usage: this function receives an object like
    {
        id:<number>(required)
    }
*/
this.read = function(object) {
	var retVal;
	var _self = this;
	try {
		//execute function
		var where = [];
		if (_.isPlainObject) {
			where.push({
				field: 'id',
				oper: '=',
				value: object.id
			});
		}

		let results = correctionModel.READ({
			where: where,
			join: _this.getJoin(true, true)
		});

		if (!_.isNil(results)) {
			let values = _.map(results, function(response) {
				var commentMap = _self.getCommentMap([response.id]);
				var linkMap = _self.getLinkMap([response.id]);
				let accountingData = response.accountingMappingForCorrections && response.accountingMappingForCorrections[0] || response.accountingMappingForCorrections || {};
				delete response.accountingMappingForCorrections;
				response.accountingData = {};
				let resp = _self.setAccountingFieldsValues(response.accountingData, response.accountingFields || [], response);
				response.accountingData = resp.accountingMapping;
				response.comments = commentMap[response.id] || [];
				response.links = linkMap[response.id] || [];
				response.MDRAccountingFields = _self.getMDRAccountingFields(response.MDRAccountingFields);
				accountingData.creditValue = response.accountingData.creditValue || '';
				accountingData.creditCoin = response.accountingData.creditCoin || '';
				accountingData.debitValue = response.accountingData.debitValue || '';
				accountingData.debitCoin = response.accountingData.debitCoin || '';
				accountingData.creditCompany = response.accountingData.creditCompany || '';
				accountingData.debitCompany = response.accountingData.debitCompany || '';
				accountingData.creditBranch = response.accountingData.creditBranch || '';
				accountingData.debitBranch = response.accountingData.debitBranch || '';
				accountingData.creditDocumentType = response.accountingData.creditDocumentType || '';
				accountingData.debitDocumentType = response.accountingData.debitDocumentType || '';
				accountingData.creditReleaseDate = response.accountingData.creditReleaseDate || '';
				accountingData.debitReleaseDate = response.accountingData.debitReleaseDate || '';
				accountingData.debitAccount = response.accountingData.debitAccount || '';
				accountingData.creditAccount = response.accountingData.creditAccount || '';
				accountingData.creditProfitCenter = response.accountingData.creditProfitCenter || '';
				accountingData.creditCostCenter = response.accountingData.creditCostCenter || response.accountingData.creditCostCenterCredit ||
					response
					.accountingData.costCenterCredit || '';
				accountingData.creditInternalOrder = response.accountingData.creditInternalOrder || '';
				accountingData.creditPepElement = response.accountingData.creditPepElement || '';
				accountingData.creditNetworkDiagram = response.accountingData.creditNetDiagram || response.accountingData.creditNetworkDiagram || '';
				accountingData.creditOperation = response.accountingData.creditOperation || '';
				accountingData.creditBranch = response.accountingData.creditBranch || response.accountingData.creditBranchDetail || '';
				accountingData.creditAttribute = response.accountingData.creditArticle || response.accountingData.creditAttribute || '';
				accountingData.debitProfitCenter = response.accountingData.debitProfitCenter || '';
				accountingData.debitCostCenter = response.accountingData.debitCostCenter || response.accountingData.debitCostCenterDebit || response.accountingData
					.costCenterDebit || '';
				accountingData.debitInternalOrder = response.accountingData.debitInternalOrder || '';
				accountingData.debitPepElement = response.accountingData.debitPepElement || '';
				accountingData.debitNetworkDiagram = response.accountingData.debitNetDiagram || response.accountingData.debitNetworkDiagram || '';
				accountingData.debitOperation = response.accountingData.debitOperation || '';
				accountingData.debitBranch = response.accountingData.debitBranch || response.accountingData.debitBranchDetail || '';
				accountingData.debitAttribute = response.accountingData.debitArticle || response.accountingData.debitAttribute || '';
				response.accountingData = _self.setEditCorrectionData(response.accountingData, accountingData);

				let editData = response.editData[0] || response.editData || {};
				if (editData && editData.correctionId) {
					response.accountingData = _self.setEditCorrectionData(response.accountingData, editData);
				}
				return response;
			});
			retVal = Array.isArray(object.id) ? values : values[0];
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201063',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return retVal;
};

this.setAccountingFieldsValues = function(accountingMapping, accountingFields, item) {
	let debitGroup = 0;
	let findCompany = true;
	let findBranch = true;
	let findReleaseDate = true;
	let findDocumentType = true;
	for (let i = 0; i < accountingFields.length; i++) {
		let actualItem = accountingFields[i];
		if (actualItem && actualItem.fieldName === 'DEBIT_ACCOUNT_AMOUNT') {
			debitGroup = actualItem.group;
			accountingMapping.debitValue = coreApi.legacyUtil.currencyToDecimalFormat(actualItem.value || '');
		} else if (actualItem && actualItem.fieldName === 'CREDIT_ACCOUNT_AMOUNT') {
			accountingMapping.creditValue = coreApi.legacyUtil.currencyToDecimalFormat(actualItem.value || '');
		} else if (actualItem && actualItem.fieldName === 'COIN') {
			accountingMapping.creditCoin = actualItem.value || '';
			accountingMapping.debitCoin = actualItem.value || '';
		} else if (actualItem && ['BRANCH', 'COMPANY'].indexOf(actualItem.fieldName) !== -1) {
			if (actualItem.fieldName === 'COMPANY' && actualItem.value) {
				accountingMapping.creditCompany = actualItem.value;
				accountingMapping.debitCompany = actualItem.value;
				findCompany = false;
			}
			if (actualItem.fieldName === 'BRANCH' && actualItem.value) {
				accountingMapping.creditBranch = actualItem.value;
				accountingMapping.debitBranch = actualItem.value;
				findBranch = false;
			}
		} else if (actualItem && actualItem.fieldName === 'ACCOUNT_DOCUMENT') {
			accountingMapping.creditDocumentType = actualItem.value || '';
			accountingMapping.debitDocumentType = actualItem.value || '';
			findDocumentType = !accountingMapping.creditDocumentType;
		} else if (actualItem && actualItem.fieldName === 'POST_DATE') {
			let date = this.getMomentDate(actualItem.value);
			accountingMapping.creditReleaseDate = date.isValid() && date.format('YYYYMMDD') || '';
			accountingMapping.debitReleaseDate = date.isValid() && date.format('YYYYMMDD') || '';
			findReleaseDate = !date.isValid();
		} else if (actualItem && actualItem.fieldName === "CREDIT_ACCOUNT") {
			accountingMapping.creditAccount = actualItem.value;
		} else if (actualItem && actualItem.fieldName === "DEBIT_ACCOUNT") {
			accountingMapping.debitAccount = actualItem.value;
		} else if (actualItem && actualItem.fieldName === "PROFIT_CENTER") {
			if (actualItem.group === debitGroup) {
				accountingMapping.debitProfitCenter = actualItem.value;
			} else {
				accountingMapping.creditProfitCenter = actualItem.value;
			}
		} else if (actualItem && actualItem.fieldName === "COST_CENTER") {
			if (actualItem.group === debitGroup) {
				accountingMapping.debitCostCenter = actualItem.value;
			} else {
				accountingMapping.creditCostCenter = actualItem.value;
			}
		}
		if (actualItem && actualItem.fieldName === 'PSTNG_DATE') {
			let date = this.getMomentDate(actualItem.value);
			accountingMapping.creditReleaseDate = date.isValid() && date.format('YYYYMMDD') || '';
			accountingMapping.debitReleaseDate = date.isValid() && date.format('YYYYMMDD') || '';
			findReleaseDate = !date.isValid();
		} else if (actualItem && actualItem.fieldName === 'YEAR') {

		} else if (actualItem && actualItem.fieldName === 'MONTH') {

		} else if (actualItem && actualItem.fieldName === 'HEADER_TXT') {

		} else if (actualItem && actualItem.fieldName === 'CURRENCY') {
			accountingMapping.creditCoin = actualItem.value || '';
			accountingMapping.debitCoin = actualItem.value || '';
		} else if (actualItem && actualItem.fieldName === 'GL_ACCOUNT') {
			accountingMapping.creditAccount = actualItem.value || '';
			accountingMapping.debitAccount = actualItem.value || '';
		} else if (actualItem && actualItem.fieldName === 'CREDIT_ACCOUNT') {
			accountingMapping.creditAccount = actualItem.value || '';
		} else if (actualItem && actualItem.fieldName === 'DEBIT_ACCOUNT') {
			accountingMapping.debitAccount = actualItem.value || '';
		} else if (actualItem && actualItem.fieldName === 'AMT_DOCCUR') {
			accountingMapping.debitValue = Math.abs(coreApi.legacyUtil.currencyToDecimalFormat(actualItem.value || '') || 0);
			accountingMapping.creditValue = coreApi.legacyUtil.currencyToDecimalFormat(actualItem.value || '');
		} else if (actualItem && actualItem.fieldName === 'ITEMNO_ACC') {

		} else if (actualItem && actualItem.fieldName === 'DOC_TYPE') {
			accountingMapping.creditDocumentType = actualItem.value || '';
			accountingMapping.debitDocumentType = actualItem.value || '';
			findDocumentType = !accountingMapping.creditDocumentType;
		} else if (actualItem && actualItem.fieldName === 'COSTCENTER') {

		} else if (actualItem && actualItem.fieldName === 'PROFIT_CTR') {

		} else if (actualItem && actualItem.fieldName === 'COMPANY_CODE') {
			accountingMapping.creditCompany = actualItem.value;
			accountingMapping.debitCompany = actualItem.value;
			findCompany = false;
		}
	}
	if (findCompany || findBranch || findReleaseDate) {
		let view = $.createBaseRuntimeModel($.viewSchema, 'timp.bsc.modeling/CV_CORRECTION_HIST', true);
		let values = [];
		let docsearch = {
			id: item.nfId,
			numitem: item.numItem
		};

		if (_this.lvcorrectionHist.has(docsearch)) {
			values = _this.lvcorrectionHist.get(docsearch);
		} else {
			let resultValue = view.find({
				where: [{
					field: 'NF_ID',
					operator: '$eq',
					value: item.nfId
                }, {
					field: 'NUM_ITEM',
					operator: '$eq',
					value: item.numItem
                }]
			});
			_this.lvcorrectionHist.set(docsearch, resultValue);
			values = resultValue;
		}
		values = values.results[0] || {};
		if (!accountingMapping.creditCompany || !accountingMapping.debitCompany) {
			accountingMapping.creditCompany = accountingMapping.creditCompany || values.COD_EMP || '';
			accountingMapping.debitCompany = accountingMapping.debitCompany || values.COD_EMP || '';
		}
		if (!accountingMapping.creditBranch || !accountingMapping.debitBranch) {
			accountingMapping.creditBranch = accountingMapping.creditBranch || values.COD_FILIAL || '';
			accountingMapping.debitBranch = accountingMapping.debitBranch || values.COD_FILIAL || '';
		}
		if (!accountingMapping.creditReleaseDate || !accountingMapping.debitReleaseDate) {
			let now = $.moment();
			let correctedDate = $.moment(values.DT_LANCTO, 'YYYYMMDD');
			let date = $.moment();
			if (now.year() !== correctedDate.year() || now.month() !== correctedDate.month()) {
				date = $.moment(correctedDate.format('YYYYMM') + '' + correctedDate.daysInMonth(), 'YYYYMMDD');
			}
			accountingMapping.creditReleaseDate = accountingMapping.creditReleaseDate || date.format('YYYYMMDD') || '';
			accountingMapping.debitReleaseDate = accountingMapping.debitReleaseDate || date.format('YYYYMMDD') || '';
		}
	}
	if (findDocumentType) {
		let view = $.createBaseRuntimeModel($.viewSchema, 'timp.bsc.modeling/CV_J_1BNFDOC', true);
		let values = [];
		let docsearch = {
			id: item.nfId,
			numitem: item.numItem
		};

		if (_this.lvdocumentType.has(docsearch)) {
			values = _this.lvdocumentType.get(docsearch);
		} else {
			let resultValue = view.find({
				where: [{
					field: 'DOCNUM',
					operator: '$eq',
					value: item.nfId
                }, {
					field: 'ITMNUM',
					operator: '$eq',
					value: item.numItem
                }]
			});
			_this.lvdocumentType.set(docsearch, resultValue);
			values = resultValue;
		}
		values = values.results[0] || {};
		if (!accountingMapping.creditDocumentType || !accountingMapping.debitDocumentType) {
			accountingMapping.creditDocumentType = accountingMapping.creditDocumentType || values.BLART || '';
			accountingMapping.debitDocumentType = accountingMapping.debitDocumentType || values.BLART || '';
		}
	}
	return {
		accountingMapping: accountingMapping,
		findCompany: findCompany,
		findBranch: findBranch,
		findDocumentType: !accountingMapping.creditDocumentType,
		findReleaseDate: !accountingMapping.creditReleaseDate,
		debitGroup: debitGroup
	};
};

this.setEditCorrectionData = function(object, editData) {
	var creditBranchDetail = editData.creditBranchDetail ? editData.creditBranchDetail.split("_")[1] : '';
	var debitBranchDetail = editData.debitBranchDetail ? editData.debitBranchDetail.split("_")[1] : '';
	object.creditValue = editData.creditValue || '';
	object.creditCoin = editData.creditCoin || '';
	object.creditAccount = editData.creditAccount || '';
	object.creditCompany = editData.creditCompany || '';
	object.creditDocumentType = editData.creditDocumentType || '';
	object.creditReleaseDate = editData.creditReleaseDate || '';
	object.creditProfitCenter = editData.creditProfitCenter || '';
	object.creditCostCenter = editData.creditCostCenter || editData.creditCostCenterCredit || editData.costCenterCredit || '';
	object.creditInternalOrder = editData.creditInternalOrder || '';
	object.creditPepElement = editData.creditPepElement || '';
	object.creditNetworkDiagram = editData.creditNetDiagram || editData.creditNetworkDiagram || '';
	object.creditOperation = editData.creditOperation || '';
	object.creditBranch = creditBranchDetail || editData.creditBranch || '';
	object.creditAttribute = editData.creditArticle || editData.creditAttribute || '';

	object.debitValue = editData.debitValue || '';
	object.debitCoin = editData.debitCoin || '';
	object.debitAccount = editData.debitAccount || '';
	object.debitCompany = editData.debitCompany || '';
	object.debitDocumentType = editData.debitDocumentType || '';
	object.debitReleaseDate = editData.debitReleaseDate || '';
	object.debitProfitCenter = editData.debitProfitCenter || '';
	object.debitCostCenter = editData.debitCostCenter || editData.debitCostCenterDebit || editData.costCenterDebit || '';
	object.debitInternalOrder = editData.debitInternalOrder || '';
	object.debitPepElement = editData.debitPepElement || '';
	object.debitNetworkDiagram = editData.debitNetDiagram || editData.debitNetworkDiagram || '';
	object.debitOperation = editData.debitOperation || '';
	object.debitBranch = debitBranchDetail || editData.debitBranch || '';
	object.debitAttribute = editData.debitArticle || editData.debitAttribute || '';
	return object;
};

this.saveEditCorrection = function(object) {
	let response = {
		success: false
	};
	try {
		correctionModel.UPDATEWHERE({
			justificationOfLastEdition: object.justification
		}, [
			{
				field: 'id',
				oper: '=',
				value: object.correctionId
			}
		]);
		(object.dataToSave).forEach(function(e) {
			accountingFieldModel.UPDATEWHERE({
				value: e.value
			}, [
				{
					field: 'id',
					oper: '=',
					value: e.id
				}
    		]);
		});
		response.success = true;
	} catch (err) {}
	return response;
};

this.saveEditCorrection_Deprecated = function(object) {
	let response = {
		success: false
	};
	try {
		let exists = correctionEditModel.READ({
			where: [{
				field: 'correctionId',
				oper: '=',
				value: object.correctionId
            }]
		})[0];
		if (exists && exists.correctionId) {
			correctionEditModel.UPDATEWHERE(object, [{
				field: 'correctionId',
				oper: '=',
				value: object.correctionId
            }]);
		} else {
			correctionEditModel.CREATE(object);
		}
		response.success = true;
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201062',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.getMDRAccountingFields = function(accountingFields) {
	let lang = $.request.cookies.get('Content-Language');
	let fields = {};
	if (accountingFields && accountingFields.length > 0) {
		accountingFields.forEach(function(field) {
			let fieldName;
			if (lang === 'ptrbr') {
				fieldName = field.descriptionPtrbr;
			} else {
				fieldName = field.descriptionEnus;
			}
			fields[field.id] = {
				label: fieldName
			};
		});
	}
	return fields;
};

/*
    Usage: this function receives an object like
    {
        id: 1 (optional),
        nfId: "" (optional),
        numItem: "" (optional),
        createdBy: 1 (optional),
        creationDate: [startDate,endDate] (optional),
        modifiedBy: 1 (optional),
        modificationDate: [startDate,endDate] (optional),
    }
    returns the where array
*/
this.getWhere = function(object) {
	var where = [];
	try {
		if (object && _.isPlainObject(object)) {
			if (_.has(object, 'id')) {
				where.push({
					field: 'id',
					oper: '=',
					value: object.id
				});
			}
			if (_.has(object, 'isCorrection')) {
				if (object.isCorrection == 0) {
					where.push({
						field: 'isCorrection',
						oper: '=',
						value: object.isCorrection
					}, {
						field: 'reason',
						oper: '!=',
						value: 'Manual Correction'
					});
				} else if (object.isCorrection == 1) {
					where.push([{
						field: 'isCorrection',
						oper: '=',
						value: object.isCorrection
                    }, {
						field: 'reason',
						oper: '=',
						value: 'Manual Correction'
                    }]);
				} else {
					where.push({
						field: 'isCorrection',
						oper: '=',
						value: object.isCorrection
					});
				}
			}
			if (_.has(object, 'nfId')) {
				where.push({
					field: 'nfId',
					oper: 'LIKE',
					value: '%' + object.nfId + '%'
				});
			}
			if (_.has(object, 'numItem')) {
				where.push({
					field: 'numItem',
					oper: 'LIKE',
					value: '%' + object.numItem + '%'
				});
			}
			if (_.has(object, 'status')) {
				where.push({
					field: 'status',
					oper: '=',
					value: object.status
				});
			}
			if (_.has(object, 'approvalType')) {
				where.push({
					field: 'approvalType',
					oper: '=',
					value: object.approvalType
				});
			}
			if (_.has(object, 'company')) {
				where.push({
					table: this.cvCorrectionOrm1(),
					field: 'companyId',
					oper: '=',
					value: object.company
				});
			}
			if (_.has(object, 'uf')) {
				where.push({
					table: this.cvCorrectionOrm1(),
					field: 'stateId',
					oper: '=',
					value: object.uf
				});
			}
			if (_.has(object, 'branch')) {
				where.push({
					table: this.cvCorrectionOrm1(),
					field: 'branchId',
					oper: '=',
					value: object.branch
				});
			}
			if (_.has(object, 'releaseDate')) {
				where.push({
					table: this.cvCorrectionOrm1(),
					field: 'releaseDate',
					oper: '>=',
					value: object.releaseDate[0] ? $.moment(object.releaseDate[0]).format('YYYYMMDD') : object.releaseDate[0]
				});
				where.push({
					table: this.cvCorrectionOrm1(),
					field: 'releaseDate',
					oper: '<=',
					value: object.releaseDate[1] ? $.moment(object.releaseDate[1]).format('YYYYMMDD') : object.releaseDate[1]
				});
			}
			if (_.has(object, 'reason')) {
				where.push({
					field: 'reason',
					oper: 'LIKE',
					value: '%' + object.reason + '%'
				});
			}
			if (_.has(object, 'justification')) {
				where.push({
					field: 'justification',
					oper: 'LIKE',
					value: '%' + object.justification + '%'
				});
			}
			if (_.has(object, 'accountingDocument')) {
				where.push({
					field: 'accountingDocument',
					oper: 'LIKE',
					value: '%' + object.accountingDocument + '%'
				});
			}
			if (_.has(object, 'correctionType')) {
				where.push({
					field: 'correctionType',
					oper: '=',
					value: object.correctionType
				});
			}
			if (_.has(object, 'reversedDocument')) {
				where.push({
					field: 'reversedDocument',
					oper: 'LIKE',
					value: '%' + object.reversedDocument + '%'
				});
			}
			if (_.has(object, 'correctionLetterDocument')) {
				where.push({
					field: 'correctionLetterDocument',
					oper: 'LIKE',
					value: '%' + object.correctionLetterDocument + '%'
				});
			}
			if (_.has(object, 'fields')) {
				where.push({
					table: fieldByCorrectionModel,
					field: 'fieldName',
					oper: '=',
					value: object.fields
				});
			}
			if (_.has(object, 'pendingJob')) {
				where.push({
					field: 'pendingJob',
					oper: '=',
					value: object.pendingJob
				});
			}
			if (_.has(object, 'accounting')) {
				where.push({
					field: 'accounting',
					oper: '=',
					value: object.accounting
				});
			}
			if (_.has(object, 'createdBy')) {
				where.push({
					field: 'creationUser',
					oper: '=',
					value: object.createdBy
				});
			}
			if (_.has(object, 'creationDate')) {
				where.push({
					field: 'creationDate',
					oper: '>=',
					value: object.creationDate[0]
				});
				where.push({
					field: 'creationDate',
					oper: '<=',
					value: object.creationDate[1]
				});
			}
			if (_.has(object, 'modifiedBy')) {
				where.push({
					field: 'modificationUser',
					oper: '=',
					value: object.modifiedBy
				});
			}
			if (_.has(object, 'modificationDate')) {
				where.push({
					field: 'modificationDate',
					oper: '>=',
					value: object.modificationDate[0]
				});
				where.push({
					field: 'modificationDate',
					oper: '<=',
					value: object.modificationDate[1]
				});
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201063',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return where;
};

this.getJ_1BNFDOC = function() {
	$.import('timp.bsc.server.models.tables', 'J_1BNFDOC');
	const J_1BNFDOC = $.timp.bsc.server.models.tables.J_1BNFDOC.getTable();
	return J_1BNFDOC;
};

this.cvCorrectionOrm1 = function() {
	$.import('timp.bsc.server.models.views', 'cvCorrection');
	const cvCorrectionOrm1 = $.timp.bsc.server.models.views.cvCorrection.cvCorrectionOrm1;
	return cvCorrectionOrm1;
};

this.getJoin = function(detailed, accounting, inner, filters) {
	let typeJoin = inner ? 'inner' : 'left';
	var join = [{
		table: fieldByCorrectionModel,
		alias: 'fields',
		outer: 'left',
		on: [{
			left: 'id',
			right: 'correctionId'
        }]
    }, {
		table: messageByCorrectionModel,
		alias: 'messages',
		outer: 'left',
		on: [{
			left: 'id',
			right: 'correctionId'
        }]
    }, {
		table: coreApi.users,
		rename: 'U2',
		alias: 'creationUserData',
		on: [{
			left: 'creationUser',
			right: 'id'
        }],
		outer: 'left'
    }, {
		table: coreApi.users,
		rename: 'U3',
		alias: 'modificationUserData',
		on: [{
			left: 'modificationUser',
			right: 'id'
        }],
		outer: 'left'
    }];
	if (detailed) {
		$.import('timp.bsc.server.models.tables', 'mdrModels');
		const scenarioModels = $.timp.bsc.server.models.tables.mdrModels;
		const sourceFieldsOrm1 = scenarioModels.sourceFieldsOrm1;
		const MDRAccountingFields = scenarioModels.accountingFieldsOrm1;
		$.import('timp.bsc.server.models.views', 'cvCorrectionComplementaryInformation');
		const cvCorrectionComplementaryInformation = $.timp.bsc.server.models.views.cvCorrectionComplementaryInformation.cvCorrectionComplementaryInformation;
		join.push({
			table: headerFieldModel,
			alias: 'headerFields',
			outer: 'left',
			on: [{
				left: 'id',
				right: 'correctionId'
            }]
		}, {
			table: itemGroupModel,
			alias: 'itemGroups',
			outer: 'left',
			on: [{
				left: 'id',
				right: 'correctionId'
            }]
		}, {
			table: itemFieldModel,
			alias: 'itemFields',
			outer: 'left',
			on: [{
				leftTable: itemGroupModel,
				left: 'id',
				right: 'itemGroupId'
            }]
		}, {
			table: sourceFieldsOrm1,
			alias: 'headerSource',
			rename: 's1',
			outer: 'left',
			on: [{
				leftTable: headerFieldModel,
				left: 'fieldName',
				right: 'field'
            }]
		}, {
			table: sourceFieldsOrm1,
			alias: 'itemSource',
			rename: 's2',
			outer: 'left',
			on: [{
				leftTable: itemFieldModel,
				left: 'fieldName',
				right: 'field'
            }]
		}, {
			table: accountingFieldModel,
			alias: 'accountingFields',
			outer: 'left',
			on: [{
				left: 'id',
				right: 'correctionId'
            }]
		}, {
			table: MDRAccountingFields,
			alias: 'MDRAccountingFields',
			outer: 'left',
			on: [{
				leftTable: accountingFieldModel,
				left: 'fieldName',
				right: 'key'
            }]
		}, {
			fields: ['docnum', 'companyId', 'branchId', 'direction', 'numDoc', 'uf'],
			table: cvCorrectionComplementaryInformation,
			rename: 'cvCorrectionComplementaryInformation',
			alias: 'cvCorrectionComplementaryInformation',
			on: [{
				left: 'nfId',
				right: 'docnum'
            }],
			outer: 'left'
		});
	}
	if (accounting) {
		$.import('timp.bsc.server.models.tables', 'externalModels');
		const accountingMappingForCorrections = $.timp.bsc.server.models.tables.externalModels.accountingMappingForCorrections;
		const accountingMappingForCorrectionsFieldsToValidation = $.timp.bsc.server.models.tables.externalModels.accountingMappingForCorrectionsFieldsToValidation;
		const scenarioHierarchy = $.timp.bsc.server.models.tables.externalModels.scenarioHierarchy;
		const scenarioConfigurationByHierarchy = $.timp.bsc.server.models.tables.externalModels.scenarioConfigurationByHierarchy;
		const scenarioConfiguration = $.timp.bsc.server.models.tables.externalModels.scenarioConfiguration;
		let onScenarioHierarchy = [{
			left: 'scenarioHierarchyId',
			right: 'id'
        }];
		const hasTaxFilter = filters && filters.tax ? true : false;
		if (hasTaxFilter) {
			onScenarioHierarchy.push([{
				field: 'taxCode',
				oper: '=',
				value: filters.tax
			}, {
				field: 'taxCode',
				oper: 'IS NULL'
			}]);
		}
		join.push({
			table: scenarioHierarchy,
			// fields: [],
			alias: 'scenarioHierarchy',
			outer: (hasTaxFilter && !filters.getNullTax) ? 'inner' : typeJoin,
			on: onScenarioHierarchy
		}, {
			table: scenarioConfigurationByHierarchy,
			fields: [],
			alias: 'scenarioConfigurationByHierarchy',
			outer: typeJoin,
			on: [{
				leftTable: scenarioHierarchy,
				left: 'id',
				right: 'scenarioHierarchyId'
            }]
		}, {
			table: scenarioConfiguration,
			// fields: [],
			alias: 'scenarioConfiguration',
			outer: typeJoin,
			on: [{
				leftTable: scenarioConfigurationByHierarchy,
				left: 'scenarioConfigurationId',
				right: 'id'
            }]
		}, {
			table: accountingMappingForCorrections,
			alias: 'accountingMappingForCorrections',
			outer: typeJoin,
			on: [{
				leftTable: scenarioConfiguration,
				left: 'code',
				right: 'correctionScenario'
            }]
		}, {
			table: accountingMappingForCorrectionsFieldsToValidation,
			alias: 'accountingMappingForCorrectionsFieldsToValidation',
			outer: typeJoin,
			on: [{
				leftTable: accountingMappingForCorrections,
				left: 'id',
				right: 'accountingMappingForCorrectionsId'
            }]
		}, {
			table: correctionEditModel,
			alias: 'editData',
			outer: 'left',
			on: [{
				leftTable: correctionModel,
				left: 'id',
				right: 'correctionId'
            }]
		});
	}
	return join;
};

this.accounting = function(object) {
	var response = [];
	try {
		//execute function
		if (_.isPlainObject(object) && _.has(object, 'id') && _.isArray(object.id)) {

			var listNumberAccountWithouCostCenter = this.getAccountsWithoutCostCenter();

			//for each correction

			if (!object.isGrouped) {
				_.forEach(object.id, function(id) {
					//try to approve correction
					var approveResponse = _this.approveCorrection(id, object.adapterId, object.location, object.isShadow, true,
						listNumberAccountWithouCostCenter);
					response.push(approveResponse);
				});
			} else {
				var approveResponse = _this.approveCorrection(object.id, object.adapterId, object.location, object.isShadow, true,
					listNumberAccountWithouCostCenter, object.isGrouped);
				response.push(approveResponse);
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201075',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.getAccountsWithoutCostCenter = function() {
	$.import('timp.mdr.server.api', 'api');
	const mdrApi = $.timp.mdr.server.api.api;
	const accountWithoutCostCenterModel = mdrApi.accountWithoutCostCenterModel;

	let today = new Date();

	let results = accountWithoutCostCenterModel.find({
		select: [{
			field: "numberAccount"
		}],
		where: [[
			{
				field: 'startValidity',
				operator: '$lte',
				value: today
            }, {
				field: 'startValidity',
				operator: '$eq',
				value: null
            }
        ], [
			{
				field: 'endValidity',
				operator: '$gt',
				value: today
            }, {
				field: 'endValidity',
				operator: '$eq',
				value: null
            }
        ]]
	}).results;

	return results.map(function(i) {
		return i.numberAccount;
	});
};

this.prepareAccountingFields = function(accounting) {
	let fieldName = {
		COST_CENTER: 'COSTCENTER',
		CREDIT_ACCOUNT: 'GL_ACCOUNT',
		DEBIT_ACCOUNT: 'GL_ACCOUNT',
		CREDIT_ACCOUNT_AMOUNT: 'AMT_DOCCUR',
		DEBIT_ACCOUNT_AMOUNT: 'AMT_DOCCUR',
		PROFIT_CENTER: 'PROFIT_CTR',
		COMPANY: 'COMPANY_CODE',
		HEADER_TEXT: 'HEADER_TXT',
		ITEM_NUMBER_ACCOUNTING_DOCUMENT: 'ITEM_NUMBER_ACCOUNTING_DOCUMENT',
		ACCOUNT_DOCUMENT: 'DOC_TYPE',
		POST_DATE: 'PSTNG_DATE',
		DOC_DATE: 'DOC_DATE',
		PSTNG_DATE: 'PSTNG_DATE',
		COIN: 'CURRENCY',
		INTERNAL_ORDER: 'ORDERID',
		PEP_ELEMENT: 'WBS_ELEMENT',
		NETWORK_DIAGRAM: 'NETWORK',
		REFERENCE: 'REF_DOC_NO',
		ASSIGNMENT: 'REF_DOC_NO',
		ITEM_TEXT: 'ITEM_TEXT',
		BRANCH: 'BRANCH',
		LOGISTICS_CENTER: 'PLANT',
		COSTCENTER: 'COSTCENTER',
		GL_ACCOUNT: 'GL_ACCOUNT',
		AMT_DOCCUR: 'AMT_DOCCUR',
		PROFIT_CTR: 'PROFIT_CTR',
		COMPANY_CODE: 'COMPANY_CODE',
		HEADER_TXT: 'HEADER_TXT',
		ITEM_NUMBER_ACCOUNTING_DOCUMENT: 'ITEM_NUMBER_ACCOUNTING_DOCUMENT',
		DOC_TYPE: 'DOC_TYPE',
		PSTNG_DATE: 'PSTNG_DATE',
		CURRENCY: 'CURRENCY',
		ORDERID: 'ORDERID',
		WBS_ELEMENT: 'WBS_ELEMENT',
		NETWORK: 'NETWORK',
		REF_DOC_NO: 'REF_DOC_NO',
		ITEM_TEXT: 'ITEM_TEXT',
		BRANCH: 'BRANCH',
		PLANT: 'PLANT',
		MATERIAL: 'MATERIAL',
		QUANTITY: 'QUANTITY',
		MEASURE: 'MEASURE'
	};
	accounting.headerFields = [];
	accounting.itemFields = [];
	accounting.headerFields.push({
		fieldName: 'BRANCH',
		value: $.lodash.has(accounting, 'cvCorrectionComplementaryInformation') ? (accounting.cvCorrectionComplementaryInformation.branchId ||
			'') : ''
	});
	_.forEach(accounting.accountingFields, (field) => {
		if (field.isHeader) {
			if (field.fieldName === 'POST_DATE') {
				let date = this.getMomentDate(field.value);
				if (date.isValid()) {
					field.value = date.format('YYYYMMDD');
				}
				let subPeriod = this.decomposeDate(field.value);
				accounting.headerFields.push({
					fieldName: fieldName[field.fieldName],
					value: subPeriod.year + subPeriod.month + subPeriod.date,
					useReference: field.useReference
				});
				accounting.headerFields.push({
					fieldName: 'YEAR',
					value: subPeriod.year
				}, {
					fieldName: 'MONTH',
					value: subPeriod.month
				});
			} else {
				accounting.headerFields.push({
					fieldName: fieldName[field.fieldName],
					value: field.value,
					useReference: field.useReference
				});
			}
		} else {
			accounting.itemFields.push({
				fieldName: fieldName[field.fieldName],
				value: field.value,
				itemGroupId: field.group || 0,
				useReference: field.useReference
			});
		}
	});
	return accounting;
};

this.getMomentDate = function(originalDate) {
	let date = $.moment(originalDate, 'DDMMYYYY');
	if (!date.isValid()) {
		date = $.moment(originalDate, 'YYYYMMDD');
	}
	if (!date.isValid()) {
		date = $.moment(originalDate, 'MMDDYYYY');
	}
	if (!date.isValid()) {
		date = $.moment(originalDate, 'DD/MM/YYYY');
	}
	if (!date.isValid()) {
		date = $.moment(originalDate, 'MM/DD/YYYY');
	}
	if (!date.isValid()) {
		date = $.moment(originalDate, 'YYYY/MM/DD');
	}
	return date;
};

this.getReferenceFields = function(idNF, idItem, fields) {
	let response = {};
	let NF = {};
	let fieldsMap = {
		COSTCENTER: 'KOSTL',
		PROFIT_CTR: 'PRCTR',
		ACCOUNT_DOCUMENT: 'DOCTYP',
		DOC_TYPE: 'BLART',
		NETWORK: 'NPLNR',
		ORDERID: 'AUFNR',
		WBS_ELEMENT: 'PROJK'
	};
	const nfView = $.createBaseRuntimeModel($.viewSchema, REFERENCE_VIEW, true);
	NF = nfView.find({
		select: $.lodash.map(fields, field => {
			return {
				field: fieldsMap[field]
			};
		}),
		where: [{
			field: 'DOCNUM',
			operator: '$eq',
			value: idNF
        }, {
			field: 'ITMNUM',
			operator: '$eq',
			value: idItem
        }]
	});
	if ($.lodash.isEmpty(NF.errors) && !$.lodash.isEmpty(NF.results) && NF.results.length >= 1) {
		$.lodash.forEach(fields, field => {
			if ($.lodash.has(NF.results[0], fieldsMap[field]) && !$.lodash.isEmpty(NF.results[0][fieldsMap[field]]) && NF.results[0][fieldsMap[
				field]] !== '00000000') {
				response[field] = NF.results[0][fieldsMap[field]];
			}
		});
	}
	return response;
};

this.decomposeDate = function(date) {
	let response = {
		year: '',
		month: '',
		date: ''
	};
	if (!$.lodash.isNil(date)) {
		if ($.lodash.isString(date)) {
			if (!$.lodash.isNil(date.match(/[-/]/g))) {
				let period = date.split(/[-/]/g);
				if (!$.lodash.isNil(period[0]) && !$.lodash.isNil(period[1])) {
					if (period[0].length === 4) {
						response.year = period[0];
						response.month = period[1];
						response.date = period[2] || '';
					} else if (period[1].length === 4) {
						response.year = period[1];
						response.month = period[0];
						response.date = period[2] || '';
					} else if (!$.lodash.isNil(period[2]) && period[2].length === 4) {
						response.year = period[2];
						response.month = period[1];
						response.date = period[0] || '';
					}
				}
			} else {
				if (date.length >= 6) {
					response.year = date.substr(0, 4);
					response.month = date.substr(4, 2);
					response.date = date.substr(6, 2) || '';
				}
			}
		} else if ($.lodash.isDate(date)) {
			response.year = date.getFullYear();
			response.month = date.getMonth() + 1;
			response.month = response.month > 9 ? response.month : ('0' + response.month);
			response.date = date.getDate();
			response.date = response.date > 9 ? response.date : ('0' + response.date);
		}
	}
	return response;
};

/*
    Usage: this function receives an object like
    {
        id: [1,2,3,...] (required),
        adapterId: <number> (required when system has TDF),
        location: <string> (required when system has TDF),
        isShadow: <boolean> (optional)
    }
*/

this.approve = function(object) {
	var response = [];
	try {
		//execute function
		if (_.isPlainObject(object) && _.has(object, 'id') && _.isArray(object.id)) {
			//for each correction
			_.forEach(object.id, function(id) {
				//try to approve correction
				if (_.isNil(object.adapterId)) {

				}
				var approveResponse = _this.approveCorrection(id, object.adapterId, object.location, object.isShadow);
				response.push(approveResponse);
			});
		}

	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201065',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.getAllIdsToBeApproved = function(status, ids, filters) {
	let response = [];
	try {
		let where = [{
			field: 'status',
			oper: '=',
			value: status || 'PENDING'
        }];
		where = where.concat(_this.getWhere(filters ? filters : {}));
		let joins = _this.getJoin(filters && filters.validationFields === 1, true, false, filters);
		response = correctionModel.READ({
			fields: ['id'],
			where: where,
			joins: joins,
			orderBy: ['id']
		});
		response = response ? response.map(function(e) {
			return e.id;
		}) : ids;
	} catch (e) {
		response = ids;
	}
	return response;
};

this.eccCorrections = function(webService) {
	const corrections = {
		timp_modifica_documento_fatura_compras: true,
		timp_modifica_documento_fatura_vendas: true,
		timp_modifica_documento_vendas: true,
		timp_modifica_documento_compras: true,
		timp_correção_cadastro_fornecedores: true,
		timp_correção_cadastro_clientes: true,
		timp_correção_cadastro_materiais: true,
	}

	return corrections[webService];
};

this.approveCorrection = function(id, adapterId, location, isShadow, isAccounting, listAccountWithouCostCenter, isGrouped) {
	var _self = this;
	var response = {
		id: id,
		xmlSent: '',
		xmlResponse: ''
	};
	try {
		var corrections = this.getCorrectionById(id, isAccounting);
		var correction = isGrouped ? corrections[0] : corrections;
		correction.isDadosContabeis = false;
		if (correction.correctionType === 'timp_modifica_dados_contabeis' && correction.accounting === 'R' && (correction.accountingFields &&
			correction.accountingFields.length > 0)) {
			correction.correctionType = 'TMFFIPostingPostSyncIn';
			correction.isDadosContabeis = 'true';
		}

		if (isGrouped) {
			$.lodash.forEach(corrections, function(correction) {
				this.recalculateDynamicValues(correction.fields);
			});

		} else {
			this.recalculateDynamicValues(correction.fields);
		}
		if (this.isValidCorrection(correction, isAccounting)) {
			// response.correctionType = correction.correctionType;

			if (this.hasTDF() || ['timp_modifica_dados_contabeis', 4, 'timp_dados_retencao', 5].indexOf(correction.correctionType) !== -1 ||
				this.eccCorrections(correction.correctionType)) {

				var approveResponse = {
					xmlSent: '',
					xmlResponse: ''
				};
				if ((['TMFFIPostingPostSyncIn', 1].indexOf(correction.correctionType) !== -1 && (correction.accounting === 'R' && (correction.isCorrection !==
					1 || correction.wasCorrected === 1)))) {
					if (isAccounting) {
						if (isGrouped) {
							corrections = _.map(corrections, function(item) {
								return _self.prepareAccountingFields(item)
							});
						} else {
							correction = this.prepareAccountingFields(correction);
						}
					}
					response.correctionType = 'TMFFIPostingPostSyncIn';
					let isHttpsActive = this.getHttpsActivity('TMFFIPostingPostSyncIn');
					approveResponse = this.callAccounting(isGrouped ? corrections : correction, listAccountWithouCostCenter, null, null, isHttpsActive,
						isGrouped);

				} else if (['timp_dados_retencao', 5].indexOf(correction.correctionType) !== -1) {
					response.correctionType = 'timp_dados_retencao';
					response.upsert = this.callCorrectionRetencao(correction);

				} else if (['timp_modifica_dados_contabeis', 4].indexOf(correction.correctionType) !== -1 && !isShadow) {
					response.correctionType = 'timp_modifica_dados_contabeis';
					approveResponse = this.callCorrectionAccountingEntry(correction, adapterId);

				} // todo agregar condición
				else if (['TMFNotaFiscalCorrectionsSyncIn', 0, null, undefined].indexOf(correction.correctionType) !== -1 || (['CORRECTED', 2].indexOf(
					correction.status) === -1) && correction.isCorrection === 1 && correction.wasCorrected !== 1) {
					response.correctionType = 'TMFNotaFiscalCorrectionsSyncIn';
					let isHttpsActive = this.getHttpsActivity('TMFNotaFiscalCorrectionsSyncIn');
					approveResponse = _this.approveByTDF(correction, adapterId, location, isShadow, isHttpsActive);

				} else if (['TMFFIReversalPostSyncIn', 2].indexOf(correction.correctionType) !== -1 && !isShadow) {
					response.correctionType = 'TMFFIReversalPostSyncIn';
					let isHttpsActive = this.getHttpsActivity('TMFFIReversalPostSyncIn');
					approveResponse = this.callReverse(correction, isHttpsActive);

				} else if (['TMFNFCorrectionLetterCreationSyncIn', 3].indexOf(correction.correctionType) !== -1 && !isShadow) {
					response.correctionType = 'TMFNFCorrectionLetterCreationSyncIn';
					let isHttpsActive = this.getHttpsActivity('TMFNFCorrectionLetterCreationSyncIn');
					approveResponse = this.callCorrectionLetterCreation(correction, adapterId, isHttpsActive);

				} else if (this.eccCorrections(correction.correctionType) && !isShadow) {
					response.correctionType = correction.correctionType;
					approveResponse = this.callCorrectionAccountingEntry(correction, adapterId);

				}
				if (approveResponse.successful) {
					this.updateDynamicValues(correction.fields);
				}
				var {
					xmlSent,
					xmlResponse,
					hasToBeApprovedByJob
				} = approveResponse;
				Object.assign(response, {
					xmlSent,
					xmlResponse,
					isAccounting: approveResponse.isAccounting,
					isShadow: approveResponse.isShadow,
					hasToBeApprovedByJob
				});
			} else {
				$.messageCodes.push({
					code: 'BSC201056',
					type: 'E',
					errorInfo: 'TDF is mandatory'
				});
			}
		} else {
			$.messageCodes.push({
				errorInfo: 'The correction is not valid'
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201065',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		Supervisor.approveCorrectionError(id, util.parseError(e));
	}
	return response;
};

this.callCorrectionRetencao = function(correction) {
	const _self = this;
	//updating correction
	var response = {};

	//Get Register By Keys from the view

	//Keys

	//Select View
	var view = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.gdu.ctr.nota_fiscal.item_com_imposto/CV_NF_CONT_RETAINED',
		true);
	var options = {};

	//Select Structure Correction
	const atrApi = $.timp.atr.server.api.api;
	var structureCorrencion = atrApi.structure.table.READ({
		field: ['structure'],
		where: [{
			field: 'hanaName',
			oper: '=',
			value: 'CV_NF_CONT_RETAINED'
                }]
	})[0];
	structureCorrencion = JSON.parse(structureCorrencion.structure);
	var fieldsMap = {};
	(structureCorrencion.fields).forEach((field) => {
		fieldsMap[field.hanaName] = field;
	})
	//Push Keys
	//KSANT CHANGE
	options.where = [{
		field: 'BELNR',
		operator: '$eq',
		value: correction.nfId
    }, {
		field: 'BUZEI',
		operator: '$eq',
		value: correction.numItem
    }, {
		field: 'EMPRESA',
		operator: '$eq',
		value: correction.company
    }, {
		field: 'GJAHR',
		operator: '$eq',
		value: correction.year
    }];

	options.select = [{
			field: "AUGBL"
		},
		{
			field: "BAIRRO"
		},
		{
			field: "BASE_EXC_IRF"
		},
		{
			field: "BELNR"
		},
		{
			field: "BUDAT"
		},
		{
			field: "BUZEI"
		},
		{
			field: "CEP"
		},
		{
			field: "CNPJ_PARCEIRO_MESTRE"
		},
		{
			field: "COD_PART"
		},
		{
			field: "CPF_PARCEIRO_MESTRE"
		},
		{
			field: "DESC_MUN"
		},
		{
			field: "DOC_ESTORNO"
		},
		{
			field: "DOC_HEADER_TEXT"
		},
		{
			field: "DT_DOC"
		},
		{
			field: "EMPRESA"
		},
		{
			field: "GJAHR"
		},
		{
			field: "IE"
		},
		{
			field: "IM"
		},
		{
			field: "TIPO_PART"
		},
		{
			field: "PSWSL"
		},
		{
			field: "TIP_REND"
		},
		{
			field: "NUM_ARQ"
		},
		{
			field: "IND_ESTORNO"
		},
		{
			field: "KOART"
		},
		{
			field: "MANDT"
		},
		{
			field: "NOME"
		},
		{
			field: "NUM_REF_LANCTO"
		},
		{
			field: "PAIS"
		},
		{
			field: "FILIAL"
		},
		{
			field: "AUGDT"
		},
		{
			field: "ENDE"
		},
		{
			field: "NUMERO"
		},
		{
			field: "DOC_HEADER_TEXT"
		},
		{
			field: "UF"
		}];

	var results = view.find(options).results;

	var register = results[0];

	//replace the correction values
	//Push Keys

	//Insert in the shadow
	var configurations = this.getConfigurations();
	const schema = configurations.schema;
	const mandt = configurations.mandt;
	var shadow = $.createBaseRuntimeModel(schema, "/TMF/D_NF_IMPWH", false, false);

	if (!register) {
		return false;
	}
	var CAT_IMPOSTO = [""];
	var CODIGO_IRF = [""];
	var existing = {};

	var shadowItem = $.createBaseRuntimeModel($.viewSchema, 'sap.glo.tmflocbr.ctr/NF_IMPOSTO_RETIDO', true);

	var shadowRegisters = shadowItem.find({
		where: [{
			field: 'NUM_LCTO',
			operator: '$eq',
			value: correction.nfId
        }, {
			field: 'NUM_ITEM',
			operator: '$eq',
			value: correction.numItem
        }, {
			field: 'EMPRESA',
			operator: '$eq',
			value: correction.company
        }, {
			field: 'EXERCICIO',
			operator: '$eq',
			value: correction.year
        }]
	}).results;
	for (var r = 0; r < shadowRegisters.length; r++) {
		if (shadowRegisters[r].CAT_IMPOSTO) {
			if (existing[shadowRegisters[r].CAT_IMPOSTO]) {
				continue;
			}
			CAT_IMPOSTO.push(shadowRegisters[r].CAT_IMPOSTO);
			CODIGO_IRF.push(shadowRegisters[r].CODIGO_IRF);
			existing[shadowRegisters[r].CAT_IMPOSTO] = true;
		}

	}

	var taxKeys = ["INSS", "INSS_20", "IR", "PIS", "COFINS", "ISS", "CSLL", "SEST"];
	var dtDocCorrection = false;
	var taxToInsert = [];
	for (var i = 0; i < correction.fields.length; i++) {
		correction.fields[i] = _self.formatDate(correction.fields[i], fieldsMap);
		register[correction.fields[i].fieldName] = correction.fields[i].newValue;
		if (correction.fields[i].fieldName === 'DT_DOC') {
			dtDocCorrection = true;
		}
		for (var j = 0; j < taxKeys.length; j++) {
			if (taxKeys[j] == "INSS" && correction.fields[i].fieldName.indexOf("INSS_20") !== -1) {
				continue;
			}
			if ((correction.fields[i].fieldName.indexOf("CA_CAT_IRF_" + taxKeys[j]) !== -1 ||
					correction.fields[i].fieldName.indexOf("CA_COD_IRF_" + taxKeys[j]) !== -1 ||
					correction.fields[i].fieldName.indexOf("CA_COD_CTA_" + taxKeys[j]) !== -1 ||
					correction.fields[i].fieldName.indexOf("CA_BASE_" + taxKeys[j]) !== -1 ||
					correction.fields[i].fieldName.indexOf("CA_VALOR_" + taxKeys[j]) !== -1) &&
				taxToInsert.indexOf(taxKeys[j]) === -1) {
				taxToInsert.push(taxKeys[j]);
			}
		}
	}
	if (!taxToInsert.length) {
		taxToInsert.push("INSS");
	}

	var mapToSave = [];

	if (dtDocCorrection) {

		for (var k = 0; k < CAT_IMPOSTO.length; k++) {
			mapToSave.push({
				"DOC_COMPENSACAO": register.AUGBL,
				"BAIRRO": register.BAIRRO,
				"BASE_EXC_IRF": register.BASE_EXC_IRF || 0,
				"NUM_LCTO": register.BELNR,
				"DT_LCTO": register.BUDAT,
				"NUM_ITEM": register.BUZEI,
				"CEP": register.CEP,
				"CNPJ": register.CNPJ_PARCEIRO_MESTRE,
				"COD_PART": register.COD_PART,
				"CPF": register.CPF_PARCEIRO_MESTRE,
				"CIDADE": register.DESC_MUN,
				"DOC_ESTORNO": register.DOC_ESTORNO,
				"DOC_HEADER_TEXT": register.DOC_HEADER_TEXT,
				"DT_DOC": register.DT_DOC,
				"EMPRESA": register.EMPRESA,
				"EXERCICIO": register.GJAHR,
				"IE": register.IE,
				"IM": register.IM,
				"TIPO_PART": register.TIPO_PART,
				"CODIGO_IRF": CODIGO_IRF[k],
				"CAT_IMPOSTO": CAT_IMPOSTO[k],
				"MOEDA": register.PSWSL,
				"TIP_REND": register.TIP_REND,
				"NUM_ARQ": register.NUM_ARQ || '',
				"IND_ESTORNO": register.IND_ESTORNO,
				"CHAVE_IRF": register.CA_COD_RECEITA_INSS || register.CA_COD_RECEITA_INSS_20 || register.CA_COD_RECEITA_IR ||
					register.CA_COD_RECEITA_PIS || register.CA_COD_RECEITA_COFINS || register.CA_COD_RECEITA_ISS || register.CA_COD_RECEITA_CSLL ||
					register.CA_COD_RECEITA_SEST,
				"J_1BWHT_BS": register.J_1BWHT_BS || 0,
				"TIP_CONTA": register.KOART,
				"MANDT": register.MANDT,
				"NOME": register.NOME,
				"CH_REF": register.NUM_REF_LANCTO,
				"COD_PAIS": register.PAIS, //no modif
				"ESTABLECIMENTO": register.FILIAL, //no modif
				"DT_COMP": register.AUGDT, //modified
				"ENDE": register.ENDE, //no modif
				"NUMERO": register.NUMERO, //no modif
				"DOC_HEADER_TEXT": register.DOC_HEADER_TEXT, //no modif
				"UF": register.UF
			});
			mapToSave[k].MANDT = mandt;
			for (var j = 0; j < Object.keys(mapToSave[k]).length; j++) {
				if (Object.keys(mapToSave[k])[j] !== "BASE_EXC_IRF" && Object.keys(mapToSave[k])[j] !== "BASE_IRF" && Object.keys(mapToSave[k])[j] !==
					"J_1BWHT_BS" && Object.keys(mapToSave[k])[j] !== "VL_IRF") {
					mapToSave[k][Object.keys(mapToSave[k])[j]] = mapToSave[k][Object.keys(mapToSave[k])[j]] || '';
				}
			}
		}
	} else {
		for (var k = 0; k < taxToInsert.length; k++) {
			mapToSave.push({
				"DOC_COMPENSACAO": register.AUGBL,
				"BAIRRO": register.BAIRRO,
				"BASE_EXC_IRF": register.BASE_EXC_IRF || 0,
				"NUM_LCTO": register.BELNR,
				"DT_LCTO": register.BUDAT,
				"NUM_ITEM": register.BUZEI,
				"CEP": register.CEP,
				"CNPJ": register.CNPJ_PARCEIRO_MESTRE,
				"COD_PART": register.COD_PART,
				"CPF": register.CPF_PARCEIRO_MESTRE,
				"CIDADE": register.DESC_MUN,
				"DOC_ESTORNO": register.DOC_ESTORNO,
				"DOC_HEADER_TEXT": register.DOC_HEADER_TEXT,
				"DT_DOC": register.DT_DOC,
				"EMPRESA": register.EMPRESA,
				"EXERCICIO": register.GJAHR,
				"IE": register.IE,
				"IM": register.IM,
				"TIPO_PART": register.TIPO_PART,

				"MOEDA": register.PSWSL,
				"TIP_REND": register.TIP_REND,
				"NUM_ARQ": register.NUM_ARQ || '',
				"IND_ESTORNO": register.IND_ESTORNO,
				"CHAVE_IRF": register.CA_COD_RECEITA_INSS || register.CA_COD_RECEITA_INSS_20 || register.CA_COD_RECEITA_IR ||
					register.CA_COD_RECEITA_PIS || register.CA_COD_RECEITA_COFINS || register.CA_COD_RECEITA_ISS || register.CA_COD_RECEITA_CSLL ||
					register.CA_COD_RECEITA_SEST,
				"J_1BWHT_BS": register.J_1BWHT_BS || 0,
				"TIP_CONTA": register.KOART,
				"MANDT": register.MANDT,
				"NOME": register.NOME,
				"CH_REF": register.NUM_REF_LANCTO,
				"COD_PAIS": register.PAIS, //no modif
				"ESTABLECIMENTO": register.FILIAL, //no modif
				"CAT_IMPOSTO": register["CA_CAT_IRF_" + taxToInsert[k]],
				"CODIGO_IRF": register["CA_COD_IRF_" + taxToInsert[k]],
				"DT_COMP": register.AUGDT, //modified
				"COD_CTA": register["CA_COD_CTA_" + taxToInsert[k]],
				"ENDE": register.ENDE, //no modif
				"NUMERO": register.NUMERO, //no modif
				"BASE_IRF": parseFloat(register["CA_BASE_" + taxToInsert[k]] || 0), //modified
				"VL_IRF": parseFloat(register["CA_VALOR_" + taxToInsert[k]] || 0), //modified
				"DOC_HEADER_TEXT": register.DOC_HEADER_TEXT, //no modif
				"UF": register.UF
			});
			mapToSave[k].MANDT = mandt;
			for (var j = 0; j < Object.keys(mapToSave[k]).length; j++) {
				if (Object.keys(mapToSave[k])[j] !== "BASE_EXC_IRF" && Object.keys(mapToSave[k])[j] !== "BASE_IRF" && Object.keys(mapToSave[k])[j] !==
					"J_1BWHT_BS" && Object.keys(mapToSave[k])[j] !== "VL_IRF") {
					mapToSave[k][Object.keys(mapToSave[k])[j]] = mapToSave[k][Object.keys(mapToSave[k])[j]] || '';
				}
			}
		}
	}

	for (var k = 0; k < mapToSave.length; k++) {
		response = shadow.upsert(mapToSave[k], {
			"where": [{
				field: 'MANDT',
				operator: '$eq',
				value: mandt
            }, {
				field: 'EMPRESA',
				operator: '$eq',
				value: mapToSave[k].EMPRESA
            }, {
				field: 'NUM_LCTO',
				operator: '$eq',
				value: mapToSave[k].NUM_LCTO
            }, {
				field: 'EXERCICIO',
				operator: '$eq',
				value: mapToSave[k].EXERCICIO
            }, {
				field: 'NUM_ITEM',
				operator: '$eq',
				value: mapToSave[k].NUM_ITEM
            }, {
				field: 'CAT_IMPOSTO',
				operator: '$eq',
				value: mapToSave[k].CAT_IMPOSTO
            }, ]
		}, true);
	}

	//Update the status of the correction
	if (response.results.upserted) {
		var objToUpdate = {
			id: correction.id
		};
		objToUpdate.status = 2;
		objToUpdate.correctionLetterDocument = correction.nfId;
		correctionModel.UPDATE(objToUpdate);
	}
	return response;
};

this.getConfigurations = function(register) {
	let schema;
	let mandt;
	let coreConfigModel = $.createBaseRuntimeModel($.schema.slice(1, -1), 'CORE::CONFIGURATION', false, true);
	let configuration = coreConfigModel.find({
		select: [{
			field: 'key'
        }, {
			field: 'value'
        }],
		where: [{
			field: 'key',
			operator: '$in',
			value: ['ECC::CLIENT', 'TIMP::TDFIntegration', 'TDF::SCHEMA']
        }]
	}).results;
	_.forEach(configuration, function(item) {
		switch (item.key) {
			case 'ECC::CLIENT':
				mandt = item.value;
				break;
			case 'TIMP::TDFIntegration':
				// tdf = [true, 'true'].indexOf(item.value) !== -1;
				break;
			case 'TDF::SCHEMA':
				schema = item.value;
				break;
		}
	});
	return {
		schema: schema,
		mandt: mandt
	};
};

this.getCorrectionById = function(id, isAccounting) {
	return _.isNumber(id) || Array.isArray(id) || !isNaN(Number(id)) ?
		this.read({
			id,
			status: isAccounting ? [2] : [1, 4]
		}) : id;
};

this.isValidCorrection = function(correction, isAccounting) {
	return (!_.isNil(correction) &&
		(
			correction.status === 'PENDING' ||
			correction.status === 'ERROR' ||
			correction.status === 'IN PROGRESS' ||
			(
				isAccounting && // correction.status === 'CORRECTED' &&
				(
					correction.correctionType === 1 ||
					correction.correctionType === 'TMFFIPostingPostSyncIn' ||
					correction.accounting === 'R'
				)
			)
		)
	);
};

this.hasTDF = function() {
	return coreApi.configController.hasTDFIntegration();
};

this.recalculateDynamicValues = function(fields = []) {
	fields.forEach((field = {}) => {
		if (field.dynamicValue === 'TODAY') {
			field.newValue = Date.toTimpDate(new Date());
		}
	});
};

this.updateDynamicValues = function(fields = []) {
	try {
		fields.forEach((field = {}) => {
			if (field.dynamicValue === 'TODAY') {
				fieldByCorrectionModel.UPDATE(field);
				$.messageCodes.push({
					code: 'BSC201040',
					type: 'S'
				});
			}
		});
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201039',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		// Supervisor.callCorrectionServiceError(correction && correction.id ? correction.id : null, util.parseError(e), response.xmlSent, response.xmlResponse);
	}
};

this.approveByTDF = function(correction, adapterId, location, isShadow, isHttpsActive) {
	var response = {
		xmlSent: '',
		xmlResponse: '',
		hasToBeApprovedByJob: false,
		isShadow: isShadow,
		hasError: true
	};
	let _self = this;
	try {
		var oldObject = _this.read({
			id: correction.id
		});
		var corrections = [];
		var mandt = '';
		if (_this.lvmandt) {
			mandt = _this.lvmandt;
		} else {
			mandt = coreApi.getSystemConfiguration({
				componentName: 'CORE',
				keys: ['ECC::CLIENT']
			})[0].value;
			_this.lvmandt = mandt;
		}
		$.import('timp.atr.server.api', 'api');
		let structureFieldsMap = {};
		const atrApi = $.timp.atr.server.api.api;
		var ctrNfStructure = atrApi.structure.table.READ({
			field: ['structure'],
			where: [{
				field: 'title',
				oper: '=',
				value: CORRECTION_STRUCTURE
            }]
		})[0];
		_.forEach(JSON.parse(ctrNfStructure.structure).fields, function(field) {
			structureFieldsMap[field.hanaName] = field;
		});
		_.forEach(correction.fields, function(field) {
			field = _self.formatDate(field, structureFieldsMap);
			var fieldNameXML = _this.fieldIsInTDFScenario(field.fieldName) || field.fieldName;
			if (!fieldNameXML) {
				response.hasToBeApprovedByJob = true;
				return;
			}
			var nfitem = correction.numItem;
			var isHeader = _this.fieldIsFromHeader(field.fieldName);
			if (isHeader) {
				nfitem = '';
			}

			//SPECIAL CASES
			switch (field.fieldName) {
				case 'TAXTYP':
				case 'TAXGRP':
					if (!isShadow) {
						return;
					} //if not shadow don't push the correction
			}

			corrections.push({
				'nfitem': nfitem,
				'fieldname': fieldNameXML,
				'oldvalue': field.oldValue,
				'newvalue': field.newValue
			});
		});
		if (corrections.length > 0 && !response.hasToBeApprovedByJob) {
			var version = '';
			var componentInformation = util.getComponentInformation('CORE');
			if (componentInformation && componentInformation.results && componentInformation.results[0] && componentInformation.results[0].VERSION) {
				version = componentInformation.results[0].VERSION;
			}
			//Call default service
			var ppid = '';
			if (_this.ppids.has(adapterId)) {
				ppid = _this.ppids.get(adapterId);
			} else {
				ppid = coreApi.getPPID(adapterId);
				this.ppids.set(adapterId, ppid);
			}
			coreApi.setMessageSyncIn(adapterId, ppid, 'BSC', version, 'TMFNotaFiscalCorrectionsSyncIn');
			var serviceCallObject = {
				'tmf:NotaFiscalCorrection': {
					'mandt': mandt,
					'nfid': correction.nfId,
					'reason': correction.reason,
					'shadow': isShadow ? 'X' : '',
					'CORRECTIONS': corrections,
					'MARKLOG': {
						'ppid': ppid,
						'timestamp': _this.getCurrentTimestamp(),
						'progmodule': 'TIMP',
						'progversion': version,
						'username': $.session.getUsername(),
						'status': 'I',
						'text': correction.justification || '---'
					}
				}
			};
			var xml = util.converterJSON({
				json: serviceCallObject
			});
			if (_.isString(xml)) {
				xml = xml.replace(/&#x2F;/g, '/');
			}
			response.xmlSent = xml;
			//call service
			var externalCallResponse = coreApi.executeTDFExternalService('POST', adapterId, location, xml, null, isHttpsActive,
				'TMFNotaFiscalCorrectionsSyncIn');
			if (externalCallResponse && externalCallResponse.xml) {
				response.xmlResponse = externalCallResponse.xml;
				if (externalCallResponse.isSuccessful) {
					let modificationUser = $.getUserID();
					let modificationDate = new Date();
					let newObject = {
						id: correction.id,
						pendingJob: 0,
						approvalType: 1,
						status: 2,
						modificationUser: modificationUser,
						modificationDate: modificationDate
					};
					response.successful = true;
					response.hasError = false;
					if (externalCallResponse.xml && externalCallResponse.xml.indexOf && externalCallResponse.xml.indexOf('<TYPE>E</TYPE>') !== -1) {
						response.hasError = true;
					}
					Supervisor.updateCorrection(oldObject, newObject);
				}
				// if (!externalCallResponse.isSuccessful) {
				//     var modificationUser = $.getUserID();
				//     var modificationDate = new Date();
				//     response.hasError = true;
				//     correctionModel.UPDATE({
				//         id: correction.id,
				//         status: 4,
				//         modificationUser: modificationUser,
				//         modificationDate: modificationDate
				//     });
				//     var newObject = _this.read({
				//         id: correction.id
				//     });
				//     Supervisor.updateCorrection(oldObject, newObject);
				// }
				Supervisor.callCorrectionService(correction.id, response.xmlSent, response.xmlResponse);
				// } else {
				//     var modificationUser = $.getUserID();
				//     var modificationDate = new Date();
				//     correctionModel.UPDATE({
				//         id: correction.id,
				//         status: 4,
				//         modificationUser: modificationUser,
				//         modificationDate: modificationDate
				//     });
				//     var newObject = _this.read({
				//         id: correction.id
				//     });
				//     Supervisor.updateCorrection(oldObject, newObject);
			}
		}
		// if (response.hasToBeApprovedByJob && !response.hasError) {
		if ((response.hasError && isShadow) || response.xmlResponse === '' || response.hasToBeApprovedByJob) {
			//update column pendingJob
			// let canApproveByJob = this.verifyPeriodOpen(correction.nfId || 0, correction.id);
			let canApproveByJob = null;
			if (_this.canApproveByJobs.has(correction.nfId)) {
				canApproveByJob = _this.canApproveByJobs.get(correction.nfId);
			} else {
				canApproveByJob = this.verifyPeriodOpen(correction.nfId || 0, correction.id);
				_this.canApproveByJobs.set(correction.nfId, canApproveByJob);
			}
			if (!canApproveByJob) {
				let modificationUser = $.getUserID();
				let modificationDate = new Date();
				response.hasError = true;
				Supervisor.approveCorrectionError(correction.id || null, 'Period Status Not Allowed', 'BSC201079');
				correctionModel.UPDATE({
					id: correction.id,
					status: 4,
					wasCorrected: 0,
					modificationUser: modificationUser,
					modificationDate: modificationDate
				});
				$.messageCodes.push({
					code: 'BSC201079',
					type: 'E',
					errorInfo: 'Period Status Not Allowed'
				});
			} else {
				response.hasToBeApprovedByJob = true;
				let modificationUser = $.getUserID();
				let modificationDate = new Date();
				correctionModel.UPDATE({
					id: correction.id,
					pendingJob: 1,
					status: 5, //IN PROGRESS
					wasCorrected: 0,
					adapterId: adapterId,
					modificationUser: modificationUser,
					modificationDate: modificationDate
				});
				let newObject = _this.read({
					id: correction.id
				});
				Supervisor.updateCorrection(oldObject, newObject);
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201069',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		Supervisor.callCorrectionServiceError(correction && correction.id ? correction.id : null, util.parseError(e), response.xmlSent, response.xmlResponse);
	}
	return response;
};

this.verifyPeriodOpen = function(nfId, id) {
	if (!nfId) {
		return false;
	}

	$.import('timp.nf.server.api', 'api');
	let nfApi = $.timp.nf.server.api.api.notafiscal;
	let notaFiscal = nfApi.listNF({
		removePaginate: true,
		id_documento: nfId
	});
	if (notaFiscal && notaFiscal.data && notaFiscal.data.length) {
		let data = notaFiscal.data[0];
		if (data.dt_lancto && data.dt_lancto.length === 8 && data.dt_lancto !== '00000000') {
			$.import('timp.tfp.server.api', 'api');
			let tfpApi = $.timp.tfp.server.api.api.fiscalPeriodController;
			let period = tfpApi.fiscalPeriodList({
				skipPagination: true,
				idCompany: [data.cod_emp],
				idBranch: [data.cod_filial],
				year: data.dt_lancto.substring(0, 4),
				month: data.dt_lancto.substring(4, 6),
				filterBy: {}
			});
			let status = period && period.data && period.data[0] && period.data[0].status || '000';
			if (['100', '200', '500', '600'].indexOf(status) !== -1) {
				return true;
			}
		}
	}
	return false;
};

this.getCurrentTimestamp = function() {
	var now = new Date();
	var timestamp = (now.getFullYear() + '' +
		(((now.getMonth() + 1) < 10) ? ('0' + (now.getMonth() + 1)) : (now.getMonth() + 1)) + '' +
		((now.getDate() < 10) ? ('0' + now.getDate()) : (now.getDate())) + '' +
		((now.getHours() < 10) ? ('0' + now.getHours()) : (now.getHours())) + '' +
		((now.getMinutes() < 10) ? ('0' + now.getMinutes()) : (now.getMinutes())) + '' +
		((now.getSeconds() < 10) ? ('0' + now.getSeconds()) : (now.getSeconds())));
	return timestamp;
};

this.fieldIsInDCorrCust = function(fieldName, mandt, nfId, numItem) {
	var response = false;
	try {
		var nfValues = _this.getNfValues(nfId, numItem, ['UF'], mandt);
		var where = [{
			field: 'mandt',
			oper: '=',
			value: mandt
        }, {
			field: 'fieldName',
			oper: '=',
			value: fieldName
        }];
		if (nfValues && nfValues.UF) {
			where.push([{
				field: 'regio',
				oper: '=',
				value: nfValues.UF
            }, {
				field: 'regio',
				oper: '=',
				value: ''
            }]);
		}
		var field = cvCorrectionFieldsConfigurationModel.READ({
			where: where
		})[0];
		if (field && field.enable === 'X') {
			response = true;
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201070',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.fieldIsFromHeader = function(fieldName) {
	var response = false;
	try {
		$.import('timp.atr.server.api', 'api');
		const atrApi = $.timp.atr.server.api.api;
		var ctrNfStructure = atrApi.structure.table.READ({
			field: ['structure'],
			where: [{
				field: 'title',
				oper: '=',
				value: CORRECTION_STRUCTURE
            }]
		})[0];
		if (ctrNfStructure) {
			var json = JSON.parse(ctrNfStructure.structure);
			var fields = _.filter(json.fields, function(field) {
				return field.shadowTable === '/TMF/D_NF_DOC' && field.hanaName === fieldName;
			});
			if (fields.length > 0) {
				response = true;
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201071',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.fieldIsInTDFScenario = function(fieldName, ctrNfStructure) {
	var response = false;
	//     try{
	//         var exists = cvCorrectionFieldsModel.READ({
	//             where:[{field:'fieldname',oper:'=',value:fieldName}]
	//         })[0];
	//         if(exists){
	//             response = true;
	//         }
	//     }catch(e){
	//         $.messageCodes.push({
	//          code: "BSC201070",
	//          type: "E",
	//          errorInfo: util.parseError(e)
	//      });
	//     }
	try {
		$.import('timp.atr.server.api', 'api');
		const atrApi = $.timp.atr.server.api.api;
		ctrNfStructure = ctrNfStructure || atrApi.structure.table.READ({
			field: ['structure'],
			where: [{
				field: 'title',
				oper: '=',
				value: CORRECTION_STRUCTURE
            }]
		})[0];
		if (ctrNfStructure) {
			var json = JSON.parse(ctrNfStructure.structure);
			var field = _.filter(json.fields, function(f) {
				return f.hanaName === fieldName;
			})[0];
			if (field) {
				response = field.shadowNameXML;
			} else {
				//SPECIAL CASES
				switch (fieldName) {
					case 'TAXTYP':
						response = 'SYS_IMPOSTO';
						break;
					case 'TAXGRP':
						response = 'TIPO_IMPOSTO';
						break;
				}
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201070',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/*
    this function will return true if the period is open (100), calculating(200) or recalculating  (500)
    the field which determines the period of the NF is DT_DOC (Emission Date)
    the function will verify through the NF_ID, NUM_ITEM, and TFP Service if the Period is in those status
*/
// this.periodIsAvailableForCorrections = function(nfId, numItem) {
//     var response = false;
//     try {

//     } catch (e) {
//         $.messageCodes.push({
//             code: 'BSC201072',
//             type: 'E',
//             errorInfo: util.parseError(e)
//         });
//     }
//     return response;
// };
this.approveByJob = function() {
    _this.approveByJobCorrection();
    _this.approveByJobAccounting();
};

this.approveByJobAccounting = function() {
	var response = [];
	var time = {
		start: $.moment()
	};

	try {
		// Accounting
		let pendingList = correctionModel.READ({
			where: [{
					field: 'pendingJob',
					oper: '=',
					value: 1
                        }, {
					field: 'accounting',
					oper: '=',
					value: 'R'
                        }, {
					field: 'isCorrection',
					oper: '!=',
					value: 1
                        }, {
					field: 'status',
					oper: '=',
					value: 5
                        },
                        [{
					field: 'correctionType',
					oper: '=',
					value: [0, 1, 4]
                        }, {
					field: 'correctionType',
					oper: 'IS NULL'
                        }]
                    ],
			paginate: {
				size: NUMBER_OF_ACCOUNTINGS_PER_JOB,
				number: 1,
				count: false
			}
		});
		time.pendingListAccounting = $.moment().diff(time.start);
		_.forEach(pendingList, function(correction) {
		    try {
		    	var host = '';
		    	var port = '';
		    	var adapterId = null;
		    	var serviceUrl = '';
		    	var urlResponse = _this.getServiceURL('TMFFIReversalPostSyncIn');
		    	host = urlResponse.host;
		    	port = urlResponse.port;
		    	serviceUrl = urlResponse.serviceUrl;
		    	adapterId = urlResponse.adapterId;
		    	var location = host + ':' + port + serviceUrl;
		    	let accounting = _this.accounting({
		    		id: [correction.id],
		    		adapterId: adapterId,
		    		location: location,
		    		isShadow: false
		    	});
		    	if (accounting && accounting[0] && accounting[0].xmlResponse && accounting[0].xmlResponse.indexOf && accounting[0].xmlResponse.indexOf(
		    		'<TYPE>S</TYPE>') !== -1) {
		    		_this.updateStatus({
		    			accounting: [accounting[0].id],
		    			success: [],
		    			error: [],
		    			accountingError: []
		    		});
		    	}
		    	if (accounting && accounting[0] && accounting[0].xmlResponse && accounting[0].xmlResponse.indexOf && accounting[0].xmlResponse.indexOf(
		    		'<TYPE>E</TYPE>') !== -1) {
		    		_this.updateStatus({
		    			accounting: [],
		    			success: [],
		    			error: [],
		    			accountingError: [accounting[0].id]
		    		});
		    	}
		    	response.push(accounting);
		    } catch (e) {
		    	_this.updateStatus({
		    		accounting: [],
		    		success: [],
		    		error: [],
		    		accountingError: [correction.id]
		    	});
		    }
		});
		time.approveDirectlyAccounting = $.moment().diff(time.start);
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201075',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	time.end = $.moment();
	return {
		response: response,
		time: time
	};
};

this.approveByJobCorrection = function(object) {
	if (object && object.NUMBER_OF_CORRECTIONS_PER_JOB) {
		NUMBER_OF_CORRECTIONS_PER_JOB = object.NUMBER_OF_CORRECTIONS_PER_JOB;
	}
	var response = [];
	var time = {
		start: $.moment()
	};
	try {
		// Correction
		let pendingList = correctionModel.READ({
			where: [{
					field: 'pendingJob',
					oper: '=',
					value: 1
                        }, [[[{
					field: 'accounting',
					oper: '!=',
					value: 'R'
                        }, {
					field: 'accounting',
					oper: 'IS NULL'
                        }]], [{
					field: 'accounting',
					oper: '=',
					value: 'R'
                        }, {
					field: 'isCorrection',
					oper: '=',
					value: 1
                        }]], {
					field: 'correctionType',
					oper: '=',
					value: [0, 1, 2, 3]
                        },
                        [{
					field: 'status',
					oper: '=',
					value: 'ERROR'
                        }, {
					field: 'status',
					oper: '=',
					value: 'IN PROGRESS'
                        }, {
					field: 'status',
					oper: '=',
					value: 4
                        }, {
					field: 'status',
					oper: '=',
					value: 5
                        }]
                    ],
			paginate: {
				size: NUMBER_OF_CORRECTIONS_PER_JOB,
				number: 1,
				count: false
			}
		});
		if (pendingList.length) {
			pendingList = correctionModel.READ({
				where: [{
						field: 'id',
						oper: '=',
						value: pendingList.map(function(item) {
							return item.id;
						})
                            }
                        ],
				join: _this.getJoin()
			});
		}
		time.pendingListCorrection = $.moment().diff(time.start);
		if (pendingList && pendingList.length) {
			$.import('timp.atr.server.api', 'api');
			const atrApi = $.timp.atr.server.api.api;
			_this.checkCorrectionScenarioForStructureID(pendingList);
			var ctrNfStructure = atrApi.structure.table.READ({
				field: ['structure'],
				where: [{
					field: 'title',
					oper: '=',
					value: CORRECTION_STRUCTURE
                        }]
			})[0];
			var nfValuesFound = _this.nGetMultipleNfValues(ctrNfStructure, pendingList);
			var meta = _this.getMetaInfo()
			_.forEach(pendingList, function(correction) {
				let specificScenarioStructure = false;
				if (correction.structureId) {
					specificScenarioStructure = atrApi.structure.table.READ({
						field: ['structure'],
						where: [{
							field: 'id',
							oper: '=',
							value: correction.structureId
        				}]
					})[0];
				}
				response.push(_this.approveDirectly(correction, (specificScenarioStructure || ctrNfStructure), nfValuesFound, meta));
			});
		}
		time.approveDirectlyCorrection = $.moment().diff(time.start);
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201065',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}

	time.end = $.moment();
	return {
		response: response,
		time: time
	};
};

this.formatDate = function(field, structureFieldsMap) {
	if (structureFieldsMap[field.fieldName] && structureFieldsMap[field.fieldName].type === 'TIMESTAMP') {
		let newValue = field.newValue.split('/');
		if (newValue.length !== 3) {
			newValue = field.newValue.split('-');
		}
		if (newValue.length === 3) {
			if (newValue[0] && newValue[0].length === 4) {
				newValue = newValue[0] + newValue[1] + newValue[2];
			} else if (newValue[2] && newValue[2].length === 4) {
				newValue = newValue[2] + newValue[1] + newValue[0];
			}
			field.newValue = _.isString(newValue) && newValue.length ? newValue : field.newValue;
		}
	}
	return field;
};

this.getMetaInfo = function() {
	var meta = {
		mandt: '',
		tdfSchema: ''
	};
	try {
		var temp = coreApi.OrmHighPerformance.__runSelect__({
			query: `SELECT "VALUE" FROM "${$.schema.slice(1, -1)}"."CORE::CONFIGURATION" WHERE "KEY" = ? OR "KEY" = ? ORDER BY "KEY"`,
			values: ['ECC::CLIENT', 'TDF::SCHEMA'],
			returnType: 'Array'
		});
		if (temp && temp.length) {
			meta = {
				mandt: temp[0][0] || '',
				tdfSchema: temp[1][0] || ''
			};
		}
	} catch (e) {
		var x = 'x';
	}
	meta.placeHolders = coreApi.resources.getStringFormatPlaceHolders();
	var urlResponse = _this.getServiceURL('TMFNotaFiscalCorrectionsSyncIn');
	var host = urlResponse.host;
	var port = urlResponse.port;
	var serviceUrl = urlResponse.serviceUrl;
	var adapterId = urlResponse.adapterId;
	var location = host + ':' + port + serviceUrl;
	meta.location = location;
	meta.adapterId = adapterId;
	meta.ppid = coreApi.getPPID(adapterId);

	return meta;
}

this.callServiceFromJob = function(correction, META) {
	var _self = this;
	var wasCorrectedByJob = false;
	var nf = correction.nfId;
	var item = correction.numItem;
	var reason = correction.reason;
	var mandt = META.mandt;
	var adapterId = META.adapterId;
	var location = META.location;
	var ppid = META.ppid;
	var response = {
		approved: {},
		wasCorrectedByJob: false
	}
	try {
		var corrections = correction.fields.map(function(correction) {
			return {
				'nfitem': item,
				'fieldname': correction.fieldName,
				'oldvalue': correction.oldValue,
				'newvalue': correction.newValue
			}
		});
		var serviceCallObject = {
			'tmf:NotaFiscalCorrection': {
				'mandt': mandt,
				'nfid': nf,
				'reason': reason,
				'shadow': '',
				'CORRECTIONS': corrections,
				'MARKLOG': {
					'ppid': ppid,
					'timestamp': _this.getCurrentTimestamp(),
					'progmodule': 'TIMP',
					'progversion': '',
					'username': $.session.getUsername(),
					'status': 'I',
					'text': '---'
				}
			}
		};

		var xml = util.converterJSON({
			json: serviceCallObject
		});
		if (_.isString(xml)) {
			xml = xml.replace(/&#x2F;/g, '/');
		}
		var xmlSent = xml;
		//call service
		let isHttpsActive = this.getHttpsActivity('TMFNotaFiscalCorrectionsSyncIn');
		var externalCallResponse = coreApi.executeTDFExternalService('POST', adapterId, location, xml, null, isHttpsActive,
			'TMFNotaFiscalCorrectionsSyncIn')
		if (externalCallResponse && externalCallResponse.xml) {
			var json = util.converterXML({
				xml: externalCallResponse.xml
			});
			if (externalCallResponse.xml && externalCallResponse.xml.indexOf && externalCallResponse.xml.indexOf('<TYPE>S</TYPE>') !== -1) {
				response.approved = correctionModel.UPDATE({
					id: correction.id,
					pendingJob: 0,
					status: 2, //CORRECTED
					wasCorrected: 0,
					modificationUser: $.getUserID(),
					modificationDate: new Date()
				});
				wasCorrectedByJob = true;
				Supervisor.callCorrectionService(correction.id, xmlSent, externalCallResponse.xml);
			} else {
				wasCorrectedByJob = false;
			}
		}
	} catch (e) {
		var x = 'x';
		wasCorrectedByJob = false;
	}
	response.wasCorrectedByJob = wasCorrectedByJob;
	return response;

};

this.approveDirectly = function(correction, ctrNfStructure, nfValuesFound, META) {
	var time = {
		start: $.moment()
	};
	$.import('timp.atr.server.api', 'api');
	const atrApi = $.timp.atr.server.api.api;
	time.importATRAPI = $.moment().diff(time.start);
	try {
		let _self = this;
		var ctrNfStructure = ctrNfStructure || atrApi.structure.table.READ({
			field: ['structure'],
			where: [{
				field: 'title',
				oper: '=',
				value: CORRECTION_STRUCTURE
                    }]
		})[0];
		time.readStructureTable = $.moment().diff(time.start);
		var mandt = META.mandt || coreApi.getSystemConfiguration({
			componentName: 'CORE',
			keys: ['ECC::CLIENT']
		})[0].value;
		var response = {
			messages: [],
			approved: false
		};
		var tdfSchema = META.tdfSchema || coreApi.getSystemConfiguration({
			componentName: 'CORE',
			keys: ['tdfSchemaName', 'TDF::SCHEMA']
		})[0].value;
		if (_.isNil(tdfSchema)) {
			throw 'TDF Schema is not defined';
		}

		let corrections = [];
		let taxFields = {
			'TAXTYP': 'SYS_IMPOSTO',
			'TAXGRP': 'TIPO_IMPOSTO'
		};
		let taxtUpsert = {};
		// corrections.push({
		//     fieldName: 'NF_ID',
		//     oldValue: correction.nfId,
		//     newValue: correction.nfId
		// });
		let structureFieldsMap = {};
		let isFcp = false;
		_.forEach(JSON.parse(ctrNfStructure.structure).fields, function(field) {
			structureFieldsMap[field.hanaName] = field;
		});

		// 		CALL SERVICE
		time.callServiceStart = $.moment().diff(time.start);
		var callServiceFromJob = _self.callServiceFromJob(correction, META);
		time.callServiceEnd = $.moment().diff(time.start);
		// END CALL SERVICE
		if (callServiceFromJob.wasCorrectedByJob) {
			response.approved = callServiceFromJob.approved;
			return response;
		}

		time.mapStructureFields = $.moment().diff(time.start);
		_.forEach(correction.fields, function(field) {
			field = _self.formatDate(field, structureFieldsMap);
			isFcp = isFcp || (field && field.fieldName && field.fieldName.indexOf && field.fieldName.indexOf('FCP_DIFAL') !== -1);
			if (taxFields[field.fieldName]) {
				taxtUpsert[taxFields[field.fieldName]] = field.newValue;
			} else {
				corrections.push(field);
			}
		});
		time.mapCorrectionFields = $.moment().diff(time.start);
		var taxGroupTaxTypeValidation = [];
		var count = 0;
		var corrected = [];
		let upserts = [];
		let upsertsMap = {};
		let noMapFields = [];
		time.shadowTableName = [];
		time.fieldShadowName = [];
		time.getGroupTypeTax = [];
		time.getTaxFieldsValues = [];
		time.getShadowTableFieldsByField = [];
		time.getNfValues = [];
		let numberTypes = ['INTEGER', 'DECIMAL', 'DOUBLE', 'DECIMAL'];
		// 		START OPTIMIZATION
		time.startOp = $.moment().diff(time.start);
		var correctionsFieldsAsArray = [];
		var groupTypeTaxAsArray = [];
		var shadowTableNameMappedByField = {};
		var fieldShadowNameMappedByField = {};
		var groupTypeTaxMappedByField = {};
		var taxFieldsValuesMappedByField = {};
		corrections.forEach(function(fieldInfo) {
			let field = fieldInfo.fieldName;
			correctionsFieldsAsArray.push(field);
			shadowTableNameMappedByField[field] = _this.getShadowTableByField(field, ctrNfStructure) || '';
			fieldShadowNameMappedByField[field] = _this.getShadowNameByHanaName(field, ctrNfStructure) || '';
			if (shadowTableNameMappedByField[field] === '/TMF/D_NF_IMPOST') {
				groupTypeTaxAsArray.push(field);
				groupTypeTaxMappedByField[field] = {
					fields: [],
					messages: [],
					standard: [],
					hasError: false
				};
				taxFieldsValuesMappedByField[field] = {};
			}
		});
		if (groupTypeTaxAsArray.length) {
			groupTypeTaxMappedByField = _this.nGetGroupTypeTax(groupTypeTaxAsArray, groupTypeTaxMappedByField);
			time.OPnGetGroupTypeTax = $.moment().diff(time.start);
		}
		time.endOp = $.moment().diff(time.start);
		// 		END OPTIMIZATION

		//return correction;
		_.forEach(corrections, function(field) {
			let isInDScenario = false; //_this.fieldIsInTDFScenario(field.fieldName, ctrNfStructure);
			if (!isInDScenario || field.fieldName === 'CANCELADO') {
				let shadowTableName = shadowTableNameMappedByField[field.fieldName] || _this.getShadowTableByField(field.fieldName, ctrNfStructure);
				time.shadowTableName.push($.moment().diff(time.start));
				let fieldShadowName = fieldShadowNameMappedByField[field.fieldName] || _this.getShadowNameByHanaName(field.fieldName, ctrNfStructure);
				time.fieldShadowName.push($.moment().diff(time.start));
				let taxFieldsMap = {};
				if (shadowTableName === '/TMF/D_NF_IMPOST') {
					taxFieldsMap = groupTypeTaxMappedByField[field.fieldName] || _this.getGroupTypeTax(field.fieldName);
					time.getGroupTypeTax.push($.moment().diff(time.start));
					if (taxFieldsMap.hasError || (_.isEmpty(taxFieldsMap.fields) && _.isEmpty(taxFieldsMap.standard))) {
						noMapFields.push({
							field: field.fieldName,
							messages: taxFieldsMap.messages
						});
						return;
					}
					taxFieldsMap = _this.nGetTaxFieldsValues({
						MANDT: mandt,
						NF_ID: correction.nfId,
						NUM_ITEM: correction.numItem,
						map: taxFieldsMap
					}, META);
					time.getTaxFieldsValues.push($.moment().diff(time.start));
					if (_.isNil(taxFieldsMap.TAXGRP) || _.isNil(taxFieldsMap.TAXTYP)) {
						noMapFields.push({
							field: field.fieldName
						});
						return;
					}
				}
				let key = correction.nfId + '-' + correction.numItem + '-' + taxFieldsMap.TAXGRP + '-' + taxFieldsMap.TAXTYP;
				if (!_.isNil(upsertsMap[key])) {
					upserts[upsertsMap[key]].values[fieldShadowName] = field.newValue;
					upserts[upsertsMap[key]].correctionValues[fieldShadowName] = numberTypes.indexOf(structureFieldsMap[field.fieldName].type) !== -1 ?
						(Number(field.newValue) || coreApi.legacyUtil.currencyToDecimalFormat(field.newValue || '')) : field.newValue;
					return;
				}
				// if (!_.isNil(upserts[shadowTableName])) {
				//     upserts[shadowTableName][fieldShadowName] = field.newValue;
				//     return;
				// }
				var fieldNames = _this.getShadowTableFieldsByField(field.fieldName, ctrNfStructure);
				time.getShadowTableFieldsByField.push($.moment().diff(time.start));
				fieldNames.push('MANDT');
				count++;
				//get necessary fields according to shadow table
				//get nf values
				let nfvalueFound;
				if (nfValuesFound && nfValuesFound[shadowTableName]) {
					nfvalueFound = nfValuesFound[shadowTableName][correction.nfId + '_' + correction.numItem] || null;
				}
				let usedCacheTable = false;
				if (!$.lodash.isEmpty(taxGroupTaxTypeValidation)) {
					for (var x = 0; x < taxGroupTaxTypeValidation.length; x++) {
						if (taxGroupTaxTypeValidation[x] === key) {
							usedCacheTable = true;
							x = taxGroupTaxTypeValidation.length;
						}
					}
				}
				if (usedCacheTable) {
					var nfValues = nfvalueFound;
				} else {
					nfValues = _this.nGetNfValues(correction.nfId, correction.numItem, fieldNames, mandt, taxFieldsMap, META);
					taxGroupTaxTypeValidation.push(key);
				}
				time.getNfValues.push($.moment().diff(time.start));
				let correctionValues = {};
				correctionValues[fieldShadowName] = numberTypes.indexOf(structureFieldsMap[field.fieldName].type) !== -1 ? Number(field.newValue) :
					field.newValue;
				if (!correctionValues[fieldShadowName]) {
					if (field.newValue && field.newValue.length && (typeof field.newValue === 'string')) {
						var splitValue = field.newValue.split(',');
						var fieldNewValueTemp = splitValue[0].replace(/\./g, '');
						fieldNewValueTemp = fieldNewValueTemp + '.' + splitValue[1];
						fieldNewValueTemp = Number(fieldNewValueTemp);
						correctionValues[fieldShadowName] = fieldNewValueTemp ? fieldNewValueTemp : 0;
						field.newValue = fieldNewValueTemp ? fieldNewValueTemp : 0;
					}
				}
				//replace field value with newValue
				nfValues[fieldShadowName] = field.newValue;
				if (shadowTableName === '/TMF/D_NF_IMPOST') {
					nfValues.SYS_IMPOSTO = taxFieldsMap.TAXTYP;
					nfValues.TIPO_IMPOSTO = taxFieldsMap.TAXGRP;
					if (isFcp) {
						nfValues.SUBDIVISAO = '004';
					}
				}
				//call upsert where nfId and numItem
				// upserts[shadowTableName] = nfValues;
				upsertsMap[key] = upserts.length;
				upserts.push({
					table: shadowTableName,
					values: nfValues,
					correctionValues: correctionValues
				});
			}
		});
		time.finishcorrection = $.moment().diff(time.start);
		response.corrected = corrected;
		var modificationUser = $.getUserID();
		var modificationDate = new Date();
		var objToUpdate = {
			id: correction.id,
			pendingJob: 0,
			approvalType: 2,
			modificationUser: modificationUser,
			modificationDate: modificationDate
		};
		if (noMapFields.length !== 0) {
			let log = {
				details: [],
				properties: [{
					property: 'field',
					enus: 'Field',
					ptrbr: 'Campo'
                        }, {
					property: 'message',
					enus: 'Message',
					ptrbr: 'Mensagem'
                        }],
				nfId: correction.nfId,
				numItem: correction.numItem
			};
			$.lodash.forEach(noMapFields, field => {
				let fieldLog = {
					enus: field.field,
					ptrbr: field.field
				};
				if (field.messages && field.messages.length) {
					$.lodash.forEach(field.messages, message => {
						log.details.push({
							field: fieldLog,
							message: message
						});
					});
				} else {
					log.details.push({
						field: fieldLog,
						message: {
							enus: 'Tax Group not parameterized for the field: ' + field.field,
							ptrbr: 'Grupo de Imposto não parametrizado para o campo: ' + field.field
						}
					});
				}
			});
			objToUpdate.status = 4; //ERROR
			objToUpdate.wasCorrected = 0;
			Supervisor.approveCorrectionError(correction.id, log);
			response.approved = correctionModel.UPDATE(objToUpdate);
			time.updateCorrectionModel = $.moment().diff(time.start);
			response.time = time;
			return response;
		}

		//	return upserts; 
		_.forEach(upserts, function(item) {
			let shadowTableName = item.table;
			let nfValues = item.values;
			if (!shadowTableName || !nfValues) {
				return;
			}
			var shadowObject = _this.mapToShadowObject(nfValues, ctrNfStructure);
			shadowObject.MANDT = shadowObject.MANDT ? shadowObject.MANDT : mandt;
			shadowObject.NF_ID = correction.nfId;
			shadowObject.NUM_ITEM = correction.numItem;
			shadowObject = correctCTRValues(shadowObject, META);
			_.forEach(item.correctionValues, function(correctionValue, correctionKey) {
				shadowObject[correctionKey] = correctionValue;
			});
			if ($.lodash.has(item.correctionValues, 'COD_NAT') || $.lodash.has(item.correctionValues, 'CFOP')) {
				if ($.lodash.has(item.correctionValues, 'COD_NAT')) {
					shadowObject.CFOP = item.correctionValues.COD_NAT.substr(0, 4);
				} else if (shadowObject.COD_NAT) {
					shadowObject.COD_NAT = shadowObject.COD_NAT.substr(4, 2) + item.correctionValues.CFOP;
				}
			}
			
			// shadowObject = _.assign(shadowObject, taxtUpsert);
			// verify if its neccesary to get tax group and tax type values
			var upserted = _this.upsertShadowTable(shadowTableName, correction.nfId, correction.numItem, shadowObject, tdfSchema);
			if (upserted && upserted.upserted) {
				corrected.push(upserted);
			}
		});
		time.upsertShadowTable = $.moment().diff(time.start);
		if (corrected.length === count && count !== 0) { //if all fields where corrected and correction status was pending (not error)
			objToUpdate.status = 2; //CORRECTED
			objToUpdate.wasCorrected = 1;
			Supervisor.approveCorrectionByShadow(correction);
		} else {
			objToUpdate.status = 4; //ERROR
			objToUpdate.wasCorrected = 0;
			Supervisor.approveCorrectionError(correction.id, {});
		}
		response.approved = correctionModel.UPDATE(objToUpdate);
	} catch (e) {
		if (correction && correction.id) {
			let modificationUser = $.getUserID();
			let modificationDate = new Date();
			response.approved = correctionModel.UPDATE({
				id: correction.id,
				pendingJob: 0,
				approvalType: 0,
				status: 4, //ERROR
				wasCorrected: 0,
				modificationUser: modificationUser,
				modificationDate: modificationDate
			});
			Supervisor.approveCorrectionError(correction.id, util.parseError(e));
		}
		$.messageCodes.push({
			code: 'BSC201069',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	response.time = time;
	return response;
};

this.nGetGroupTypeTax = function(groupTypeTaxAsArray, groupTypeTaxMappedByField) {
	let groupTypeTaxMappedByFieldTemp = {};
	let messages = {
		noMapping: {
			enus: 'Check Configuration of table BSC::CORRECTION_FIELD_MAPPING',
			ptrbr: 'Verificar configuração da tabela BSC::CORRECTION_FIELD_MAPPING'
		},
		standarFieldError: {
			enus: 'Check configuration of table BSC::CORRECTION_FIELD_MAPPING. More than one item marked as standard for the same field',
			ptrbr: 'Verificar configuração da tabela BSC::CORRECTION_FIELD_MAPPING. Mais de um item marcado como standard, para o mesmo campo'
		},
		multipleFieldError: {
			enus: 'Check configuration of table BSC::CORRECTION_FIELD_MAPPING. There are Tax Types for the same field and even group with definition missing for Multiple Types',
			ptrbr: 'Verificar configuração da tabela BSC::CORRECTION_FIELD_MAPPING. Existem Tipos de impostos para o mesmo campo e mesmo grupo com definição falta para Multiplos Tipos'
		}
	};
	let response = {
		fields: [],
		messages: [],
		standard: [],
		hasError: false
	};
	var selectFields = [
        'FIELD',
        'TAXGRP',
        'TAXTYP',
        'MULTTYP',
        'TYPSTD'
    ];
	var whereValues = groupTypeTaxAsArray.map(function(field) {
		return `'${field}'`
	})
	let query =
		`
        SELECT ${selectFields.join(', ')} 
        FROM "${$.schema.slice(1, -1)}"."BSC::CORRECTION_FIELD_MAPPING"
        WHERE "FIELD" IN (${whereValues.join(', ')})
    `;

	let executionObject = {
		query: query,
		returnType: 'Array'
	};
	let temp = coreApi.OrmHighPerformance.__runSelect__(executionObject);
	temp.forEach(function(result) {
		let object = {};
		let field = result[0];
		selectFields.forEach(function(field, i) {
			if (i > 0) {
				object[field] = result[i]
			}
		});
		groupTypeTaxMappedByFieldTemp[field] = groupTypeTaxMappedByFieldTemp[field] || [];
		groupTypeTaxMappedByFieldTemp[field].push(object);
	})
	groupTypeTaxAsArray.forEach(function(field) {
		let result = {
			results: []
		};
		if (groupTypeTaxMappedByFieldTemp[field] && groupTypeTaxMappedByFieldTemp[field].length) {
			result.results = groupTypeTaxMappedByFieldTemp[field];
		}
		if (result.results.length !== 0) {
			let fields = [];
			let isMultiple = false;
			$.lodash.forEach(result.results, map => {
				isMultiple = isMultiple || ([1, '1', true, 'true'].indexOf(map.MULTTYP) !== -1);
				map.TAXGRP = String(map.TAXGRP || '').trim();
				map.TAXTYP = String(map.TAXTYP || '').trim();
				if ([1, '1', true, 'true'].indexOf(map.TYPSTD) !== -1) {
					groupTypeTaxMappedByField[field].standard.push(map);
				} else {
					fields.push(map);
				}
			});
			if (groupTypeTaxMappedByField[field].standard.length > 1) {
				groupTypeTaxMappedByField[field].hasError = true;
				groupTypeTaxMappedByField[field].messages.push(messages.standarFieldError);
			}
			if (result.results.length > 1 && !isMultiple) {
				groupTypeTaxMappedByField[field].hasError = true;
				groupTypeTaxMappedByField[field].messages.push(messages.multipleFieldError);
			}
			if (!groupTypeTaxMappedByField[field].hasError) {
				groupTypeTaxMappedByField[field].fields = fields;
			}
		} else {
			groupTypeTaxMappedByField[field].hasError = true;
			groupTypeTaxMappedByField[field].messages.push(messages.noMapping);
		}
	})
	return groupTypeTaxMappedByField;
};

this.getGroupTypeTax = function(field) {
	const mappingTable = $.createBaseRuntimeModel($.schema.slice(1, -1), MAP_FIELD_TABLE);
	let messages = {
		noMapping: {
			enus: 'Check Configuration of table BSC::CORRECTION_FIELD_MAPPING',
			ptrbr: 'Verificar configuração da tabela BSC::CORRECTION_FIELD_MAPPING'
		},
		standarFieldError: {
			enus: 'Check configuration of table BSC::CORRECTION_FIELD_MAPPING. More than one item marked as standard for the same field',
			ptrbr: 'Verificar configuração da tabela BSC::CORRECTION_FIELD_MAPPING. Mais de um item marcado como standard, para o mesmo campo'
		},
		multipleFieldError: {
			enus: 'Check configuration of table BSC::CORRECTION_FIELD_MAPPING. There are Tax Types for the same field and even group with definition missing for Multiple Types',
			ptrbr: 'Verificar configuração da tabela BSC::CORRECTION_FIELD_MAPPING. Existem Tipos de impostos para o mesmo campo e mesmo grupo com definição falta para Multiplos Tipos'
		}
	};
	let response = {
		fields: [],
		messages: [],
		standard: [],
		hasError: false
	};
	let options = {
		select: [{
			field: 'TAXGRP'
        }, {
			field: 'TAXTYP'
        }, {
			field: 'MULTTYP'
        }, {
			field: 'TYPSTD'
        }],
		where: [{
			field: 'FIELD',
			operator: '$eq',
			value: field
        }]
	};

	let result = mappingTable.find(options);
	if (result.errors.length === 0 && result.results.length !== 0) {
		let fields = [];
		let isMultiple = false;
		$.lodash.forEach(result.results, map => {
			isMultiple = isMultiple || ([1, '1', true, 'true'].indexOf(map.MULTTYP) !== -1);
			map.TAXGRP = String(map.TAXGRP || '').trim();
			map.TAXTYP = String(map.TAXTYP || '').trim();
			if ([1, '1', true, 'true'].indexOf(map.TYPSTD) !== -1) {
				response.standard.push(map);
			} else {
				fields.push(map);
			}
		});
		if (response.standard.length > 1) {
			response.hasError = true;
			response.messages.push(messages.standarFieldError);
		}
		if (result.results.length > 1 && !isMultiple) {
			response.hasError = true;
			response.messages.push(messages.multipleFieldError);
		}
		if (!response.hasError) {
			response.fields = fields;
		}
	} else {
		response.hasError = true;
		response.messages.push(messages.noMapping);
	}
	return response;
};

this.nGetTaxFieldsValues = function(shadowObject, META) {
	let result = {};
	let groups = [];
	let groupsFormated = [];
	let types = [];
	let typesFormated = [];
	$.lodash.forEach(shadowObject.map.fields.concat(shadowObject.map.standard), item => {
		if (groups.indexOf(item.TAXGRP) === -1) {
			groups.push(item.TAXGRP);
			groupsFormated.push(`'${item.TAXGRP}'`);
		}
		if (types.indexOf(item.TAXTYP) === -1) {
			types.push(item.TAXTYP);
			typesFormated.push(`'${item.TAXTYP}'`);
		}
	});

	var selectFields = [
        'TAXGRP',
        'TAXTYP'
    ];
	let query =
		`
        SELECT TOP 1 ${selectFields.join(', ')} 
        FROM "${$.viewSchema}"."${GROUP_IMPOSTO_VIEW}" ${META.placeHolders}
        WHERE (
            "MANDT" = '${shadowObject.MANDT}'
            AND "DOCNUM" = '${shadowObject.NF_ID}'
            AND "ITMNUM" = '${shadowObject.NUM_ITEM}'
            AND "TAXGRP" IN (${groupsFormated.join(', ')} )
            AND "TAXTYP" IN (${typesFormated.join(', ')} )
        )
    `;

	let executionObject = {
		query: query,
		returnType: 'Array'
	};
	let temp = coreApi.OrmHighPerformance.__runSelect__(executionObject);
	temp = temp.map(function(register) {
		var obj = {};
		selectFields.forEach(function(field, i) {
			obj[field] = register[i];
		})
		return obj;
	})
	if (temp.length !== 0) {
		shadowObject.TAXGRP = temp[0].TAXGRP || groups[0];
		shadowObject.TAXTYP = temp[0].TAXTYP || (shadowObject.map.standard[0] && shadowObject.map.standard[0].TAXTYP || types[0]);
	} else {
		shadowObject.TAXGRP = groups[0];
		shadowObject.TAXTYP = shadowObject.map.standard[0] && shadowObject.map.standard[0].TAXTYP;
	}
	return shadowObject;
};

this.getTaxFieldsValues = function(shadowObject) {
	let result = {};
	let groups = [];
	let types = [];
	$.lodash.forEach(shadowObject.map.fields.concat(shadowObject.map.standard), item => {
		if (groups.indexOf(item.TAXGRP) === -1) {
			groups.push(item.TAXGRP);
		}
		if (types.indexOf(item.TAXTYP) === -1) {
			types.push(item.TAXTYP);
		}
	});
	let options = {
		select: [{
			field: 'TAXGRP'
        }, {
			field: 'TAXTYP'
        }],
		where: [{
			field: 'MANDT',
			operator: '$eq',
			value: shadowObject.MANDT
        }, {
			field: 'DOCNUM',
			operator: '$eq',
			value: shadowObject.NF_ID
        }, {
			field: 'ITMNUM',
			operator: '$eq',
			value: shadowObject.NUM_ITEM
        }, {
			field: 'TAXGRP',
			operator: '$in',
			value: groups
        }, {
			field: 'TAXTYP',
			operator: '$in',
			value: types
        }]
	};

	// let eccSchema = coreApi.getSystemConfiguration({
	//     componentName: "CORE",
	//     keys: ["tdfSchemaName", "TDF::SCHEMA"]
	// })[0];
	// eccSchema = eccSchema ? eccSchema.value : 'SAP_ECC_TIMP';
	// const impostoTable = $.createBaseRuntimeModel(eccSchema, 'J_1BNFSTX');
	const impostoTable = $.createBaseRuntimeModel($.viewSchema, GROUP_IMPOSTO_VIEW, true);
	result = impostoTable.find(options);
	if (result.errors.length === 0 && result.results.length !== 0) {
		shadowObject.TAXGRP = result.results[0].TAXGRP || groups[0];
		shadowObject.TAXTYP = result.results[0].TAXTYP || (shadowObject.map.standard[0] && shadowObject.map.standard[0].TAXTYP || types[0]);
	} else {
		shadowObject.TAXGRP = groups[0];
		shadowObject.TAXTYP = shadowObject.map.standard[0] && shadowObject.map.standard[0].TAXTYP;
	}
	return shadowObject;
};

/*
    receives an string (field hanaName)
    returns an array of hanaNames
*/
this.getShadowTableFieldsByField = function(fieldName, ctrNfStructure) {
	var response = [];
	try {
		$.import('timp.atr.server.api', 'api');
		const atrApi = $.timp.atr.server.api.api;
		ctrNfStructure = ctrNfStructure || atrApi.structure.table.READ({
			field: ['structure'],
			where: [{
				field: 'title',
				oper: '=',
				value: CORRECTION_STRUCTURE
            }]
		})[0];
		if (ctrNfStructure) {
			var json = JSON.parse(ctrNfStructure.structure);
			var field = _.filter(json.fields, function(f) {
				return f.hanaName === fieldName;
			})[0];
			var shadowTableName = field.shadowTable;
			var fieldList = _.filter(json.fields, function(f) {
				return f.shadowTable && (f.shadowTable === shadowTableName ||
					(['/TMF/D_NF_ITEM', '/TMF/D_NF_DOC'].indexOf(shadowTableName) !== -1 && f.hanaName === 'COD_PART'));
			});
			response = _.map(fieldList, function(f) {
				return f.hanaName || f.shadowName;
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201073',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/*
    nfId -> string
    numItem -> string
    fields -> Array of strings
*/
this.getNfValues = function(nfId, numItem, fields, mandt, values) {
	var response = {};
	try {
		if (_.isString(nfId) && _.isString(numItem) && _.isArray(fields) && !_.isEmpty(fields)) {
			var selectOptions = fields.filter(function(elem) {
				return taxFields.indexOf(elem) === -1;
			}).map(function(elem) {
				return {
					field: elem
				};
			});
			var selectTaxOptions = fields.filter(function(elem) {
				return taxFields.indexOf(elem) !== -1;
			}).map(function(elem) {
				return {
					field: elem
				};
			});

			var cvCtrNFModel = $.createBaseRuntimeModel('_SYS_BIC', CORRECTION_VIEW, true);
			let whereOptions = [{
				field: 'NF_ID',
				operator: '$eq',
				value: nfId
            }, {
				field: 'NUM_ITEM',
				operator: '$eq',
				value: numItem
            }, {
				field: 'MANDT',
				operator: '$eq',
				value: mandt
            }];
			if (values && values.SYS_IMPOSTO) {
				whereOptions.push({
					field: 'SYS_IMPOSTO',
					operator: '$eq',
					value: values.SYS_IMPOSTO
				});
			}
			if (values && values.TIPO_IMPOSTO) {
				whereOptions.push({
					field: 'TIPO_IMPOSTO',
					operator: '$eq',
					value: values.TIPO_IMPOSTO
				});
			}
			response = cvCtrNFModel.find({
				select: selectOptions,
				where: whereOptions
			}).results[0];
			if (selectTaxOptions && selectTaxOptions.length) {
				var cvTaxFieldsModel = $.createBaseRuntimeModel('_SYS_BIC', CORRECTION_VIEW_OLD, true);
				var taxFieldsResponse = cvCtrNFModel.find({
					select: selectTaxOptions,
					where: whereOptions
				}).results[0];
				response = $.lodash.assign(taxFieldsResponse || {}, response || {});
				response = $.lodash.assign(cvTaxFieldsModel || {}, response || {});
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201074',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.nGetNfValues = function(nfId, numItem, fields, mandt, values, META) {
	var response = {};
	try {
		if (_.isString(nfId) && _.isString(numItem) && _.isArray(fields) && !_.isEmpty(fields)) {
			var selectOptions = fields.filter(function(elem) {
				return taxFields.indexOf(elem) === -1;
			}).map(function(elem) {
				return {
					field: elem
				};
			});
			var selectTaxOptions = fields.filter(function(elem) {
				return taxFields.indexOf(elem) !== -1;
			}).map(function(elem) {
				return {
					field: elem
				};
			});
			selectOptions = selectOptions.concat(selectTaxOptions);
			let selectOptionsAsPlainArray = selectOptions.map(function(option) {
				return option.field;
			});
			let select = `SELECT ${selectOptionsAsPlainArray.join(', ')}`;
			let where = ` WHERE
                (NF_ID = ? AND NUM_ITEM = ?)
            `;
			let from = `FROM "${$.viewSchema}"."${CORRECTION_VIEW}"`;
			let placeHolders = META.placeHolders;
			let whereValues = [
                nfId,
                numItem
            ];
			if (values && values.SYS_IMPOSTO) {
				where += ' AND (SYS_IMPOSTO = ?) ';
				whereValues.push(values.SYS_IMPOSTO);
			}
			if (values && values.TIPO_IMPOSTO) {
				where += ' AND (TIPO_IMPOSTO = ?) ';
				whereValues.push(values.TIPO_IMPOSTO);
			}
			let query = select + ' ' + from + ' ' + placeHolders + ' ' + where;
			let executionObject = {
				query: query,
				values: whereValues,
				returnType: 'Array'
			};
			let temp = coreApi.OrmHighPerformance.__runSelect__(executionObject);
			if (temp && temp.length) {
				temp = temp[0];
			}
			selectOptions.forEach(function(field, index) {
				response[field.field] = temp[index];
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201074',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.nGetMultipleNfValues = function(ctrNfStructure, pendingList) {
	var response = {};
	try {

		let structureFieldsMap = {};
		let fieldsInShadowTable = {
			'/TMF/D_NF_ITEM': [],
			'/TMF/D_NF_DOC': [],
			'/TMF/D_NF_IMPOST': []
		};
		let shadowTableByCorrection = {
			'/TMF/D_NF_ITEM': {},
			'/TMF/D_NF_DOC': {},
			'/TMF/D_NF_IMPOST': {}
		};
		_.forEach(JSON.parse(ctrNfStructure.structure).fields, function(field) {
			structureFieldsMap[field.hanaName] = field;
		});
		pendingList.forEach(function(correction) {
			correction.fields.forEach(function(field) {
				let fieldName = field.fieldName;
				let shadowTable = structureFieldsMap[fieldName] && structureFieldsMap[fieldName].shadowTable || '';
				if (!_.isNil(fieldsInShadowTable[shadowTable])) {
					shadowTableByCorrection[shadowTable][correction.nfId + '_' + correction.numItem] = {};
				}
				if (!_.isNil(fieldsInShadowTable[shadowTable])) {
					fieldsInShadowTable[shadowTable] = _this.getShadowTableFieldsByField(fieldName, ctrNfStructure);
					fieldsInShadowTable[shadowTable].push('MANDT');
				}
			});
		});
		_.forEach(shadowTableByCorrection, function(shadowTable, table) {
			let docnumsInShadowtable = Object.keys(shadowTable);
			let fields = fieldsInShadowTable[table];
			if (!_.isEmpty(fields)) {
				var selectOptions = fields.filter(function(elem) {
					return taxFields.indexOf(elem) === -1;
				}).map(function(elem) {
					return {
						field: elem
					};
				});
				var selectTaxOptions = fields.filter(function(elem) {
					return taxFields.indexOf(elem) !== -1;
				}).map(function(elem) {
					return {
						field: elem
					};
				});
				selectOptions = selectOptions.concat(selectTaxOptions);
				let selectOptionsAsPlainArray = selectOptions.map(function(option) {
					return option.field;
				});
				let select = `SELECT NF_ID, NUM_ITEM, ${selectOptionsAsPlainArray.join(', ')}`;
				let where = '';
				let from = `FROM "${$.viewSchema}"."${CORRECTION_VIEW}"`;
				let placeHolders = coreApi.resources.getStringFormatPlaceHolders();
				let whereValues = [];
				if (docnumsInShadowtable && docnumsInShadowtable.length) {
					let correctionHeaderWhere = [];
					where = 'WHERE ';
					docnumsInShadowtable.forEach(function(correction) {
						let correctionSplited = correction.split('_');
						let nfId = correctionSplited[0];
						let numItem = correctionSplited[1];
						correctionHeaderWhere.push(` (NF_ID = ? AND NUM_ITEM = ?) `);
						whereValues.push(nfId, numItem);
					});
					where += correctionHeaderWhere.join(' OR ');
				}
				let query = select + ' ' + from + ' ' + placeHolders + ' ' + where;
				let executionObject = {
					query: query,
					values: whereValues,
					returnType: 'Array'
				};
				let temp = coreApi.OrmHighPerformance.__runSelect__(executionObject);
				temp.forEach(function(nf, i) {
					let nfId = temp[i].shift();
					let numItem = temp[i].shift();
					let correction = nfId + '_' + numItem;
					selectOptions.forEach(function(field, index) {
						shadowTableByCorrection[table][correction][field.field] = temp[i][index];
					});
				});
				response = shadowTableByCorrection || {};
			}
		});
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201074',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.mapToShadowObject = function(objectToUpsert, ctrNfStructure, atrApi) {
	var response = {};
	try {
		var map = {};
		// 		$.import('timp.atr.server.api', 'api');
		// 		const atrApi = $.timp.atr.server.api.api;
		ctrNfStructure = ctrNfStructure || atrApi.structure.table.READ({
			field: ['structure'],
			where: [{
				field: 'title',
				oper: '=',
				value: CORRECTION_STRUCTURE
            }]
		})[0];
		if (ctrNfStructure) {
			var json = JSON.parse(ctrNfStructure.structure);
			_.forEach(json.fields, function(f) {
				map[f.hanaName] = {
					shadowName: f.shadowName,
					type: f.type
				};
				map[f.shadowName] = {
					shadowName: f.shadowName,
					type: f.type
				};
			});
			const numericTypes = ['TINYINT', 'SMALLINT', 'INT', 'INTEGER', 'BIGINT', 'DECIMAL', 'REAL', 'DOUBLE', 'SMALLDECIMAL'];
			_.forEach(objectToUpsert, function(value, hanaName) {
				var shadowName = map[hanaName] ? map[hanaName].shadowName : hanaName;
				if (!shadowName && hanaName == 'MANDT') {
					shadowName = 'MANDT';
				}
				if (map[hanaName] && hanaName !== 'TP_CT_E') {
					//if it is a numeric value convert to number
					if (!_.isNil(value) && hanaName !== 'CANCELADO' && _.indexOf(numericTypes, map[hanaName].type) !== -1 && typeof value === 'string') {
						let dot = value.indexOf('.');
						let comma = value.indexOf(',');
						if (dot !== -1 && (comma === -1 || dot > comma)) {
							value = Number(value.replace(/,/g, ''));
						} else {
							value = Number(value.replace(/\./g, '').replace(/,/g, '.'));
						}
					}
				}
				
				response[shadowName] = response[shadowName] || value;
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201074',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/*
    receives an string (field hanaName)
    returns a string (shadow table name)
*/
this.getShadowTableByField = function(fieldName, ctrNfStructure) {
	var response = '';
	try {
		$.import('timp.atr.server.api', 'api');
		const atrApi = $.timp.atr.server.api.api;
		ctrNfStructure = ctrNfStructure || atrApi.structure.table.READ({
			field: ['structure'],
			where: [{
				field: 'title',
				oper: '=',
				value: CORRECTION_STRUCTURE
            }]
		})[0];
		if (ctrNfStructure) {
			var json = JSON.parse(ctrNfStructure.structure);
			var field = _.filter(json.fields, function(f) {
				return f.hanaName === fieldName;
			})[0];
			response = field.shadowTable;
		}
	} catch (e) {
		$.messageCodes.push({
			code: '',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.getShadowNameByHanaName = function(fieldName, ctrNfStructure) {
	var response = fieldName;
	try {
		$.import('timp.atr.server.api', 'api');
		const atrApi = $.timp.atr.server.api.api;
		ctrNfStructure = ctrNfStructure || atrApi.structure.table.READ({
			field: ['structure'],
			where: [{
				field: 'title',
				oper: '=',
				value: CORRECTION_STRUCTURE
            }]
		})[0];
		if (ctrNfStructure) {
			var json = JSON.parse(ctrNfStructure.structure);
			var field = _.filter(json.fields, function(f) {
				return fieldName === f.hanaName;
			})[0];
			if (field) {
				response = field.shadowName || fieldName;
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: '',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/**
 *
 * @method upsertShadowTable
 * @param {String} shadowTableName
 * @param {String} nfId
 * @param {String} numItem
 * @param {Object} values
 * @param {Object} values.id
 * @return {}
 * */
this.upsertShadowTable = function(shadowTableName, nfId, numItem, values, tdfSchema) {
	var response = {};
	try {
		if (_.isString(shadowTableName) && _.isString(nfId) && _.isString(numItem) && !_.isNil(values)) {
			tdfSchema = tdfSchema || coreApi.getSystemConfiguration({
				componentName: 'CORE',
				keys: ['tdfSchemaName', 'TDF::SCHEMA']
			})[0].value;
			if (!_.isNil(tdfSchema)) {
				var shadowTableRuntimeModel = $.createBaseRuntimeModel(tdfSchema, shadowTableName);
				let whereOptions = [{
					field: 'NF_ID',
					operator: '$eq',
					value: nfId
                }, {
					field: 'NUM_ITEM',
					operator: '$eq',
					value: numItem
                }, {
					field: 'MANDT',
					operator: '$eq',
					value: values.MANDT
                }];
				if (values.SYS_IMPOSTO) {
					whereOptions.push({
						field: 'SYS_IMPOSTO',
						operator: '$eq',
						value: values.SYS_IMPOSTO
					});
				}
				if (values.TIPO_IMPOSTO) {
					whereOptions.push({
						field: 'TIPO_IMPOSTO',
						operator: '$eq',
						value: values.TIPO_IMPOSTO
					});
				}
				Object.keys(values).forEach(function(k) {
					if (_.isNil(values[k])) {
						delete values[k];
					}
				});
				response = shadowTableRuntimeModel.upsert(values, {
					where: whereOptions
				}, true);
			} else {
				throw 'TDF Schema is not defined';
			}
		} else {
			throw 'Invalid Parameters';
		}

	} catch (e) {
		return $.parseError(e);
	}

	return response.results;
};

/*
* @method updateStatus
* @param {
    success:[{
        id:<number>,
        isShadow:<boolean>
    }], (array of objects)
    error:[] (array of ids)
}
*/
this.updateStatus = function(object) {
	var response = {
		approvalSuccess: false,
		approvalError: false,
		accountingSuccess: false,
		accountingError: false
	};
	try {
		var modificationUser = $.getUserID();
		var modificationDate = new Date();
		if (object.success.length > 0) {
			object.success = object.accounting.length ? object.accounting.reduce(function(a, b) {
				return Array.isArray(a) && a.concat(b) || object.success
			}) : object.success;

			_.forEach(object.success, function(s) {
				var oldObject = _this.read({
					id: s.id
				});
				response.approvalSuccess = correctionModel.UPDATE({
					id: s.id,
					status: s.pendingJob ? 1 : 2,
					approvalType: s.isShadow ? 2 : 1,
					pendingJob: s.pendingJob ? 1 : 0,
					wasCorrected: 1,
					modificationUser: modificationUser,
					modificationDate: modificationDate
				});
				var newObject = _this.read({
					id: s.id
				});
				Supervisor.updateCorrection(oldObject, newObject);
				if (s.isShadow) {
					Supervisor.approveCorrectionByShadow(newObject);
				} else {
					Supervisor.approveCorrection(newObject);
				}
			});
		}
		if (object.error.length > 0) {
			var tracker = {};
			object.error = object.accounting.reduce(function(a, b) {
				return Array.isArray(a) && a.concat(b) || object.error
			});

			_.forEach(object.error, function(id) {
				var oldObject = _this.read({
					id: id
				});
				tracker[id] = {
					oldObject: oldObject
				};
			});
			response.approvalError = correctionModel.UPDATEWHERE({
				status: 4,
				wasCorrected: 0,
				modificationUser: modificationUser,
				modificationDate: modificationDate
			}, [{
				field: 'id',
				oper: '=',
				value: object.error
            }]);
			_.forEach(object.error, function(id) {
				var newObject = _this.read({
					id: id
				});
				tracker[id].newObject = newObject;
			});
			_.forEach(tracker, function(tr) {
				Supervisor.updateCorrection(tr.oldObject, tr.newObject);
			});
		}

		if (object.accounting.length > 0) {
			object.accountingError = object.accounting.reduce(function(a, b) {
				return Array.isArray(a) && a.concat(b) || object.accounting
			});

			_.forEach(object.accounting, function(s) {
				var oldObject = _this.read({
					id: s.id || s
				});
				response.accountingSuccess = correctionModel.UPDATE({
					id: s.id || s,
					wasCorrected: 1,
					accounting: 'C',
					modificationUser: modificationUser,
					modificationDate: modificationDate
				});
				if (oldObject.referenceId) {
					let oldUpdate = correctionModel.UPDATE({
						id: oldObject.referenceId,
						wasCorrected: 1,
						accounting: 'C',
						modificationUser: modificationUser,
						modificationDate: modificationDate
					});
				}
				var newObject = _this.read({
					id: s.id || s
				});
				Supervisor.updateCorrection(oldObject, newObject);
				Supervisor.accountingCorrection(newObject);
			});
		}

		if (object.accountingError.length > 0) {
			object.accountingError = object.accountingError.reduce(function(a, b) {
				return Array.isArray(a) && a.concat(b) || object.accountingError
			});
			_.forEach(object.accountingError, function(s) {
				var newObject = _this.read({
					id: s.id || s
				});
				correctionModel.UPDATE({
					id: s.id || s,
					status: 4,
					modificationUser: modificationUser,
					modificationDate: modificationDate
				});
				response.accountingError = newObject;
				Supervisor.accountingCorrectionError(newObject);
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201064',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/*
receives an object like
{
    filters:{
        id:<integer> (optional),
        nfId:<Array of string> (optional),
        numItem:<Array of string> (optional),
        createdBy:<Array of integer> (optional),
        creationDate: [startDate, endDate] (optional),
        createdBy:<Array of integer> (optional),
        modification: [startDate, endDate] (optional)
    },
    page:1 <number> (required)
}
*/
this.list = function(object) {
	$.import('timp.bsc.server.models.views', 'cvCorrectionComplementaryInformation');
	const cvCorrectionComplementaryInformation = $.timp.bsc.server.models.views.cvCorrectionComplementaryInformation.cvCorrectionComplementaryInformation;
	$.import('timp.bsc.server.models.tables', 'externalModels');
	const accountingMappingForCorrectionsFieldsToValidation = $.timp.bsc.server.models.tables.externalModels.accountingMappingForCorrectionsFieldsToValidation;
	var taxesPrivileges = coreApi.view.userTaxes;
	const _self = this;
	var response = {
		data: [],
		pageCount: 1
	};
	try {
		//execute function
		var taxesPrivileges = coreApi.view.userTaxes;
        var taxesArray = taxesPrivileges.getTaxesByUser();
		let newTaxFilters = [];
		if (object) {
		    if (!object.filters) {
		        object.filters = {};
		    }
            object.filters.getNullTax = true;
		    if (object.filters.tax) {
                object.filters.getNullTax = false;
        		if (!_.isArray(object.filters.tax)) {
                    object.filters.tax = [object.filters.tax];
        		}
                if (_.isArray(taxesArray)) {
                    object.filters.tax.forEach(function(elem){
                        if (taxesArray.indexOf(elem) !== -1) {
                            newTaxFilters.push(elem);
            		    } 
                    });
    		    }
    	    } else {
    	        if(_.isArray(taxesArray)) {
    	            newTaxFilters = newTaxFilters.concat(taxesArray);
    	        }
    	    }
            object.filters.tax = newTaxFilters;
        }
		let detailed = Array.isArray(object.correctionType) ? object.correctionType.indexOf(4) != -1 : object.correctionType == 4;
		let joins = _this.getJoin( /*object.filters && object.filters.validationFields === 1*/ detailed, true, false, object.filters);
		let filterJoins = [];
		let cvCorrectionComplementaryInformationFields = ['docnum', 'branchId', 'direction', 'numDoc', 'numDocNum', 'uf'];
		if (object && object.orderBy) {
			let cvCorrectionComplementaryInformationOrderBy = [];
			_.forEach(object.orderBy, function(element) {
				let asc = element.indexOf('-') === -1 ? true : false;
				element = element.indexOf('-') !== -1 ? element.substring(1, element.length) : element;
				if (cvCorrectionComplementaryInformationFields.indexOf(element) !== -1) {
					cvCorrectionComplementaryInformationOrderBy.push({
						table: cvCorrectionComplementaryInformation,
						field: !asc ? '-' + element : element
					});
				}
			});
			object.orderBy = object.orderBy.concat(cvCorrectionComplementaryInformationOrderBy);
			filterJoins.push({
				fields: ['docnum', 'branchId', 'direction', 'numDoc', 'numDocNum', 'uf'],
				table: cvCorrectionComplementaryInformation,
				// rename: 'cvCorrectionComplementaryInformation',
				alias: 'cvCorrectionComplementaryInformation',
				on: [{
					left: 'nfId',
					right: 'docnum'
                }],
				outer: 'left'
			});
		}
		joins.push({
			fields: ['docnum', 'branchId', 'direction', 'numDoc', 'uf'],
			table: cvCorrectionComplementaryInformation,
			// rename: 'cvCorrectionComplementaryInformation',
			alias: 'cvCorrectionComplementaryInformation',
			on: [{
				left: 'nfId',
				right: 'docnum'
            }],
			outer: 'left'
		});
		if (object.filters && (object.filters.company || object.filters.uf || object.filters.branch || object.filters.releaseDate)) {
			filterJoins.push({
				fields: ['companyId', 'stateId', 'branch', 'releaseDate'],
				table: this.cvCorrectionOrm1(),
				alias: 'cvCorrectionOrm1',
				on: [{
					left: 'id',
					right: 'id'
                }],
				outer: 'left'
			});
		}
		if (object.filters && (object.filters.fields)) {
			filterJoins.push({
				fields: ['fieldName'],
				table: fieldByCorrectionModel,
				alias: 'fieldByCorrectionModel',
				on: [{
					left: 'id',
					right: 'correctionId'
                }],
				outer: 'left'
			});
		}
		if (object.filters && object.filters.subperiodId) {
			var idsBySubperiod = this.getCorrectionIdsBySubperiod({
				subperiodId: object.filters.subperiodId
			});
			if (idsBySubperiod && idsBySubperiod.length > 0) {
				object.filters.id = idsBySubperiod;
			}
			delete object.filters.subperiodId;
		}
		var where = _this.getWhere(object && object.filters ? object.filters : {});
		if (object.getStructureFields) {
			try {
				$.import('timp.atr.server.api', 'api');
				const atrApi = $.timp.atr.server.api.api;
				response.structure = atrApi.structure.table.READ({
					field: ['structure'],
					where: [{
						field: 'title',
						oper: '=',
						value: CORRECTION_STRUCTURE
                    }]
				})[0] || {};
			} catch (e) {
				response.structure = {};
			}
		} else {
			response.structure = {};
		}

		if (object.filters && object.filters.validationFields && object.filters.validationFields.length) {
			let fieldMapping = {
				CREDIT_VALUE: 'creditValue',
				CREDIT_COIN: 'creditCoin',
				CREDIT_ACCOUNT: 'creditAccount',
				CREDIT_COMPANY: 'creditCompany',
				// CREDIT_BRANCH: 'creditBranch',
				CREDIT_DOCUMENT_TYPE: 'creditDocumentType',
				CREDIT_RELEASE_DATE: 'creditReleaseDate',
				CREDIT_PROFIT_CENTER: 'creditProfitCenter',
				CREDIT_COST_CENTER: 'creditCostCenter',
				CREDIT_INTERNAL_ORDER: 'creditInternalOrder',
				CREDIT_PEP_ELEMENT: 'creditPepElement',
				CREDIT_NET_DIAGRAM: 'creditNetworkDiagram',
				CREDIT_OPERATION: 'creditOperation',
				CREDIT_BRANCH: 'creditBranch',
				CREDIT_TAX: 'creditAttribute',
				DEBIT_VALUE: 'debitValue',
				DEBIT_COIN: 'debitCoin',
				DEBIT_ACCOUNT: 'debitAccount',
				DEBIT_COMPANY: 'debitCompany',
				// DEBIT_BRANCH: 'debitBranch',
				DEBIT_DOCUMENT_TYPE: 'debitDocumentType',
				DEBIT_RELEASE_DATE: 'debitReleaseDate',
				DEBIT_PROFIT_CENTER: 'debitProfitCenter',
				DEBIT_COST_CENTER: 'debitCostCenter',
				DEBIT_INTERNAL_ORDER: 'debitInternalOrder',
				DEBIT_PEP_ELEMENT: 'debitPepElement',
				DEBIT_NET_DIAGRAM: 'debitNetworkDiagram',
				DEBIT_OPERATION: 'debitOperation',
				DEBIT_BRANCH: 'debitBranch',
				DEBIT_TAX: 'debitAttribute'
			};
			response.pageCount = 1;
			where.push({
				field: 'accounting',
				oper: '=',
				value: ['R', 'E']
			});
			let list = correctionModel.READ({
				where: where,
				orderBy: object.orderBy ? object.orderBy : ['id'],
				join: joins
			});
			list = list.filter(function(item) {
				return item.accountingMappingForCorrectionsFieldsToValidation && (item.accountingMappingForCorrectionsFieldsToValidation.length ||
					item.accountingMappingForCorrectionsFieldsToValidation.accountingMappingForCorrectionsId);
			}).filter(function(item) {
				let accountingMapping = item.accountingMappingForCorrections[0] || item.accountingMappingForCorrections || {};
				if (!_.has(accountingMapping, 'id')) {
					return false;
				}
				let validationFields = accountingMappingForCorrectionsFieldsToValidation.READ({
					where: [{
						field: 'accountingMappingForCorrectionsId',
						oper: '=',
						value: accountingMapping.id
                    }]
				});
				if (validationFields && validationFields.length) {
					if (item.accountingFields && item.accountingFields.length) {
						let resp = _self.setAccountingFieldsValues(accountingMapping, item.accountingFields, item);
						accountingMapping = resp.accountingMapping;
					}
					let editData = item.editData[0] || item.editData || {};
					if (editData && editData.correctionId) {
						accountingMapping = _self.setEditCorrectionData(accountingMapping, editData);
					}
					for (let i = 0; i < validationFields.length; i++) {
						let actualField = validationFields[i];
						actualField = actualField.fieldName || actualField;
						actualField = fieldMapping[actualField];
						if (object.filters.validationFields.indexOf(actualField) !== -1 && accountingMapping[actualField] === '' || _.isNil(
							accountingMapping[actualField])) {
							return true;
						}
					}
				}
				return false;
			});
			let correctionIdList = _.map(list, function(e) {
				return e.id;
			});
			if (correctionIdList.length > 0) {
				let commentMap = this.getCommentMap(correctionIdList);
				let linkMap = this.getLinkMap(correctionIdList);
				list = _.map(list, function(e) {
					e.comments = commentMap[e.id] || [];
					e.links = linkMap[e.id] || [];
					return e;
				});
			}
			response.data = this.setRule(list);
			return response;
		}
		let whereOfTrabalhista = {
			field: 'isTrabalhistaCorrection',
			oper: object.filters && object.filters.isTrabalhistaCorrection ? '=' : 'IS NULL'
		};
		if (object.filters && object.filters.isTrabalhistaCorrection) {
			whereOfTrabalhista.value = 1;
		}
		where.push(whereOfTrabalhista);
		var idsInPage = correctionModel.READ({
			where: where,
			simulate: false,
			fields: ['id'],
			orderBy: object.orderBy ? object.orderBy : ['id'],
			join: filterJoins,
			paginate: {
				size: 15,
				number: object && object.page ? (Number(object.page) || 1) : 1,
				count: true
			}
		});
		response.pageCount = idsInPage.pageCount;
		var whereIdsInPage = [];
		if (idsInPage && idsInPage.length > 0) {
			whereIdsInPage.push({
				field: 'id',
				oper: '=',
				value: _.map(idsInPage, function(e) {
					return e.id;
				})
			});
		} else {
			whereIdsInPage.push({
				field: 'id',
				oper: '=',
				value: 0
			});
		}
		var list = correctionModel.READ({
			where: whereIdsInPage,
			orderBy: object.orderBy ? object.orderBy : ['id'],
			join: joins
		});
		var correctionIdList = _.map(list, function(e) {
			return e.id;
		});
		if (correctionIdList.length > 0) {
			var commentMap = this.getCommentMap(correctionIdList);
			var linkMap = this.getLinkMap(correctionIdList);
			list = _.map(list, function(e) {
				e.comments = commentMap[e.id] || [];
				e.links = linkMap[e.id] || [];
				return e;
			});
		}
		response.data = this.setRule(list);
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201068',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.setRule = function(list) {
	if (!list || !(list instanceof Array)) {
		list = [];
	}
	let ruleTable = $.createBaseRuntimeModel($.schema.slice(1, -1), 'BRE::Rule');
	let ruleList = list.map(function(item) {
		return item.rulePath && typeof(item.rulePath) === 'string' ? Number(item.rulePath.split('-')[0]) : false;
	}).filter(function(item) {
		return item && !isNaN(item) ? true : false;
	});
	let rules = ruleTable.find({
		select: [{
			field: 'ID',
			as: 'id'
        }, {
			field: 'NAME',
			as: 'name'
        }],
		where: [{
			field: 'ID',
			operator: '$in',
			value: ruleList
        }]
	});
	let ruleMap = rules.results.reduce(function(res, item) {
		res[item.id] = item.id + ' - ' + item.name;
		return res;
	}, {});
	list.forEach(function(item) {
		item.ruleName = '';
		if (item.rulePath && typeof(item.rulePath) === 'string') {
			let rulePath = item.rulePath;
			if (rulePath.split('-')[0] === 'Manual') {
				rulePath = rulePath.split('-').slice(1).join('-');
			}
			if (!isNaN(Number(rulePath.split('-')[0]))) {
				item.ruleName = ruleMap[Number(rulePath.split('-')[0])];
			}
		}
		if (item.scenarioConfiguration) {
			let scenario = item.scenarioConfiguration;
			if (item.scenarioConfiguration instanceof Array) {
				// scenario = item.scenarioConfiguration[0] || {};
				_.forEach(item.scenarioConfiguration, function(scenarioConfig) {
					let regex = new RegExp(`\\d+-\\d+-${scenarioConfig.id}`, 'g');
					if (item.rulePath && item.rulePath.match && item.rulePath.match(regex)) {
						scenario = scenarioConfig || {};
					}
				});
			}
			item.scenarioName = (scenario.code || '') + (scenario.description ? (' - ' + scenario.description) : '');
		}
		if (item.scenarioHierarchy) {
			let scenario = item.scenarioHierarchy;
			if (item.scenarioHierarchy instanceof Array) {
				scenario = item.scenarioHierarchy[0] || {};
			}
			item.hierarchyName = (scenario.code || '') + (scenario.description ? (' - ' + scenario.description) : '');
		}
	});
	return list;
};

this.getCommentMap = function(correctionIdList) {
	var response = {};
	if (correctionIdList && correctionIdList.length > 0) {
		var commentList = commentByCorrectionModel.READ({
			where: [{
				field: 'correctionId',
				oper: '=',
				value: correctionIdList
            }],
			join: [{
				table: coreApi.users,
				alias: 'modificationUserData',
				on: [{
					left: 'modificationUser',
					right: 'id'
                }]
            }]
		});
		response = _.reduce(commentList, function(obj, e) {
			obj[e.correctionId] = obj[e.correctionId] || [];
			var modificationUser = e.modificationUserData[0].name + ' ' + e.modificationUserData[0].last_name;
			obj[e.correctionId].push({
				comment: e.comment,
				modificationUser: modificationUser,
				modificationDate: e.modificationDate
			});
			return obj;
		}, {});
	}
	return response;
};

this.getLinkMap = function(correctionIdList) {
	var response = {};
	if (correctionIdList && correctionIdList.length > 0) {
		var commentList = linkByCorrectionModel.READ({
			where: [{
				field: 'correctionId',
				oper: '=',
				value: correctionIdList
            }],
			join: [{
				table: coreApi.users,
				alias: 'modificationUserData',
				on: [{
					left: 'modificationUser',
					right: 'id'
                }]
            }]
		});
		response = _.reduce(commentList, function(obj, e) {
			obj[e.correctionId] = obj[e.correctionId] || [];
			var modificationUser = e.modificationUserData[0].name + ' ' + e.modificationUserData[0].last_name;
			obj[e.correctionId].push({
				link: e.link,
				modificationUser: modificationUser,
				modificationDate: e.modificationDate
			});
			return obj;
		}, {});
	}
	return response;
};

this.getMessageMap = function(correctionIdList) {
	var response = {};
	if (correctionIdList && correctionIdList.length > 0) {
		var messageList = messageByCorrectionModel.READ({
			where: [{
				field: 'correctionId',
				oper: '=',
				value: correctionIdList
            }],
			fields: ['id', 'correctionId', 'creationDate', 'message']
		});
		response = _.reduce(messageList, function(obj, e) {
			obj[e.correctionId] = obj[e.correctionId] || [];
			obj[e.correctionId].push(e);
			return obj;
		}, {});
	}
	return response;
};

this.getCorrectionIdsBySubperiod = function(object) {
	var response = [];
	try {
		$.import('timp.tfp.server.api', 'api');
		const tfpApi = $.timp.tfp.server.api.api;
		var subperiodInstance = tfpApi.fiscalSubPeriod.getSubperiodById({
			id: object.subperiodId
		});
		var startDate = $.moment(subperiodInstance.startDateTypeDate).format('YYYYMMDD');
		var endDate = $.moment(subperiodInstance.endDateTypeDate).format('YYYYMMDD');
		//add subperiod filters
		var mainWhere = [{
			alias: 'cvCorrection',
			field: 'companyId',
			operator: '$eq',
			value: subperiodInstance.idCompany
        }, {
			alias: 'cvCorrection',
			field: 'stateId',
			operator: '$eq',
			value: subperiodInstance.uf
        }, {
			alias: 'cvCorrection',
			field: 'branchId',
			operator: '$eq',
			value: subperiodInstance.idBranch
        }, {
			alias: 'cvCorrection',
			field: 'releaseDate',
			operator: '$between',
			value: [startDate, endDate]
        }];
		var eccMandt = coreApi.getSystemConfiguration({
			componentName: 'CORE',
			keys: ['ECC::CLIENT']
		})[0].value;
		var result = cvCorrection.find({
			aliases: [{
				collection: cvCorrection.getIdentity(),
				name: 'cvCorrection',
				isPrimary: true
            }],
			where: mainWhere,
			select: [{
				field: 'id',
				alias: 'cvCorrection'
            }],
			placeholders: [{
				alias: 'cvCorrection',
				params: [{
					name: 'ipMandante',
					values: [eccMandt]
                }]
            }]
		});
		if (result.results.length > 0) {
			response = _.map(result.results, function(e) {
				return e.id;
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201068',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.createComments = function(comments, correctionId) {
	var response = [];
	if (comments && comments.length > 0 && correctionId) {
		_.forEach(comments, function(comment) {
			var objToCreate = {
				correctionId: correctionId,
				comment: comment,
				modificationUser: $.getUserID(),
				modificationDate: new Date()
			};
			var created = commentByCorrectionModel.CREATE(objToCreate);
			response.push(created);
		});
	}
	return response;
};

this.createLinks = function(links, correctionId) {
	var response = [];
	if (links && links.length > 0 && correctionId) {
		_.forEach(links, function(link) {
			var objToCreate = {
				correctionId: correctionId,
				link: link,
				modificationUser: $.getUserID(),
				modificationDate: new Date()
			};
			var created = linkByCorrectionModel.CREATE(objToCreate);
			response.push(created);
		});
	}
	return response;
};

/*
    receives any filter
    returns a list of corrections
*/
this.get = function(object) {
	var response = [];
	try {
		var where = this.getWhere(object);
		response = correctionModel.READ({
			where: where,
			join: this.getJoin()
		});
	} catch (e) {
		$.messageCodes.push({
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.listAdvancedFilters = function() {
	var response = {
		createdBy: [],
		approvalType: [],
		modifiedBy: [],
		fields: []
	};
	try {
		// let lang = $.request.cookies.get('Content-Language');

		var distinctApprovalType = correctionModel.READ({
			fields: ['approvalType'],
			distinct: true,
			where: [
                [{
					field: 'isDeleted',
					oper: '=',
					value: 0
                }, {
					field: 'isDeleted',
					oper: 'IS NULL'
                }], {
					field: 'approvalType',
					oper: '=',
					value: 0,
					not: true
                }
            ]
		});
		if (distinctApprovalType && distinctApprovalType.length > 0) {
			response.approvalType = _.map(distinctApprovalType, function(e) {
				return {
					key: e.approvalType,
					name: e.approvalType
				};
			});
		}

		let distinctFields = fieldByCorrectionModel.READ({
			fields: ['fieldName'],
			distinct: true
		});
		if (distinctFields && distinctFields.length > 0) {
			response.fields = _.map(distinctFields, function(e) {
				return {
					key: e.fieldName,
					name: e.fieldName
				};
			});
		}

		var distinctCreatedAndModificateBy = correctionModel.READ({
			fields: ['creationUser', 'modificationUser'],
			distinct: true,
			where: [
                [{
					field: 'isDeleted',
					oper: '=',
					value: 0
                }, {
					field: 'isDeleted',
					oper: 'IS NULL'
                }]
            ]
		});
		if (distinctCreatedAndModificateBy && distinctCreatedAndModificateBy.length > 0) {
			var createdBy = coreApi.users.READ({
				fields: ['id', 'name', 'last_name'],
				where: [{
					field: 'id',
					oper: '=',
					value: distinctCreatedAndModificateBy.map(function(e) {
						return e.creationUser;
					})
                }]
			});
			response.createdBy = _.map(createdBy, function(c) {
				return {
					key: c.id,
					name: c.name + ' ' + c.last_name
				};
			});

			var modifiedBy = coreApi.users.READ({
				fields: ['id', 'name', 'last_name'],
				where: [{
					field: 'id',
					oper: '=',
					value: distinctCreatedAndModificateBy.map(function(e) {
						return e.modificationUser;
					})
                }]
			});
			response.modifiedBy = _.map(modifiedBy, function(c) {
				return {
					key: c.id,
					name: c.name + ' ' + c.last_name
				};
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201067',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/*
receives an object like
{
    ids: <Array of integer> (required),
    justification: <string> (required)
}
*/
this.reject = function(object) {
	var response = {
		rejected: false
	};
	try {
		//execution function
		if (_.isPlainObject(object)) {
			if (_.has(object, 'id') && _.isArray(object.id) && object.id.length > 0) {
				var modificationUser = $.getUserID();
				var modificationDate = new Date();
				response.rejected = correctionModel.UPDATEWHERE({
					justification: object.justification,
					status: 3,
					wasCorrected: 0,
					modificationUser: modificationUser,
					modificationDate: modificationDate
				}, [{
					field: 'id',
					oper: '=',
					value: object.id
                }]);
				//log action
				_.forEach(object.id, function(id) {
					var correction = _this.read({
						id: id
					});
					Supervisor.rejectCorrection(correction);
				});
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201066',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		//log error
		if (_.isPlainObject(object) && _.has(object, 'id')) {
			_.forEach(object.ids, function(id) {
				Supervisor.rejectCorrectionError(id, util.parseError(e));
			});
		}
	}
	return response;
};

/*
returns all the invoices with a counter by status
*/
this.getCorrectionCount = function() {
	var response = [];
	try {
		var list = correctionModel.READ({
			fields: ['nfId', 'numItem', 'status'],
			groupBy: ['nfId', 'numItem', 'status'],
			count: true
		});
		if (list && list.length > 0) {
			let statusMap = {
				1: 'OPEN',
				2: 'CORRECTED',
				3: 'REJECTED',
				4: 'ERROR',
				5: 'IN PROGRESS' //used to identify that is being approved by job
			};
			list = _.map(list, function(row) {
				return {
					nfId: row[0],
					numItem: row[1],
					status: row[2],
					count: Number(row[3])
				};
			});
			var groupedResponse = _.reduce(list, function(obj, element) {
				var key = element.nfId + '_' + element.numItem;
				obj[key] = obj[key] || {
					nfId: element.nfId,
					numItem: element.numItem
				};
				var status = statusMap[element.status];
				if (!status) {
					statusMap = element.status;
				}
				obj[key][status] = element.count;
				return obj;
			}, {});
			_.forEach(groupedResponse, function(element) {
				response.push(element);
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201068',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/*
receives an object like
{
    toUpdate: [{id:1, message:""}]
}
*/
this.createMessages = function(object) {
	var response = {
		created: []
	};
	try {
		if (_.isPlainObject(object) && _.has(object, 'toUpdate') && _.isArray(object.toUpdate)) {
			_.forEach(object.toUpdate, function(correction) {
				if (Array.isArray(correction.id)) {
					_.forEach(correction.id, function(id) {
						var created = messageByCorrectionModel.CREATE({
							correctionId: id,
							message: correction.message,
							creationDate: new Date()
						});
						response.created.push(created);
					});
				} else {
					var created = messageByCorrectionModel.CREATE({
						correctionId: correction.id,
						message: correction.message,
						creationDate: new Date()
					});
					response.created.push(created);
				}
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201064',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

// this.getCorrectionsByNfId = function(object) {
//     var response = {
//         data: [],
//         pageCount: 1
//     };
//     try {
//         var joinCorrection = {
//             // table: correctionModel,
//             // fields: [],
//             alias: 'correction',
//             on: [{
//                 left: 'correctionId',
//                 right: 'id'
//             }]
//         };
//         var joinModificationUser = {
//             table: coreApi.users,
//             rename: 'U3',
//             alias: 'modificationUserData',
//             on: [{
//                 leftTable: correctionModel,
//                 left: 'modificationUser',
//                 right: 'id'
//             }],
//             outer: 'left'
//         };
//         let joinHeaderFieldModel = {
//             // fields: [],
//             alias: 'headerFieldModel',
//             on: [{
//                 left: 'id',
//                 right: 'correctionId'
//             }],
//             outer: 'left'
//         };
//         if (object && _.isPlainObject(object)) {
//             if (_.has(object, 'nfId')) {
//                 joinCorrection.on.push({
//                     field: 'nfId',
//                     oper: '=',
//                     value: object.nfId
//                 });
//             }
//         }
//         let joinCorrection2 = JSON.parse(JSON.stringify(joinCorrection));
//         joinCorrection.table = correctionModel;
//         joinCorrection2.table = correctionModel;
//         var idsInPage = fieldByCorrectionModel.READ({
//             fields: ['id'],
//             orderBy: ['id'],
//             join: [joinCorrection],
//             paginate: {
//                 size: 15,
//                 number: object && object.page ? (Number(object.page) || 1) : 1,
//                 count: true
//             }
//         });
//         response.pageCount = idsInPage.pageCount;
//         var whereIdsInPage = [];
//         if (idsInPage && idsInPage.length > 0) {
//             whereIdsInPage.push({
//                 field: 'id',
//                 oper: '=',
//                 value: _.map(idsInPage, function(e) {
//                     return e.id;
//                 })
//             });
//         } else {
//             whereIdsInPage.push({
//                 field: 'id',
//                 oper: '=',
//                 value: 0
//             });
//         }
//         var list = fieldByCorrectionModel.READ({
//             join: [joinCorrection2, joinModificationUser],
//             where: whereIdsInPage
//         });
//         var statusMap = {
//             1: "PENDING",
//             2: "CORRECTED",
//             3: "REJECTED",
//             4: "ERROR",
//             5: "IN PROGRESS" //used to identify that is being approved by job
//         };
//         if (list && _.isArray(list) && list.length > 0) {
//             _.forEach(list, function(element) {
//                 var modificationUser = "";
//                 if (element.modificationUserData && element.modificationUserData[0]) {
//                     modificationUser = element.modificationUserData[0].name + ' ' + element.modificationUserData[0].last_name;
//                 }
//                 response.data.push({
//                     correctionId: element.correction[0].id,
//                     nfId: element.correction[0].nfId,
//                     numItem: element.correction[0].numItem,
//                     status: statusMap[element.correction[0].status],
//                     fieldName: element.fieldName,
//                     oldValue: element.oldValue,
//                     newValue: element.newValue,
//                     modificationDate: element.correction[0].modificationDate,
//                     modificationUser: modificationUser
//                 });
//             });
//             response.data = response.data.sort(function(a, b) {
//                 var modificationDateA = new Date(a.modificationDate);
//                 var modificationDateB = new Date(b.modificationDate);
//                 if (modificationDateA.getTime() > modificationDateB.getTime()) {
//                     return 1;
//                 }
//                 if (modificationDateA.getTime() < modificationDateB.getTime()) {
//                     return -1;
//                 }
//                 return 0;
//             });
//         }
//     } catch (e) {
//         $.messageCodes.push({
//             code: "BSC201068",
//             type: "E",
//             errorInfo: util.parseError(e)
//         });
//     }
//     return response;
// };

this.getCorrectionsByNfId = function(object) {
	let response = {
		data: [],
		pageCount: 1
	};
	try {
		let list = correctionModel.READ({
			fields: ['id', 'nfId', 'numItem', 'status', 'modificationDate'],
			mode: 'flat',
			where: [{
				field: 'nfId',
				oper: '=',
				value: object.nfId
            }, {
				field: 'status',
				oper: '=',
				value: 2
            }],
			paginate: {
				size: 15,
				number: object && object.page ? (Number(object.page) || 1) : 1,
				count: true
			},
			orderBy: ['id'],
			join: this.getHistoryJoins()
		});
		response.pageCount = list.pageCount;
		let statusMap = {
			1: 'PENDING',
			2: 'CORRECTED',
			3: 'REJECTED',
			4: 'ERROR',
			5: 'IN PROGRESS' //used to identify that is being approved by job
		};
		if (!_.isEmpty(list) && _.isArray(list) && list.length > 0) {
			_.forEach(list, function(element) {
				let modificationUser = '';
				let newValue = '';
				let fieldName = '';
				if (!_.isEmpty(element['modificationUserData.name'])) {
					modificationUser = element['modificationUserData.name'] + ' ' + element['modificationUserData.last_name'];
				}
				if (!_.isNil(element['fieldByCorrectionModel.newValue'])) {
					newValue = element['fieldByCorrectionModel.newValue'];
				} else if (!_.isNil(element['headerFieldModel.value'])) {
					newValue = element['headerFieldModel.value'];
				} else if (!_.isNil(element['itemFieldModel.value'])) {
					newValue = element['itemFieldModel.value'];
				}
				if (!_.isNil(element['fieldByCorrectionModel.fieldName'])) {
					fieldName = element['fieldByCorrectionModel.fieldName'];
				} else if (!_.isNil(element['headerFieldModel.fieldName'])) {
					fieldName = element['headerFieldModel.fieldName'];
				} else if (!_.isNil(element['itemFieldModel.fieldName'])) {
					fieldName = element['itemFieldModel.fieldName'];
				}
				response.data.push({
					correctionId: element.id,
					nfId: element.nfId,
					numItem: element.numItem,
					status: statusMap[element.status] || element.status,
					fieldName: fieldName,
					oldValue: element['fieldByCorrectionModel.oldValue'] || '',
					newValue: newValue,
					modificationDate: element.modificationDate,
					modificationUser: modificationUser
				});
			});
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201068',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.getHistoryJoins = function() {
	let joins = [{
		table: fieldByCorrectionModel,
		alias: 'fieldByCorrectionModel',
		fields: ['correctionId', 'fieldName', 'oldValue', 'newValue'],
		on: [{
			left: 'id',
			right: 'correctionId'
        }]
		// outer: 'left'
    }, {
		table: coreApi.users,
		rename: 'U3',
		alias: 'modificationUserData',
		fields: ['id', 'name', 'last_name'],
		on: [{
			left: 'modificationUser',
			right: 'id'
        }],
		outer: 'left'
    }];
	// joins.push({
	//     table: headerFieldModel,
	//     alias: 'headerFieldModel',
	//     fields: ['correctionId', 'fieldName', 'value'],
	//     on: [{
	//         left: 'id',
	//         right: 'correctionId'
	//     }],
	//     outer: 'left'
	// }, {
	//     table: itemGroupModel,
	//     alias: 'itemGroupModel',
	//     fields: ['id', 'correctionId'],
	//     on: [{
	//         left: 'id',
	//         right: 'correctionId'
	//     }],
	//     outer: 'left'
	// }, {
	//     table: itemFieldModel,
	//     alias: 'itemFieldModel',
	//     fields: ['itemGroupId', 'fieldName', 'value'],
	//     on: [{
	//         leftTable: itemGroupModel,
	//         left: 'id',
	//         right: 'itemGroupId'
	//     }],
	//     outer: 'left'
	// });
	return joins;
};

/*
function used by calendar to get corrections progress
@param subperiod {
    id:,
    idCompany:,
    uf:,
    idBranch,
    idTax:,
    subPeriod:,
    year:,
    month:
}
*/
this.getCorrectionProgressBySubperiod = function(subperiod) {
	var response = {
		label: 'ANALIZED CORRECTIONS',
		progress: 0,
		total: 0
	};
	try {
		$.import('timp.tfp.server.api', 'api');
		const tfpApi = $.timp.tfp.server.api.api;
		var subperiodInstance = tfpApi.fiscalSubPeriod.getSubperiodById({
			id: subperiod.id
		});
		var startDate = $.moment(subperiodInstance.startDateTypeDate).format('YYYYMMDD');
		var endDate = $.moment(subperiodInstance.endDateTypeDate).format('YYYYMMDD');
		//add subperiod filters
		var mainWhere = [{
			alias: 'cvCorrection',
			field: 'companyId',
			operator: '$eq',
			value: subperiodInstance.idCompany
        }, {
			alias: 'cvCorrection',
			field: 'stateId',
			operator: '$eq',
			value: subperiodInstance.uf
        }, {
			alias: 'cvCorrection',
			field: 'branchId',
			operator: '$eq',
			value: subperiodInstance.idBranch
        }, {
			alias: 'cvCorrection',
			field: 'releaseDate',
			operator: '$between',
			value: [startDate, endDate]
        }];
		var correctedWhere = JSON.parse(JSON.stringify(mainWhere));
		correctedWhere.push({
			alias: 'cvCorrection',
			field: 'status',
			operator: '$in',
			value: [2, 3]
		});
		var eccMandt = coreApi.getSystemConfiguration({
			componentName: 'CORE',
			keys: ['ECC::CLIENT']
		})[0].value;
		//get corrected count
		var correctedResult = cvCorrection.find({
			aliases: [{
				collection: cvCorrection.getIdentity(),
				name: 'cvCorrection',
				isPrimary: true
            }],
			where: correctedWhere,
			select: [{
				aggregation: {
					type: 'COUNT',
					param: {
						field: 'status',
						alias: 'cvCorrection'
					}
				},
				as: 'corrected',
				alias: 'cvCorrection'
            }],
			placeholders: [{
				alias: 'cvCorrection',
				params: [{
					name: 'ipMandante',
					values: [eccMandt]
                }]
            }]
		});
		if (correctedResult.results && correctedResult.results.length > 0) {
			response.progress = correctedResult.results[0].corrected;
		}
		//get total count by subperiod
		var totalResult = cvCorrection.find({
			aliases: [{
				collection: cvCorrection.getIdentity(),
				name: 'cvCorrection',
				isPrimary: true
            }],
			where: mainWhere,
			select: [{
				aggregation: {
					type: 'COUNT',
					param: {
						field: 'status',
						alias: 'cvCorrection'
					}
				},
				as: 'totalCount',
				alias: 'cvCorrection'
            }],
			placeholders: [{
				alias: 'cvCorrection',
				params: [{
					name: 'ipMandante',
					values: [eccMandt]
                }]
            }]
		});
		if (totalResult.results && totalResult.results.length > 0) {
			response.total = totalResult.results[0].totalCount;
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201068',
			type: 'E',
			errorInfo: coreApi.util.parseError(e)
		});
	}
	return response;
};

/*
{
        id:<integer> (optional),
        nfId:<Array of string> (optional),
        numItem:<Array of string> (optional),
        createdBy:<Array of integer> (optional),
        creationDate: [startDate, endDate] (optional),
        modifiedBy:<Array of integer> (optional),
        modification: [startDate, endDate] (optional)
    }
*/
this.getListWhere = function(object) {
	var where = [];
	try {
		if (object && _.isPlainObject(object)) {
			if (_.has(object, 'id')) {
				where.push({
					field: 'id',
					alias: 'cvCorrection',
					operator: '$eq',
					value: object.id
				});
			}
			if (_.has(object, 'nfId')) {
				where.push({
					field: 'nfId',
					alias: 'cvCorrection',
					operator: _.isArray(object.nfId) ? '$in' : '$eq',
					value: object.nfId
				});
			}
			if (_.has(object, 'numItem')) {
				where.push({
					field: 'numItem',
					alias: 'cvCorrection',
					operator: _.isArray(object.numItem) ? '$in' : '$eq',
					value: object.numItem
				});
			}
			if (_.has(object, 'status')) {
				var statusMap = {
					'PENDING': 1,
					'CORRECTED': 2,
					'REJECTED': 3,
					'ERROR': 4,
					'IN PROGRESS': 5
				};
				var status = object.status;
				if (_.isArray(object.status)) {
					status = _.map(function(s) {
						if (statusMap[s]) {
							return statusMap[s];
						}
						return s;
					});
				} else {
					if (statusMap[status]) {
						status = statusMap.status;
					}
				}
				where.push({
					field: 'status',
					alias: 'cvCorrection',
					operator: _.isArray(status) ? '$in' : '$eq',
					value: status
				});
			}
			if (_.has(object, 'approvalType')) {
				where.push({
					field: 'approvalType',
					alias: 'cvCorrection',
					operator: _.isArray(object.approvalType) ? '$in' : '$eq',
					value: object.approvalType
				});
			}
			if (_.has(object, 'createdBy')) {
				where.push({
					field: 'creationUserId',
					alias: 'cvCorrection',
					operator: _.isArray(object.createdBy) ? '$in' : '$eq',
					value: object.createdBy
				});
			}
			if (_.has(object, 'creationDate')) {
				where.push({
					field: 'creationDate',
					alias: 'cvCorrection',
					operator: '$gte',
					value: object.creationDate[0]
				});
				where.push({
					field: 'creationDate',
					alias: 'cvCorrection',
					operator: '$lte',
					value: object.creationDate[1]
				});
			}
			if (_.has(object, 'modifiedBy')) {
				where.push({
					field: 'modificationUserId',
					alias: 'cvCorrection',
					operator: _.isArray(object.modifiedBy) ? '$in' : '$eq',
					value: object.modifiedBy
				});
			}
			if (_.has(object, 'modificationDate')) {
				where.push({
					field: 'modificationDate',
					alias: 'cvCorrection',
					operator: '$gte',
					value: object.modificationDate[0]
				});
				where.push({
					field: 'modificationDate',
					alias: 'cvCorrection',
					operator: '$lte',
					value: object.modificationDate[1]
				});
			}
			if (_.has(object, 'companyId')) {
				where.push({
					field: 'companyId',
					alias: 'cvCorrection',
					operator: '$eq',
					value: object.companyId
				});
			}
			if (_.has(object, 'stateId')) {
				where.push({
					field: 'stateId',
					alias: 'cvCorrection',
					operator: '$eq',
					value: object.stateId
				});
			}
			if (_.has(object, 'branchId')) {
				where.push({
					field: 'branchId',
					alias: 'cvCorrection',
					operator: '$eq',
					value: object.branchId
				});
			}
			if (_.has(object, 'startDate')) {
				where.push({
					field: 'releaseDate',
					alias: 'cvCorrection',
					operator: '$gte',
					value: object.startDate
				});
			}
			if (_.has(object, 'endDate')) {
				where.push({
					field: 'releaseDate',
					alias: 'cvCorrection',
					operator: '$lte',
					value: object.endDate
				});
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201063',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return where;
};

/*
receives an object like
{
    filters:{
        id:<integer> (optional),
        nfId:<Array of string> (optional),
        numItem:<Array of string> (optional),
        createdBy:<Array of integer> (optional),
        creationDate: [startDate, endDate] (optional),
        createdBy:<Array of integer> (optional),
        modification: [startDate, endDate] (optional)
    },
    page:1 <number> (required)
}
*/
this.completeDataList = function(object) {
	const pageSize = 15;
	var page = 1;
	var response = {
		data: [],
		pageCount: 1
	};
	try {
		if (object && object.page) {
			page = object.page;
		}
		var eccMandt = coreApi.getSystemConfiguration({
			componentName: 'CORE',
			keys: ['ECC::CLIENT']
		})[0].value;
		var where = this.getListWhere(object.filters);
		var fieldByCorrectionRuntimeModel = $.createBaseRuntimeModel($.schema.slice(1, -1), 'BSC::FIELD_BY_CORRECTION');
		var countResult = cvCorrection.find({
			aliases: [{
				collection: cvCorrection.getIdentity(),
				name: 'cvCorrection',
				isPrimary: true
            }, {
				collection: fieldByCorrectionRuntimeModel.getIdentity(),
				name: 'fields'
            }, {
				collection: cvCorrection.getIdentity(),
				name: 'subquery',
				query: {
					aliases: [{
						collection: cvCorrection.getIdentity(),
						name: 'cvCorrectionPaginated',
						isPrimary: true
                    }, {
						collection: fieldByCorrectionRuntimeModel.getIdentity(),
						name: 'newFields'
                    }],
					distinct: true,
					select: [{
						alias: 'cvCorrectionPaginated',
						field: 'id',
						as: 'id'
                    }],
					where: where,
					join: [{
						alias: 'newFields',
						type: 'left',
						on: [{
							alias: 'newFields',
							field: 'CORRECTION_ID',
							operator: '$eq',
							value: {
								alias: 'cvCorrectionPaginated',
								field: 'id'
							}
                        }]
                    }],
					placeholders: [{
						alias: 'cvCorrectionPaginated',
						params: [{
							name: 'ipMandante',
							values: [eccMandt]
                        }]
                    }]
				}
            }],
			distinct: true,
			select: [{
				aggregation: {
					type: 'COUNT',
					param: {
						alias: 'cvCorrection',
						field: 'id'
					}
				},
				as: 'count',
				alias: 'cvCorrection'
            }],
			join: [{
				alias: 'subquery',
				type: 'inner',
				on: [{
					alias: 'subquery',
					field: 'id',
					operator: '$eq',
					value: {
						alias: 'cvCorrection',
						field: 'id'
					}
                }]
            }],
			where: where,
			placeholders: [{
				alias: 'cvCorrection',
				params: [{
					name: 'ipMandante',
					values: [eccMandt]
                }]
            }]
		});
		if (countResult.results.length > 0) {
			response.pageCount = Math.ceil(countResult.results[0].count / pageSize);
		}
		var result = cvCorrection.find({
			aliases: [{
				collection: cvCorrection.getIdentity(),
				name: 'cvCorrection',
				isPrimary: true
            }, {
				collection: fieldByCorrectionRuntimeModel.getIdentity(),
				name: 'fields'
            }, {
				collection: cvCorrection.getIdentity(),
				name: 'subquery',
				query: {
					aliases: [{
						collection: cvCorrection.getIdentity(),
						name: 'cvCorrectionPaginated',
						isPrimary: true
                    }, {
						collection: fieldByCorrectionRuntimeModel.getIdentity(),
						name: 'newFields'
                    }],
					distinct: true,
					select: [{
						alias: 'cvCorrectionPaginated',
						field: 'id',
						as: 'id'
                    }],
					where: where,
					join: [{
						alias: 'newFields',
						type: 'left',
						on: [{
							alias: 'newFields',
							field: 'CORRECTION_ID',
							operator: '$eq',
							value: {
								alias: 'cvCorrectionPaginated',
								field: 'id'
							}
                        }]
                    }],
					placeholders: [{
						alias: 'cvCorrectionPaginated',
						params: [{
							name: 'ipMandante',
							values: [eccMandt]
                        }]
                    }],
					paginate: {
						limit: pageSize,
						offset: (page - 1) * pageSize
					},
					orderBy: [{
						alias: 'cvCorrectionPaginated',
						field: 'id'
                    }]
				}
            }],
			join: [{
				alias: 'subquery',
				type: 'inner',
				on: [{
					alias: 'subquery',
					field: 'id',
					operator: '$eq',
					value: {
						alias: 'cvCorrection',
						field: 'id'
					}
                }]
            }, {
				alias: 'fields',
				type: 'left',
				map: 'fields',
				on: [{
					alias: 'fields',
					field: 'CORRECTION_ID',
					operator: '$eq',
					value: {
						alias: 'cvCorrection',
						field: 'id'
					}
                }]
            }],
			select: [{
				alias: 'cvCorrection',
				field: 'id'
            }, {
				alias: 'cvCorrection',
				field: 'scenarioHierarchyId'
            }, {
				alias: 'cvCorrection',
				field: 'nfId'
            }, {
				alias: 'cvCorrection',
				field: 'numItem'
            }, {
				'case': [{
					conditions: [{
						alias: 'cvCorrection',
						field: 'status',
						operator: '$eq',
						value: 1
                    }],
					value: 'PENDING'
                }, {
					conditions: [{
						alias: 'cvCorrection',
						field: 'status',
						operator: '$eq',
						value: 2
                    }],
					value: 'CORRECTED'
                }, {
					conditions: [{
						alias: 'cvCorrection',
						field: 'status',
						operator: '$eq',
						value: 3
                    }],
					value: 'REJECTED'
                }, {
					conditions: [{
						alias: 'cvCorrection',
						field: 'status',
						operator: '$eq',
						value: 4
                    }],
					value: 'ERROR'
                }, {
					conditions: [{
						alias: 'cvCorrection',
						field: 'status',
						operator: '$eq',
						value: 5
                    }],
					value: 'IN PROGRESS'
                }],
				alias: 'cvCorrection',
				as: 'status'
            }, {
				alias: 'cvCorrection',
				field: 'reason'
            }, {
				alias: 'cvCorrection',
				field: 'justification'
            }, {
				'case': [{
					conditions: [{
						alias: 'cvCorrection',
						field: 'approvalType',
						operator: '$eq',
						value: 0
                    }],
					value: ''
                }, {
					conditions: [{
						alias: 'cvCorrection',
						field: 'approvalType',
						operator: '$eq',
						value: 1
                    }],
					value: 'ECC'
                }, {
					conditions: [{
						alias: 'cvCorrection',
						field: 'approvalType',
						operator: '$eq',
						value: 2
                    }],
					value: 'SHADOW'
                }],
				alias: 'cvCorrection',
				as: 'approvalType'
            }, {
				alias: 'cvCorrection',
				field: 'pendingJob'
            }, {
				alias: 'cvCorrection',
				field: 'adapterId'
            }, {
				alias: 'cvCorrection',
				field: 'creationDate'
            }, {
				alias: 'cvCorrection',
				field: 'creationUser'
            }, {
				alias: 'cvCorrection',
				field: 'modificationDate'
            }, {
				alias: 'cvCorrection',
				field: 'modificationUser'
            }, {
				alias: 'cvCorrection',
				field: 'companyId'
            }, {
				alias: 'cvCorrection',
				field: 'stateId'
            }, {
				alias: 'cvCorrection',
				field: 'branchId'
            }, {
				alias: 'cvCorrection',
				field: 'releaseDate'
            }, {
				alias: 'fields',
				field: 'ID',
				as: 'id'
            }, {
				alias: 'fields',
				field: 'CORRECTION_ID',
				as: 'correctionId'
            }, {
				alias: 'fields',
				field: 'FIELD_NAME',
				as: 'fieldName'
            }, {
				alias: 'fields',
				field: 'NEW_VALUE',
				as: 'newValue'
            }, {
				alias: 'fields',
				field: 'OLD_VALUE',
				as: 'oldValue'
            }],
			placeholders: [{
				alias: 'cvCorrection',
				params: [{
					name: 'ipMandante',
					values: [eccMandt]
                }]
            }],
			orderBy: [{
				alias: 'cvCorrection',
				field: 'id'
            }]
		});
		var correctionIdList = _.map(result.results, function(e) {
			return e.id;
		});
		if (correctionIdList.length > 0) {
			var commentMap = this.getCommentMap(correctionIdList);
			var linkMap = this.getLinkMap(correctionIdList);
			var messageMap = this.getMessageMap(correctionIdList);
			result.results = _.map(result.results, function(e) {
				e.comments = commentMap[e.id] || [];
				e.links = linkMap[e.id] || [];
				e.messages = messageMap[e.id] || [];
				return e;
			});
		}
		response.data = result.results;
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201068',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/*
@param correction {object} //correction instance
@param options {object}
@example options = {
    dateType: <integer>//1 || 2,
    date: <Date>,
    year:<string>,
    month:<string>
}
*/
this.callReverse = function(correction, isHttpsActive) {
	var response = {
		xmlSent: '',
		xmlResponse: ''
	};
	try {
		var host = '';
		var port = '';
		var adapterId = null;
		var serviceUrl = '';
		var mandtTDF = '';
		var urlResponse = this.getServiceURL('TMFFIReversalPostSyncIn');
		host = urlResponse.host;
		port = urlResponse.port;
		serviceUrl = urlResponse.serviceUrl;
		adapterId = urlResponse.adapterId;
		mandtTDF = urlResponse.mandtTDF;
		var location = host + ':' + port + serviceUrl;
		var fieldsMap = this.getServiceFieldsMap(correction.headerFields, correction.nfId, correction.numItem);
		if (fieldsMap.MANDT_TDF) {
			mandtTDF = fieldsMap.MANDT_TDF;
		}
		// var today = new Date().toSAPDate();
		if (fieldsMap.COMPANY_CODE && fieldsMap.BRANCH) {
			mandtTDF = this.getMandtTdfFromTDF(fieldsMap.COMPANY_CODE, fieldsMap.BRANCH) || mandtTDF;
		}
		var objSystem = this.getObjSys(mandtTDF);
		// var postingDate = options.dateType === 1 ? today : (options.dateType === 2 ? this.getLastDay(options.date) : options.date);
		var serviceParameters = {
			'tmf:FIPostingReversal': {
				BusinessPlace: {
					MandtTDF: mandtTDF,
					CompanyCode: fieldsMap.COMPANY_CODE,
					Branch: fieldsMap.BRANCH
				},
				FIPostingReversalData: {
					OBJ_TYPE: 'BKPFF',
					OBJ_KEY: fieldsMap.AC_DOC_NO + '' + fieldsMap.COMPANY_CODE + '' + fieldsMap.YEAR,
					OBJ_SYS: objSystem,
					OBJ_KEY_R: fieldsMap.AC_DOC_NO + '' + fieldsMap.COMPANY_CODE + '' + fieldsMap.YEAR,
					PSTNG_DATE: fieldsMap.PSTNG_DATE, //postingDate || today,
					FIS_PERIOD: fieldsMap.YEAR + '/' + fieldsMap.MONTH,
					COMP_CODE: fieldsMap.COMPANY_CODE,
					REASON_REV: fieldsMap.REASON_REV || '01',
					AC_DOC_NO: fieldsMap.AC_DOC_NO,
					ACC_PRINCIPLE: ''
				},
				Check: {
					Check: ''
				}
			}
		};
		var xml = util.converterJSON({
			json: serviceParameters
		});
		if (_.isString(xml)) {
			xml = xml.replace(/&#x2F;/g, '/');
		}
		var externalCallResponse = coreApi.executeTDFExternalService('POST', adapterId, location, xml, null, isHttpsActive,
			'TMFFIReversalPostSyncIn');
		if (externalCallResponse && externalCallResponse.xml) {
			var json = util.converterXML({
				xml: externalCallResponse.xml
			});
			response.xmlSent = xml;
			response.xmlResponse = externalCallResponse.xml;
			response.successful = externalCallResponse.isSuccessful;
			Supervisor.callReversalService(correction.id, response.xmlSent, response.xmlResponse);
			if (json.Envelope && json.Envelope.Body && json.Envelope.Body.FIPostingReturn && json.Envelope.Body.FIPostingReturn.FIPostingReturn &&
				json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable) {
				var objToUpdate = {
					id: correction.id
				};
				var revertDocument = function(reversedDocument) {
					objToUpdate.status = 2;
					objToUpdate.reversedDocument = reversedDocument;
					objToUpdate.wasCorrected = 0;
					correctionModel.UPDATE(objToUpdate);
				};
				if (_.isArray(json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable)) {
					for (var j = 0; j < json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable.length; j++) {
						if (json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable[j].TYPE === 'S') {
							revertDocument(json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable[j].MESSAGE_V2);
							j = json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable.length;
						}
					}
				} else if (json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable.TYPE === 'S') {
					revertDocument(json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable.MESSAGE_V2);
				}
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201065',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.callCorrectionLetterCreation = function(correction, adapterId, isHttpsActive) {
	var response = {
		xmlSent: '',
		xmlResponse: ''
	};
	try {
		var host = '';
		var port = '';
		var adapterId = null;
		var serviceUrl = '';
		var mandtTDF = '';
		var urlResponse = this.getServiceURL('TMFNFCorrectionLetterCreationSyncIn');
		host = urlResponse.host;
		port = urlResponse.port;
		serviceUrl = urlResponse.serviceUrl;
		adapterId = urlResponse.adapterId;
		mandtTDF = urlResponse.mandtTDF;
		var location = host + ':' + port + serviceUrl;
		var fieldsMap = this.getServiceFieldsMap(correction.headerFields, correction.nfId, correction.numItem);
		if (fieldsMap && fieldsMap.hasOwnProperty('ID_NF_CREATION_PARAMETERS') && fieldsMap.ID_NF_CREATION_PARAMETERS) {
			let _fieldsMap = this.getFieldsMapNfcp(fieldsMap.ID_NF_CREATION_PARAMETERS, correction.nfId);
			fieldsMap.NFID = _fieldsMap.NFID;
			fieldsMap.CORRECTION_TEXT = _fieldsMap.CORRECTION_TEXT;
		}
		if (fieldsMap.MANDT) {
			mandtTDF = fieldsMap.MANDT;
		}
		var version = '';
		var componentInformation = util.getComponentInformation('CORE');
		if (componentInformation && componentInformation.results && componentInformation.results[0] && componentInformation.results[0].VERSION) {
			version = componentInformation.results[0].VERSION;
		}
		var ppid = coreApi.getPPID(adapterId);
		coreApi.setMessageSyncIn(adapterId, ppid, 'BSC', version, 'TMFNFCorrectionLetterCreationSyncIn');
		var serviceParameters = {
			'tmf:NFCorrectionLetter': {
				mandt: mandtTDF,
				nfid: fieldsMap.NFID,
				correction_text: fieldsMap.CORRECTION_TEXT,
				MARKLOG: {
					'ppid': ppid,
					'timestamp': _this.getCurrentTimestamp(),
					'progmodule': 'TIMP',
					'progversion': version,
					'username': $.session.getUsername(),
					'status': 'I',
					'text': correction.justification || '---'
				}
			}
		};
		var xml = util.converterJSON({
			json: serviceParameters
		});
		if (_.isString(xml)) {
			xml = xml.replace(/&#x2F;/g, '/');
		}
		response.xmlSent = xml;
		var externalCallResponse = coreApi.executeTDFExternalService('POST', adapterId, location, xml, null, isHttpsActive,
			'TMFNFCorrectionLetterCreationSyncIn');
		if (externalCallResponse && externalCallResponse.xml) {
			var json = util.converterXML({
				xml: externalCallResponse.xml
			});
			response.xmlSent = xml;
			response.xmlResponse = externalCallResponse.xml;
			response.successful = externalCallResponse.isSuccessful;
			Supervisor.callCorrectionLetterCreation(correction.id, response.xmlSent, response.xmlResponse);
			if (json.Envelope && json.Envelope.Body && json.Envelope.Body.NFCorrectionLetterReturn && json.Envelope.Body.NFCorrectionLetterReturn.NFEvent &&
				!(json.Envelope.Body.NFCorrectionLetterReturn.ReturnTable && json.Envelope.Body.NFCorrectionLetterReturn.ReturnTable.TYPE === 'E')) {
				var objToUpdate = {
					id: correction.id
				};
				objToUpdate.status = 2;
				objToUpdate.correctionLetterDocument = json.Envelope.Body.NFCorrectionLetterReturn.NFEvent.DOCNUM;
				correctionModel.UPDATE(objToUpdate);
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201065',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

//For Accounting
this.callCorrectionAccountingEntry = function(correction) {
	var response = {
		xmlSent: '',
		xmlResponse: ''
	};
	const mapping = {
		COD_EMP: 'BUKRS',
		ID_DOC_CONT: 'BELNR',
		ANO_EXERCICIO: 'GJAHR',
		MES_EXERCICIO: 'MONAT',
		TP_DOC: 'BLART',
		DT_DOC: 'BLDAT',
		DT_LANCTO: 'BUDAT',
		NUM_DOC_REFER: 'XBLNR',
		NUM_DOC_ESTORNO: 'STBLG',
		ANO_EXERCICIO_ESTORNO: 'STJAH',
		TEXTO_CAB_DOC: 'BKTXT',
		ST_DOCUMENTO: 'BSTAT',
		TP_REF: 'AWTYP',
		CHV_REF: 'AWKEY',
		DT_CRIACAO: 'CPUDT',
		HR_CRIACAO: 'CPUTM',
		DT_ALTERACAO: 'AEDAT',
		USU_CRIACAO: 'USNAM',
		ID_ITEM: 'BUZEI',
		COD_CTA: 'HKONT',
		DT_COMPENSACAO: 'AUGDT',
		NUM_DOC_COMPENSACAO: 'AUGBL',
		DIVISAO: 'GSBER',
		COD_IVA: 'MWSKZ',
		CHV_LANCTO: 'BSCHL',
		VL_CONTABIL: 'DMBTR',
		DESC_ITEM: 'SGTXT',
		COD_CENTRO: 'WERKS',
		CENTRO_CUSTO: 'KOSTL',
		CENTRO_LUCRO: 'PRCTR',
		COD_FILIAL: 'BUPLA',
		ZUONR: 'ZUONR'
	};
	try {
		var host = '';
		var port = '';
		var adapterId = null;
		var serviceUrl = '';
		var urlResponse = this.getServiceURL('timp_modifica_dados_contabeis');
		host = urlResponse.host;
		port = urlResponse.port;
		serviceUrl = urlResponse.serviceUrl;
		adapterId = urlResponse.adapterId;
		var location = host + ':' + port + serviceUrl;
		var version = '';
		var componentInformation = util.getComponentInformation('CORE');
		if (componentInformation && componentInformation.results && componentInformation.results[0] && componentInformation.results[0].VERSION) {
			version = componentInformation.results[0].VERSION;
		}
		var ppid = coreApi.getPPID(adapterId);
		coreApi.setMessageSyncIn(adapterId, ppid, 'BSC', version, 'timp_modifica_dados_contabeis');
		var serviceParameters = {
			'urn:ZFM_TIMP_FI_DOCUMENT_CHANGE': {
				'I_BELNR': correction.nfId,
				'I_BUKRS': correction.company,
				'I_BUZEI': correction.numItem,
				'I_GJAHR': correction.year,
				'T_ACCCHG': {
					'item': []
				}
			}
		};
		if (correction.fields && correction.fields.length) {
			for (var i = 0; i < correction.fields.length; i++) {
				serviceParameters['urn:ZFM_TIMP_FI_DOCUMENT_CHANGE'].T_ACCCHG.item.push({
					FDNAME: mapping[correction.fields[i].fieldName],
					OLDVAL: correction.fields[i].oldValue,
					NEWVAL: correction.fields[i].newValue
				});
			}
		}
		serviceParameters = {
			'soapenv:Envelope': {
				'soapenv:Body': serviceParameters
			}
		};
		var xml = util.converterJSON({
			json: serviceParameters
		});
		if (_.isString(xml)) {
			xml = xml.replace(/&#x2F;/g, '/');
			xml = '<soapenv:Envelope  xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:sap-com:document:sap:rfc:functions"' +
				xml.split('<soapenv:Envelope')[1];
		}
		response.xmlSent = xml;
		var externalCallResponse = coreApi.executeTDFExternalService('POST', adapterId, location, xml);
		if (externalCallResponse && externalCallResponse.xml) {
			var json = util.converterXML({
				xml: externalCallResponse.xml
			});
			response.xmlSent = xml;
			response.xmlResponse = externalCallResponse.xml;
			response.successful = externalCallResponse.isSuccessful;
			Supervisor.callCorrectionLetterCreation(correction.id, response.xmlSent, response.xmlResponse);
			if (json.Envelope && json.Envelope.Body && json.Envelope.Body.ZFM_TIMP_FI_DOCUMENT_CHANGEResponse && json.Envelope.Body.ZFM_TIMP_FI_DOCUMENT_CHANGEResponse
				.ES_BAPIRET2 &&
				(json.Envelope.Body.ZFM_TIMP_FI_DOCUMENT_CHANGEResponse.ES_BAPIRET2.TYPE === 'S')) {
				var objToUpdate = {
					id: correction.id
				};
				objToUpdate.status = 2;
				correctionModel.UPDATE(objToUpdate);
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201065',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

/*
@param serviceName {string}
*/
this.getServiceURL = function(serviceName) {
	var response = {
		serviceUrl: '',
		host: '',
		port: '',
		adapterId: undefined,
		mandtTDF: '',
		tdfUserName: ''
	};
	try {
		var serviceResponse = coreApi.getServiceInformation(serviceName);
		if (serviceResponse && serviceResponse[0]) {
			var serviceDestination = _.filter(serviceResponse[0].destinations, function(dest) {
				return dest.isDefault;
			})[0];
			if (!serviceDestination) {
				serviceDestination = serviceResponse[0].destinations[0];
			}
			if (serviceDestination.adapterId) {
				var adapterId = serviceDestination.adapterId;
				response.adapterId = adapterId;
				var adapterResponse = coreApi.getProxyAdapter({
					filter: {
						id: adapterId
					}
				});
				if (adapterResponse) {
					response.adapterId = adapterResponse.adapters[0].id;
					response.host = adapterResponse.adapters[0].hostName;
					response.port = adapterResponse.adapters[0].port;
					response.mandtTDF = adapterResponse.adapters[0].client;
					response.tdfUserName = adapterResponse.adapters[0].userName;
				}
			}
			response.serviceUrl = serviceDestination.destination;
			response.serviceUrl = response.serviceUrl.split('bndg_url')[1] || response.serviceUrl;
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'TFP001120',
			type: 'E',
			errorInfo: util.parseError(e)
		});
		response.error = util.parseError(e);
	}
	return response;
};

/*
@param fields {array}
@example [{fieldName:"MANDT_TDF",value:"300"},{fieldName:"COMPANY_CODE",value:"1000"}]
@returns {MANDT_TDF:"300",COMPANY_CODE:"1000"}
*/
this.getServiceFieldsMap = function(fields, nfId, idItem) {
	var response = {};
	let refFields = [];
	let year = '',
		month = '';
	response = _.reduce(fields, function(obj, element) {
		let key = element.fieldName;
		obj[key] = element.value;
		if (element.useReference) {
			refFields.push(key);
		}
		if (key === 'PSTNG_DATE') {
			let date = $.moment(obj[key], 'DDMMYYYY');
			if (!date.isValid()) {
				date = $.moment(obj[key], 'YYYYMMDD');
			}
			if (!date.isValid()) {
				date = $.moment(obj[key], 'MMDDYYYY');
			}
			if (date.isValid()) {
				obj[key] = date.format('YYYYMMDD');
				year = date.format('YYYY');
				month = date.format('MM');
			}
		}
		return obj;
	}, {});
	if (refFields.length) {
		let values = this.getReferenceFields(nfId, idItem, refFields);
		$.lodash.forEach(values, (value, key) => {
			response[key] = !$.lodash.isNil(value) && !$.lodash.isEmpty(value) ? value : response[key];
		});
	}
	if (_.isNil(response.YEAR)) {
		response.YEAR = year;
	}
	if (_.isNil(response.MONTH)) {
		response.MONTH = month;
	}
	return response;
};

/**
 * @param   { string } idNfCreationParameters - MDR's ID NF creation parameters
 * @param   { number } docnum - NF ID obtained from the correction
 * @returns { object } - Fields map replace for this case
 */
this.getFieldsMapNfcp = function(idNfCreationParameters, docnum) {
	$.import('timp.mdr.server.api', 'api');
	const mdrApi = $.timp.mdr.server.api.api;
	const nfCreationParametersController = mdrApi.nfCreationParameters;

	try {
		let nfCreationParameters = nfCreationParametersController.read({
			id: idNfCreationParameters
		});
		const correctionText = nfCreationParameters.text || '';
		const nfId = docnum;

		return {
			CORRECTION_TEXT: correctionText,
			NFID: nfId
		};
	} catch (err) {
		return {};
	}
};

this.getObjSys = function(mandt) {
	$.import('timp.tpc.server.api', 'api');
	var objSys = $.timp.tpc.server.api.api.objSys;
	var item = objSys.READ({
		where: [{
			field: 'mandt',
			oper: '=',
			value: mandt
        }]
	})[0];
	return item ? item.idSys : '';
};

/*
@param date {Date}
*/
this.getLastDay = function(date) {
	var newDate = new Date(date);
	newDate.setMonth(newDate.getMonth() + 1);
	newDate.setDate(0);
	return $.moment(newDate).format('YYYYMMDD');
};

/*
@param correction {object} //correction instance
*/
this.callAccounting = function(corrections, listAccountWithouCostCenter, destination, externalCallResponse, isHttpsActive, isGrouped) {
	let correction = isGrouped ? corrections[0] : corrections;
	var response = {
		xmlSent: '',
		xmlResponse: '',
		isAccounting: true
	};
	try {
		var host = '';
		var port = '';
		var adapterId = null;
		var serviceUrl = '';
		var mandtTDF = '';
		var tdfUserName = '';
		var urlResponse = this.getServiceURL('TMFFIPostingPostSyncIn');
		correction.headerFields = setEditDataToHeader(correction);
		host = urlResponse.host;
		port = urlResponse.port;
		serviceUrl = urlResponse.serviceUrl;
		adapterId = urlResponse.adapterId;
		mandtTDF = urlResponse.mandtTDF;
		tdfUserName = urlResponse.tdfUserName;
		var location = host + ':' + port + serviceUrl;
		var fieldsMap = this.getServiceFieldsMap(correction.headerFields, correction.nfId, correction.numItem);
		if (fieldsMap.COMPANY_CODE && fieldsMap.BRANCH) {
			mandtTDF = this.getMandtTdfFromTDF(fieldsMap.COMPANY_CODE, fieldsMap.BRANCH) || mandtTDF;
		}
		
		if (!(fieldsMap.BRANCH)) {
			$.import('timp.mdr.server.api', 'api');
			var mdrApi = $.timp.mdr.server.api.api;
			var companies = mdrApi.companiesController;
			//search filial principal
			let companyResult = [];
			let mainBranchFound = false;
			companyResult = companies.listCompanies({
				"codEmpresa": fieldsMap.COMPANY_CODE,
				"filiais": true
			});
			if (companyResult && companyResult.length) {
				if (companyResult[0].filiais.length > 0) {
					for (var z = 0; z < companyResult[0].filiais.length; z++) {
						if (companyResult[0].filiais[z].filialPrincipal === 1) {
							fieldsMap.BRANCH = companyResult[0].filiais[z].codFilial;
							break;
						}
					}
				}
			}
		}
		var objSystem = this.getObjSys(mandtTDF);
		var accountingItems = this.getAccountingItems(isGrouped ? corrections : correction, fieldsMap, listAccountWithouCostCenter, isGrouped);
		let currentDate = $.moment();
		if (fieldsMap && fieldsMap.MONTH < currentDate.format('MM') && fieldsMap.YEAR == currentDate.format('YYYY')) {
			fieldsMap.MONTH = currentDate.format('MM');
			let firstDayOfMonth = currentDate.format('YYYYMM') + '01';
			fieldsMap.PSTNG_DATE = firstDayOfMonth;
		}
		
		var serviceParameters = {
			'tmf:FIPostingPost': {
				BusinessPlace: {
					MandtTDF: mandtTDF,
					CompanyCode: fieldsMap.COMPANY_CODE || '',
					Branch: fieldsMap.BRANCH || ''
				},
				FIPostingHeaderData: {
					OBJ_KEY: fieldsMap.OBJ_KEY || '',
					OBJ_TYPE: 'BKPFF',
					OBJ_SYS: objSystem,
					USERNAME: tdfUserName,
					HEADER_TXT: '', //fieldsMap.HEADER_TXT,
					OBJ_KEY_R: '',
					COMP_CODE: fieldsMap.COMPANY_CODE || '',
					AC_DOC_NO: this.getSequenceField(correction, fieldsMap),
					FISC_YEAR: fieldsMap.YEAR || '',
					DOC_DATE: fieldsMap.DOC_DATE || fieldsMap.PSTNG_DATE || '',
					PSTNG_DATE: fieldsMap.PSTNG_DATE || '',
					TRANS_DATE: fieldsMap.PSTNG_DATE || '',
					FIS_PERIOD: (fieldsMap.YEAR && fieldsMap.MONTH) ? (fieldsMap.YEAR + '/' + fieldsMap.MONTH) : '',
					DOC_TYPE: $.lodash.isNil(fieldsMap.DOC_TYPE) ? '' : fieldsMap.DOC_TYPE,
					REF_DOC_NO: fieldsMap.REF_DOC_NO || '',
					COMPO_ACC: '',
					REASON_REV: '',
					REF_DOC_NO_LONG: '',
					ACC_PRINCIPLE: '',
					DOC_STATUS: ''
				},
				FIPostingItemData: {
					ItemsTable: accountingItems.itemsTable.length ? accountingItems.itemsTable : undefined
				},
				FIPostingVendorData: {
					VendorsItemsTable: accountingItems.vendorsTable.length ? accountingItems.vendorsTable : undefined
				},
				FIPostingCustomerData: {
					// CustomersItemsTable: customersItemsTable
				},
				FIPostingCurrency: {
					CurrencyTable: accountingItems.currencyTable
				},
				Check: {
					Check: ''
				},
				FIPostingExtension2: {
					Extension2Table: accountingItems.extension2Table
				}
			}
		};
		if (!(_.isEmpty(correction.cvCorrectionComplementaryInformation))) {
			serviceParameters = this.checkMDRAccountingParameters(serviceParameters, correction, listAccountWithouCostCenter);
		}
		if (accountingItems.itemsTable.length === 0) {
			delete serviceParameters['tmf:FIPostingPost'].FIPostingItemData.ItemsTable;
		}
		if (accountingItems.vendorsTable.length === 0) {
			delete serviceParameters['tmf:FIPostingPost'].FIPostingVendorData.VendorsItemsTable;
		}
		if (serviceParameters && serviceParameters['tmf:FIPostingPost'] && serviceParameters['tmf:FIPostingPost'].FIPostingItemData &&
			serviceParameters['tmf:FIPostingPost'].FIPostingItemData.ItemsTable && serviceParameters['tmf:FIPostingPost'].FIPostingItemData.ItemsTable
			.length) {
			let index = 0;
			let itemId = 1;
			serviceParameters['tmf:FIPostingPost'].FIPostingItemData.ItemsTable.sort(function() {
				let response = -1;
				if (serviceParameters['tmf:FIPostingPost'].FIPostingCurrency.CurrencyTable[index] && serviceParameters['tmf:FIPostingPost'].FIPostingCurrency
					.CurrencyTable[index + 1]) {
					let val_1 = serviceParameters['tmf:FIPostingPost'].FIPostingCurrency.CurrencyTable[index].AMT_DOCCUR;
					let val_2 = serviceParameters['tmf:FIPostingPost'].FIPostingCurrency.CurrencyTable[++index].AMT_DOCCUR;
					response = val_1 < val_2 ? 1 : -1;
				}
				return response;
			}).map(function(item) {
				item.ITEMNO_ACC = '000'.substring(0, 3 - itemId.toString().length) + itemId;
				itemId++;
				return item;
			});
			itemId = 1;
			serviceParameters['tmf:FIPostingPost'].FIPostingCurrency.CurrencyTable.sort(function(a, b) {
				return a.AMT_DOCCUR < b.AMT_DOCCUR ? 1 : -1;
			}).map(function(item) {
				item.ITEMNO_ACC = '000'.substring(0, 3 - itemId.toString().length) + itemId;
				itemId++;
				return item;
			});
		}
		var xml = util.converterJSON({
			json: serviceParameters
		});
		if (_.isString(xml)) {
			xml = xml.replace(/&#x2F;/g, '/');
		}

		if (destination && destination.destinationPath && externalCallResponse) {
			var externalCallResponse = externalCallResponse;
		} else if (destination && destination.destinationPath) {
			return coreApi.executeTDFExternalService('POST', adapterId, location, xml, destination.destinationPath, isHttpsActive,
				'TMFFIPostingPostSyncIn');
		} else {
			var externalCallResponse = coreApi.executeTDFExternalService('POST', adapterId, location, xml, null, isHttpsActive,
				'TMFFIPostingPostSyncIn');
		}
		response.xmlSent = xml;
		if (externalCallResponse && externalCallResponse.xml) {
			var json = util.converterXML({
				xml: externalCallResponse.xml
			});
			let returnTable = this.getReturnTable(json);
			let iteratorItem = returnTable && returnTable.item || returnTable;
			var messages = [];
			response.xmlResponse = externalCallResponse.xml;
			response.successful = externalCallResponse.isSuccessful;
			if (iteratorItem.length) {
				messages = this.getErrorMessagesFromTDF(iteratorItem);
				messages = messages.filter(e => e.indexOf('Período contábil') !== -1 && e.indexOf('não aberto') !== -1);
				if (messages.length) {
					let newXml = this.dateToggler(serviceParameters);
					externalCallResponse = coreApi.executeTDFExternalService('POST', adapterId, location, newXml, null, isHttpsActive,
						'TMFFIPostingPostSyncIn');
					if (externalCallResponse && externalCallResponse.xml) {
						json = util.converterXML({
							xml: externalCallResponse.xml
						});
						response.xmlSent = newXml;
						response.xmlResponse = externalCallResponse.xml;
						response.successful = externalCallResponse.isSuccessful;
					}
				}
			}
			Supervisor.callAccountingService(correction.id, response.xmlSent, response.xmlResponse);
			if (json.Envelope && json.Envelope.Body && json.Envelope.Body.FIPostingReturn && json.Envelope.Body.FIPostingReturn.FIPostingReturn &&
				json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable) {
				var update = false,
					text, message;
				var objToUpdate = {
					id: correction.id
				};
				var setEccDocument = function(item, idx) {
					if (item.FIPostingValues && item.FIPostingValues.OBJ_KEY) {
						text = item.FIPostingValues.OBJ_KEY;
						objToUpdate.accountingDocument = text.substr(0, 10);
						objToUpdate.accountingDoc = text.substr(0, 10);
					} else {
						if (idx) {
							message = item.FIPostingReturn.ReturnTable[idx].MESSAGE;
						} else {
							message = item.FIPostingReturn.ReturnTable.MESSAGE;
						}
						if (message) {
							objToUpdate.accountingDocument = message.substr(message.indexOf('BKPFF ') + 6, 10);
							objToUpdate.accountingDoc = text.substr(0, 10);
						} else {
							if (idx) {
								message = item.FIPostingReturn.ReturnTable[idx].MESSAGE_V2;
							} else {
								message = item.FIPostingReturn.ReturnTable.MESSAGE_V2;
							}
							objToUpdate.accountingDocument = message.substr(0, 10);
							objToUpdate.accountingDoc = text.substr(0, 10);
						}
					}
					update = true;
				};
				if ($.lodash.isArray(json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable)) {
					for (var i = 0; i < json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable.length; i++) {
						if (json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable[i].TYPE === 'S') {
							setEccDocument(json.Envelope.Body.FIPostingReturn, i);
							if (objToUpdate.accountingDocument || objToUpdate.accountingDoc) {
								i = json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable.length;
							}
						}
						// else if (json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable[i].TYPE === 'I') {
						//     info.push(json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable[i].MESSAGE);
						// } else {
						//     messages.push(json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable[i].MESSAGE);
						// }
						// if (messages.length) {
						//     if(response.error.filter(function(ele){return ele.id==correction.id;}).length==0){
						//         response.error.push({id:correction.id,messages:messages});
						//     }
						// }
						// if (info.length) {
						//     response.info.push({id:correction.id,messages:info});
						// }
					}
				} else if (json.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable.TYPE === 'S') {
					setEccDocument(json.Envelope.Body.FIPostingReturn);
				}
				if (update) {
					objToUpdate.status = 2;
					if (correction.correctionType === 'timp_modifica_dados_contabeis' || correction.isDadosContabeis) {
						objToUpdate.correctionType = 4;
					} else {
						objToUpdate.correctionType = 1;
					}
					objToUpdate.wasCorrected = 1;
					correctionModel.UPDATE(objToUpdate);
				} else {
					objToUpdate.status = 4;
					correctionModel.UPDATE(objToUpdate);
				}
			}
		}
	} catch (e) {
		$.messageCodes.push({
			code: 'BSC201065',
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.dateToggler = function(params) {
	var today = $.moment();
	var month = today.format('MM');
	var year = today.format('YYYY');
	var day = today.format('DD');
	var period = `${year}/${month}`;
	var fulldate = today.format('YYYYMMDD');

	params['tmf:FIPostingPost'].FIPostingHeaderData.FISC_YEAR = year;
	params['tmf:FIPostingPost'].FIPostingHeaderData.DOC_DATE = fulldate;
	params['tmf:FIPostingPost'].FIPostingHeaderData.PSTNG_DATE = fulldate;
	params['tmf:FIPostingPost'].FIPostingHeaderData.TRANS_DATE = fulldate;
	params['tmf:FIPostingPost'].FIPostingHeaderData.FIS_PERIOD = period;
	params['tmf:FIPostingPost'].FIPostingItemData.ItemsTable.forEach(element => {
		element.PSTNG_DATE = fulldate;
		element.FISC_YEAR = year;
		element.FIS_PERIOD = period;
	});

	let newXML = util.converterJSON({
		json: params
	});

	if (_.isString(newXML)) {
		newXML = newXML.replace(/&#x2F;/g, '/');
	}

	return newXML;
}

this.getReturnTable = function(response) {
	return (response.Envelope && response.Envelope.Body && response.Envelope.Body.FIPostingReturn && response.Envelope.Body.FIPostingReturn.FIPostingReturn &&
		response.Envelope.Body.FIPostingReturn.FIPostingReturn.ReturnTable) || (response.Envelope && response.Envelope.Body && response.Envelope
		.Body.ZFM_TIMP_CRIAR_DOCU_CONTABILResponse &&
		response.Envelope.Body.ZFM_TIMP_CRIAR_DOCU_CONTABILResponse.EV_FIPOSTING_RETURN &&
		response.Envelope.Body.ZFM_TIMP_CRIAR_DOCU_CONTABILResponse.EV_FIPOSTING_RETURN.FIPOSTING_RETURN &&
		response.Envelope.Body.ZFM_TIMP_CRIAR_DOCU_CONTABILResponse.EV_FIPOSTING_RETURN.FIPOSTING_RETURN.RETURN_TABLE) || (response.Envelope &&
		response.Envelope.Body && response.Envelope.Body.ZFM_TIMP_CRIAR_DOCU_CONTABIL2Response &&
		response.Envelope.Body.ZFM_TIMP_CRIAR_DOCU_CONTABIL2Response.EV_FIPOSTING_RETURN &&
		response.Envelope.Body.ZFM_TIMP_CRIAR_DOCU_CONTABIL2Response.EV_FIPOSTING_RETURN.FIPOSTING_RETURN &&
		response.Envelope.Body.ZFM_TIMP_CRIAR_DOCU_CONTABIL2Response.EV_FIPOSTING_RETURN.FIPOSTING_RETURN.RETURN_TABLE) || (response.Envelope &&
		response.Envelope.Body && response.Envelope.Body.ZFM_TIMP_ESTORNAR_DOC_CONTABILResponse && response.Envelope.Body.ZFM_TIMP_ESTORNAR_DOC_CONTABILResponse
		.OUT_RETURNTABLE) || response.Envelope && response.Envelope.Body && response.Envelope.Body.OUT_FIPOSTINGVALUES || false;
};

this.getErrorMessagesFromTDF = function(returnTable) {
	var messages = [];
	var distinctMessages = [];
	try {
		var lang = $.request.cookies.get('Content-Language');
		lang = lang === 'enus' ? 'enus' : 'ptrbr';
		let iteratorItem = returnTable && returnTable.item || returnTable;
		iteratorItem = iteratorItem.forEach ? iteratorItem : [];
		iteratorItem.forEach(function(ele) {
			if (ele.TYPE !== 'E') {
				return;
			}
			var message = ele.MESSAGE;

			return messages.push(message);
		});
	} catch (e) {}
	return messages;
};

this.checkMDRAccountingParameters = function(params, correction, listAccountWithouCostCenter) {
	$.import('timp.bsc.server.models.views', 'cvAccounting');
	let view = $.timp.bsc.server.models.views.cvAccounting.CV_J_1BNFLIN;
	let rbco = $.timp.bsc.server.models.views.cvAccounting.RBCO_V;
	$.import('timp.mdr.server.api', 'api');
	let mdrApi = $.timp.mdr.server.api.api;
	let _self = this;
	let clientName = _self.getClientName();
	// let accountingMappingController = mdrApi.accountingMappingForCorrections;
	let defaultCostObjectsController = mdrApi.defaultCostObjects;
	let defaultAccountsStockToCorrectionsController = mdrApi.defaultAccountsStockToCorrections;
	// let scenarioCodes = this.getScenarioConfigurationCodes(correction.scenarioHierarchyId);
	let accountingMapping = this.refactoredGetDataFromMDRController({
		company: correction && correction.cvCorrectionComplementaryInformation && correction.cvCorrectionComplementaryInformation.companyId ||
			correction.companyId,
		branch: correction && correction.cvCorrectionComplementaryInformation && correction.cvCorrectionComplementaryInformation.branchId ||
			correction.branchId,
		// scenarios: scenarioCodes,
		scenarioHierarchyId: correction.scenarioHierarchyId
	}) || {};
	let defaultCostObject = this.getMDRDefaultCostObject(defaultCostObjectsController, {
		company: correction && correction.cvCorrectionComplementaryInformation && correction.cvCorrectionComplementaryInformation.companyId ||
			correction.companyId,
		branch: correction && correction.cvCorrectionComplementaryInformation && correction.cvCorrectionComplementaryInformation.branchId ||
			correction.branchId
	}, true);
	let defaultAccountsStockToCorrections = this.refactoredGetMDRDefaultAccountsStockToCorrectionsController({
		company: correction && correction.cvCorrectionComplementaryInformation && correction.cvCorrectionComplementaryInformation.companyId ||
			correction.companyId,
		branch: correction && correction.cvCorrectionComplementaryInformation && correction.cvCorrectionComplementaryInformation.branchId ||
			correction.branchId,
		// scenarios: scenarioCodes
		scenarioHierarchyId: correction.scenarioHierarchyId
	});
	if (_.isEmpty(defaultAccountsStockToCorrections)) {
		defaultCostObject = {};
	}
	let debitItems = [];
	let creditItems = [];
	accountingMapping = this.setEditCorrectionData(accountingMapping, correction.accountingData);
	// const rbco = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.pb.ajuste_apuracao/RBCO_V', true);
	// let view = $.createBaseRuntimeModel($.viewSchema, 'timp.bsc.modeling/CV_J_1BNFLIN', true);
	let docNo = '';
	let itmText = '';
	if (accountingMapping.attributionField === 1) {
		docNo = correction.nfId;
	} else if (accountingMapping.attributionField === 2) {
		docNo = correction.id;
	} else if (accountingMapping.attributionField === 3) {
		docNo = correction.nfId;
	}
	if (accountingMapping.textField === 1) {
		itmText = correction.id + ' - ' + correction.reason;
	} else if (accountingMapping.textField === 2) {
		itmText = correction.id + ' - ' + correction.reason;
	} else if (accountingMapping.textField === 3) {
		itmText = correction.reason;
	} else if (accountingMapping.textField === 4) {
		let scenario = correction.scenarioConfiguration;
		if (correction.scenarioConfiguration) {
			if (correction.scenarioConfiguration instanceof Array) {
				scenario = correction.scenarioConfiguration[0] || {};
			}
		}
		itmText = correction.id + ' - ' + scenario.id + ' - ' + (correction.rulePath || '-').split('-')[0];
	}
	params['tmf:FIPostingPost'].FIPostingHeaderData.HEADER_TXT = accountingMapping.headerText || params['tmf:FIPostingPost'].FIPostingHeaderData
		.HEADER_TXT;
	params['tmf:FIPostingPost'].FIPostingHeaderData.COMP_CODE = params['tmf:FIPostingPost'].FIPostingHeaderData.COMP_CODE || accountingMapping
		.creditCompany || accountingMapping.debitCompany || '';
	params['tmf:FIPostingPost'].FIPostingHeaderData.PSTNG_DATE = params['tmf:FIPostingPost'].FIPostingHeaderData.PSTNG_DATE ||
		accountingMapping.creditReleaseDate || accountingMapping.debitReleaseDate || '';
	params['tmf:FIPostingPost'].FIPostingHeaderData.DOC_TYPE = params['tmf:FIPostingPost'].FIPostingHeaderData.DOC_TYPE || accountingMapping.creditDocumentType ||
		accountingMapping.debitDocumentType || '';
	params['tmf:FIPostingPost'].FIPostingHeaderData.DOC_DATE = params['tmf:FIPostingPost'].FIPostingHeaderData.DOC_DATE || accountingMapping.creditReleaseDate ||
		'';
	params['tmf:FIPostingPost'].FIPostingHeaderData.TRANS_DATE = params['tmf:FIPostingPost'].FIPostingHeaderData.TRANS_DATE ||
		accountingMapping.creditReleaseDate || '';
	params['tmf:FIPostingPost'].FIPostingHeaderData.FIS_PERIOD = params['tmf:FIPostingPost'].FIPostingHeaderData.FIS_PERIOD || (
		accountingMapping.creditReleaseDate ? (accountingMapping.creditReleaseDate.substr(0, 4) + '/' + accountingMapping.creditReleaseDate.substr(
			4, 2)) : '');
	params['tmf:FIPostingPost'].FIPostingHeaderData.FISC_YEAR = params['tmf:FIPostingPost'].FIPostingHeaderData.FISC_YEAR || (
		accountingMapping.creditReleaseDate ? (accountingMapping.creditReleaseDate.substr(0, 4)) : '');
	params['tmf:FIPostingPost'].BusinessPlace.CompanyCode = params['tmf:FIPostingPost'].BusinessPlace.CompanyCode || accountingMapping.creditCompany ||
		accountingMapping.debitCompany || '';
	params['tmf:FIPostingPost'].BusinessPlace.Branch = params['tmf:FIPostingPost'].BusinessPlace.Branch || accountingMapping.creditBranch ||
		accountingMapping.debitBranch || '';
	_.forEach((params['tmf:FIPostingPost'].FIPostingCurrency.CurrencyTable || []), function(item) {
		if ((item.AMT_DOCCUR && ((item.AMT_DOCCUR.indexOf && item.AMT_DOCCUR.indexOf('-') === -1) || (Number(item.AMT_DOCCUR) >= 0)) &&
			creditItems.length === debitItems.length) || (creditItems.length >= debitItems.length)) { //Debit Accounts
			debitItems.push(item.ITEMNO_ACC);
		} else {
			creditItems.push(item.ITEMNO_ACC);
		}
	});
	_.forEach(params['tmf:FIPostingPost'].FIPostingItemData.ItemsTable || [], function(item, index) {
		let rbcoAccount = {};
		let ekknAccount = {};
		let msegAccount = {};
		item.ITEM_TEXT = itmText || item.ITEM_TEXT;
		item.ALLOC_NMBR = item.ALLOC_NMBR || docNo || '';
		if (debitItems && debitItems.indexOf(item.ITEMNO_ACC) !== -1) {
			// Debit Account
			if (accountingMapping.debitCfmePedido /*&& _.isEmpty(accountingMapping.debitAccount)*/ ) {
				// search information in the RBCO table
				let hasRegistersInMSEG = _self.getMseg(docNo);
				if (hasRegistersInMSEG) {
					var x = 'x';
				} else {
					let result = _self.getRefDocOrigBydocnum(view, correction.nfId);
					if (!_.isEmpty(result)) {
						let refkey = result.REFKEY.substring(0, result.REFKEY.length - 4);
						let gjahr = result.REFKEY.substring(result.REFKEY.length - 4, result.REFKEY.length);
						let item = result.ITMNUM.substring(1, result.ITMNUM.length - 1);
						rbcoAccount = _self.getRbco(rbco, refkey, gjahr, item);
					}
					ekknAccount = _self.getEkkn((correction.nfId), correction.numItem);
					msegAccount = _self.getMsegFieldsForXML((correction.nfId), correction.numItem);
					if (ekknAccount && ekknAccount.hasResponse) {
						rbcoAccount.hasResponse = ekknAccount.hasResponse;
						rbcoAccount.data = _self.mergeRBCOandEKKNData(ekknAccount.data, rbcoAccount.data, msegAccount.data);
					}
					item.NETWORK = _self.getNplnr(docNo, correction.numItem);
					item.ACTIVITY = _self.getVornr(docNo, correction.numItem);
					let pspnr = _self.getPspnrFromEKKN(docNo, correction.numItem);
					if (pspnr) {
						item.WBS_ELEMENT = _self.getPspnr(pspnr);
					}
				}
			}
			var WBS_ELEMENT = '';
			let date = _self.getMomentDate((accountingMapping && accountingMapping.debitReleaseDate) || item.PSTNG_DATE);
			item.GL_ACCOUNT = (accountingMapping && accountingMapping.debitAccount) || (defaultAccountsStockToCorrections &&
				defaultAccountsStockToCorrections.debitAccount) || item.GL_ACCOUNT;
			item.COMP_CODE = (accountingMapping && accountingMapping.debitCompany) || item.COMP_CODE;
			item.PSTNG_DATE = item.PSTNG_DATE || (date.isValid() && date.format('YYYYMMDD')) || item.PSTNG_DATE;
			item.DOC_TYPE = (accountingMapping && accountingMapping.debitDocumentType) || item.DOC_TYPE;
			item.PLANT = (accountingMapping && accountingMapping.debitOperation) || item.PLANT;
			item.PROFIT_CTR = (accountingMapping && (accountingMapping.debitProfitCenter || accountingMapping.profitCenterDebit)) || (rbcoAccount &&
				rbcoAccount.hasResponse && accountingMapping.debitCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && rbcoAccount.data.PRCTR) ||
				defaultCostObject.profitCenter || item.PROFIT_CTR;
			item.COSTCENTER = (accountingMapping && accountingMapping.costCenterDebit) || (rbcoAccount && rbcoAccount.hasResponse &&
				accountingMapping.debitCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && rbcoAccount.data.KOSTL) || defaultCostObject.costCenter ||
				item.COSTCENTER;
			item.ORDERID = (accountingMapping && accountingMapping.debitInternalOrder) || item.ORDERID;
			item.WBS_ELEMENT = (accountingMapping && accountingMapping.debitPepElement) || item.WBS_ELEMENT;
			item.NETWORK = (accountingMapping && accountingMapping.debitNetDiagram) || item.NETWORK;
			item.BUS_AREA = clientName == 'GERDAU' ? ((correction && correction.cvCorrectionComplementaryInformation && correction.cvCorrectionComplementaryInformation.branchId) ||
				correction.branchId || (rbcoAccount.hasResponse && rbcoAccount.GSBER) || (defaultCostObject && defaultCostObject.localPayment) || item
				.BUS_AREA || '') : '';
			if (item.GL_ACCOUNT && Number(item.GL_ACCOUNT).toString().indexOf(4) === 0) {
				item.COSTCENTER = (accountingMapping && accountingMapping.costCenterDebit) || (defaultCostObject && (defaultCostObject.debitCostCenterDebit ||
					defaultCostObject.costCenterDebit)) || item.COSTCENTER;
			}
			if (rbcoAccount && rbcoAccount.hasResponse && (accountingMapping.debitCfmePedido)) {
				item.GL_ACCOUNT = accountingMapping.debitAccount || rbcoAccount.data.SAKNR || (defaultAccountsStockToCorrections &&
					defaultAccountsStockToCorrections.debitAccount);
				item.COSTCENTER = (accountingMapping && accountingMapping.costCenterDebit) || (rbcoAccount && rbcoAccount.hasResponse &&
					accountingMapping.debitCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && rbcoAccount.data.KOSTL) || defaultCostObject.costCenter ||
					item.COSTCENTER;
				// item.PROFIT_CTR = item.PROFIT_CTR || rbcoAccount.data.PRCTR;
				item.ORDERID = item.ORDERID || rbcoAccount.data.AUFNR;
				WBS_ELEMENT = rbcoAccount.data.PS_PSP_PNR != '00000000' ? rbcoAccount.data.PS_PSP_PNR : '';
				if (!_.isEmpty(WBS_ELEMENT)) {
					let pspnr = _self.getPspnr(WBS_ELEMENT);
					item.WBS_ELEMENT = item.WBS_ELEMENT || pspnr;
				}
				item.NETWORK = item.NETWORK || rbcoAccount.data.NPLNR;
				item.PLANT = item.PLANT || rbcoAccount.data.WERKS;
				if (item.NETWORK) {
					item.ACTIVITY = item.ACTIVITY || _self.getVornr(correction.nfId, correction.numItem) || (rbcoAccount.data.VORNR ? rbcoAccount.data.VORNR :
						rbcoAccount.data.VORNR);
				}
				item.ROUTING_NO = rbcoAccount.data.EBELN || '';
				item.ORDER_ITNO = rbcoAccount.data.EBELP || '';
				if (params['tmf:FIPostingPost'].FIPostingExtension2 && params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index]) {
					params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART3 = params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[
						index].VALUEPART3 != '' ? params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART3 : rbcoAccount.data.MENGE ||
						'';
					params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART4 = params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[
						index].VALUEPART4 != '' ? params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART4 : rbcoAccount.data.MATNR ||
						'';
					params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART5 = params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[
						index].VALUEPART5 != '' ? params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART5 : rbcoAccount.data.MEINS ||
						'';
				}
				// item.SEGMENT = rbcoAccount.data.MENGE || '';
				// item.PARTNER_SEGMENT = rbcoAccount.data.VPROZ || '';
				// item.ACTTYPE = rbcoAccount.data.BWTAR;
			}
			if ((rbcoAccount && rbcoAccount.hasResponse && accountingMapping.debitCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && _.isEmpty(
				rbcoAccount.data.PRCTR))) {
				item.PROFIT_CTR = '';
			}
			if ((rbcoAccount && rbcoAccount.hasResponse && accountingMapping.debitCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && _.isEmpty(
				rbcoAccount.data.KOSTL))) {
				item.COSTCENTER = '';
			}
			if (item.ORDERID || item.WBS_ELEMENT || item.ACTIVITY) {
				item.COSTCENTER = '';
			}
		} else {
			// Credit Account
			if (accountingMapping.creditCfmePedido /*&& _.isEmpty(accountingMapping.creditAccount)*/ ) {
				// search information in the RBCO table
				let hasRegistersInMSEG = _self.getMseg(docNo);
				if (hasRegistersInMSEG) {
					var x = 'x';
				} else {
					let result = _self.getRefDocOrigBydocnum(view, correction.nfId);
					if (!_.isEmpty(result)) {
						let refkey = result.REFKEY.substring(0, result.REFKEY.length - 4);
						let gjahr = result.REFKEY.substring(result.REFKEY.length - 4, result.REFKEY.length);
						let item = result.ITMNUM.substring(1, result.ITMNUM.length - 1);
						rbcoAccount = _self.getRbco(rbco, refkey, gjahr, item);
					}
					ekknAccount = _self.getEkkn((correction.nfId), correction.numItem);
					msegAccount = _self.getMsegFieldsForXML((correction.nfId), correction.numItem);
					if (ekknAccount && ekknAccount.hasResponse) {
						rbcoAccount.hasResponse = ekknAccount.hasResponse;
						rbcoAccount.data = _self.mergeRBCOandEKKNData(ekknAccount.data, rbcoAccount.data, msegAccount.data);
					}
					item.NETWORK = _self.getNplnr(docNo, correction.numItem);
					item.ACTIVITY = _self.getVornr(docNo, correction.numItem);
					let pspnr = _self.getPspnrFromEKKN(docNo, correction.numItem);
					if (pspnr) {
						item.WBS_ELEMENT = _self.getPspnr(pspnr);
					}
				}
			}
			var WBS_ELEMENT = '';
			let date = _self.getMomentDate((accountingMapping && accountingMapping.creditReleaseDate) || item.PSTNG_DATE);
			item.GL_ACCOUNT = (accountingMapping && accountingMapping.creditAccount) || (defaultAccountsStockToCorrections &&
				defaultAccountsStockToCorrections.creditAccount) || item.GL_ACCOUNT;
			item.COMP_CODE = (accountingMapping && accountingMapping.creditCompany) || item.COMP_CODE;
			item.PSTNG_DATE = item.PSTNG_DATE || (date.isValid() && date.format('YYYYMMDD')) || item.PSTNG_DATE;
			item.DOC_TYPE = (accountingMapping && accountingMapping.creditDocumentType) || item.DOC_TYPE;
			item.PLANT = (accountingMapping && accountingMapping.creditOperation) || item.PLANT;
			item.PROFIT_CTR = (accountingMapping && (accountingMapping.creditProfitCenter || accountingMapping.profitCenterCredit)) || (rbcoAccount &&
				rbcoAccount.hasResponse && accountingMapping.creditCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && rbcoAccount.data.PRCTR) || (
				defaultCostObject && defaultCostObject.profitCenter) || item.PROFIT_CTR;
			item.COSTCENTER = (accountingMapping && accountingMapping.costCenterDebit) || (rbcoAccount && rbcoAccount.hasResponse &&
				accountingMapping.creditCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && rbcoAccount.data.KOSTL) || defaultCostObject.costCenter ||
				item.COSTCENTER;
			item.ORDERID = (accountingMapping && accountingMapping.creditInternalOrder) || item.ORDERID;
			item.WBS_ELEMENT = (accountingMapping && accountingMapping.creditPepElement) || item.WBS_ELEMENT;
			item.NETWORK = (accountingMapping && accountingMapping.creditNetDiagram) || item.NETWORK;
			item.BUS_AREA = clientName == 'GERDAU' ? ((correction && correction.cvCorrectionComplementaryInformation && correction.cvCorrectionComplementaryInformation.branchId) ||
				correction.branchId || (rbcoAccount.hasResponse && rbcoAccount.GSBER) || (defaultCostObject && defaultCostObject.localPayment) || item
				.BUS_AREA || '') : '';
			if (item.GL_ACCOUNT && Number(item.GL_ACCOUNT).toString().indexOf(4) === 0) {
				item.COSTCENTER = (accountingMapping && accountingMapping.costCenterCredit) || (defaultCostObject && (defaultCostObject.creditCostCenter ||
					defaultCostObject.costCenterCredit)) || item.COSTCENTER;
			}
			if (rbcoAccount && rbcoAccount.hasResponse && (accountingMapping.creditCfmePedido)) {
				item.GL_ACCOUNT = accountingMapping.creditAccount || rbcoAccount.data.SAKNR || (defaultAccountsStockToCorrections &&
					defaultAccountsStockToCorrections.creditAccount);
				item.COSTCENTER = (accountingMapping && accountingMapping.costCenterDebit) || (rbcoAccount && rbcoAccount.hasResponse &&
					accountingMapping.creditCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && rbcoAccount.data.KOSTL) || (defaultCostObject && (
					defaultCostObject.debitCostCenterDebit || defaultCostObject.costCenterDebit || defaultCostObject.costCenter)) || item.COSTCENTER;
				// item.PROFIT_CTR = item.PROFIT_CTR || rbcoAccount.data.PRCTR;
				item.ORDERID = item.ORDERID || rbcoAccount.data.AUFNR;
				WBS_ELEMENT = rbcoAccount.data.PS_PSP_PNR != '00000000' ? rbcoAccount.data.PS_PSP_PNR : '';
				if (!_.isEmpty(WBS_ELEMENT)) {
					let pspnr = _self.getPspnr(WBS_ELEMENT);
					item.WBS_ELEMENT = item.WBS_ELEMENT || pspnr;
				}
				item.NETWORK = item.NETWORK || rbcoAccount.data.NPLNR;
				item.PLANT = item.PLANT || rbcoAccount.data.WERKS;
				if (item.NETWORK) {
					item.ACTIVITY = item.ACTIVITY || _self.getVornr(correction.nfId, correction.numItem) || (rbcoAccount.data.VORNR ? rbcoAccount.data.VORNR :
						rbcoAccount.data.VORNR);
				}
				item.ROUTING_NO = rbcoAccount.data.EBELN || '';
				item.ORDER_ITNO = rbcoAccount.data.EBELP || '';
				if (params['tmf:FIPostingPost'].FIPostingExtension2 && params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index]) {
					params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART3 = params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[
						index].VALUEPART3 != '' ? params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART3 : rbcoAccount.data.MENGE ||
						'';
					params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART4 = params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[
						index].VALUEPART4 != '' ? params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART4 : rbcoAccount.data.MATNR ||
						'';
					params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART5 = params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[
						index].VALUEPART5 != '' ? params['tmf:FIPostingPost'].FIPostingExtension2.Extension2Table[index].VALUEPART5 : rbcoAccount.data.MEINS ||
						'';
				}
				// item.SEGMENT = rbcoAccount.data.MENGE || '';
				// item.PARTNER_SEGMENT = rbcoAccount.data.VPROZ || '';
				// item.ACTTYPE = rbcoAccount.data.BWTAR;
			}
		}
		item.FIS_PERIOD = item.FIS_PERIOD || (item.PSTNG_DATE ? (item.PSTNG_DATE.substr(0, 4) + '/' + item.PSTNG_DATE.substr(4, 2)) : '');
		item.FISC_YEAR = item.FISC_YEAR || (item.PSTNG_DATE ? item.PSTNG_DATE.substr(0, 4) : '');
		// item.PROFIT_CTR = item.PROFIT_CTR;//_.isEmpty(item.PROFIT_CTR) ? '' : '0000000000'.substr(0, 10 - (item.PROFIT_CTR || '').toString().length) + (item.PROFIT_CTR || '').toString();
		item.COSTCENTER = _.isEmpty(item.COSTCENTER) ? '' : '0000000000'.substr(0, 10 - (item.COSTCENTER || '').toString().length) + (item.COSTCENTER ||
			'').toString();
		if ((rbcoAccount && rbcoAccount.hasResponse && accountingMapping.creditCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && _.isEmpty(
			rbcoAccount.data.PRCTR))) {
			item.PROFIT_CTR = '';
		}
		if ((rbcoAccount && rbcoAccount.hasResponse && accountingMapping.creditCfmePedido && !_.isEmpty(rbcoAccount.data.SAKNR) && _.isEmpty(
			rbcoAccount.data.KOSTL))) {
			item.COSTCENTER = '';
		}
		if (!_self.startWith3Or4(item.GL_ACCOUNT) || (listAccountWithouCostCenter && listAccountWithouCostCenter.length &&
			listAccountWithouCostCenter.indexOf(item.GL_ACCOUNT) > -1)) {
			item.COSTCENTER = "";
		}
		if (item.ORDERID || item.WBS_ELEMENT || item.ACTIVITY) {
			item.COSTCENTER = '';
		}
	});
	_.forEach(params['tmf:FIPostingPost'].FIPostingCurrency.CurrencyTable || [], function(item) {
		if (debitItems && debitItems.indexOf(item.ITEMNO_ACC) !== -1) {
			// Debit Account
			item.CURRENCY = (accountingMapping && accountingMapping.debitCoin) || item.CURRENCY;
			item.CURRENCY_ISO = (accountingMapping && accountingMapping.debitCoin) || item.CURRENCY_ISO;
			item.AMT_DOCCUR = Number((accountingMapping && accountingMapping.debitValue) || item.AMT_DOCCUR) || 0;
			if (Number(item.AMT_DOCCUR) < 0) {
				item.AMT_DOCCUR = (Number(item.AMT_DOCCUR) || 0) * -1;
			}
		} else {
			// Credit Account
			item.CURRENCY = (accountingMapping && accountingMapping.creditCoin) || item.CURRENCY;
			item.CURRENCY_ISO = (accountingMapping && accountingMapping.creditCoin) || item.CURRENCY_ISO;
			item.AMT_DOCCUR = Number((accountingMapping && accountingMapping.creditValue) || item.AMT_DOCCUR) || 0;
			if (Number(item.AMT_DOCCUR) > 0) {
				item.AMT_DOCCUR = (Number(item.AMT_DOCCUR) || 0) * -1;
			}
		}
	});
	return params;
};

this.getScenarioConfigurationCodes = function(hierarchyId) {
	let schema = $.schema.slice(1, -1);
	let scenarioHierarchyModel = $.createBaseRuntimeModel(schema, 'MDR::SCENARIO_HIERARCHY');
	let scenarioByHierarchyModel = $.createBaseRuntimeModel(schema, 'MDR::SCENARIO_CONFIGURATION_BY_HIERARCHY');
	let scenarioConfigurationModel = $.createBaseRuntimeModel(schema, 'MDR::SCENARIO_CONFIGURATION');
	let scenario = scenarioConfigurationModel.find({
		aliases: [{
			collection: scenarioConfigurationModel.getIdentity(),
			name: 'scenario',
			isPrimary: true
        }, {
			collection: scenarioHierarchyModel.getIdentity(),
			name: 'hierarchy'
        }, {
			collection: scenarioByHierarchyModel.getIdentity(),
			name: 'item'
        }],
		select: [{
			alias: 'scenario',
			field: 'CODE'
        }, {
			alias: 'scenario',
			field: 'ID'
        }],
		join: [{
			alias: 'item',
			type: 'inner',
			on: [{
				alias: 'item',
				field: 'SCENARIO_CONFIGURATION_ID',
				operator: '$eq',
				value: {
					alias: 'scenario',
					field: 'ID'
				}
            }]
        }, {
			alias: 'hierarchy',
			type: 'inner',
			on: [{
				alias: 'hierarchy',
				field: 'ID',
				operator: '$eq',
				value: {
					alias: 'item',
					field: 'SCENARIO_HIERARCHY_ID'
				}
            }]
        }],
		where: [{
			alias: 'hierarchy',
			field: 'ID',
			operator: '$eq',
			value: hierarchyId
        }]
	});
	if (scenario && scenario.results && scenario.results.length) {
		return scenario.results.map(function(item) {
			return item.CODE;
		});
	}
	return [];
};

this.getDataFromMDRController = function(controller, params) {
	let response = {};
	if (controller && controller.list && params && params.company && params.branch && params.scenarios) {
		response = controller.list({
			filters: {
				company: params.company || params.idCompany,
				branch: params.branch || params.idBranch,
				scenarios: params.scenarios
			},
			orderBy: {
				field: 'id',
				type: 'desc'
			}
		});
		if (response && response.data && response.data.length !== 0) {
			response = response.data[0];
		} else {
			response = {};
		}
	}
	return response;
};

this.refactoredGetDataFromMDRController = function(params) {
	$.import('timp.bsc.server.models.tables', 'orm2ExternalModels');
	const table = $.timp.bsc.server.models.tables.orm2ExternalModels.accountingMappingModelForCorrections;

	let response = {};
	let options = this.getOptionsForMDRGetData(params);

	response.data = table.find(options).results;

	if (response && response.data && response.data.length !== 0) {
		response = response.data[0];
	} else {
		response = {};
	}

	return response;
};

this.getOptionsForMDRGetData = function(params) {
	$.import('timp.bsc.server.models.tables', 'orm2ExternalModels');
	const table = $.timp.bsc.server.models.tables.orm2ExternalModels.accountingMappingModelForCorrections;
	const eefTable = $.timp.bsc.server.models.tables.orm2ExternalModels.accountingMappingForCorrectionsEefModel;
	const fieldsTable = $.timp.bsc.server.models.tables.orm2ExternalModels.accountingMappingForCorrectionsFieldsModel;
	const fieldsToValidationTable = $.timp.bsc.server.models.tables.orm2ExternalModels.accountingMappingForCorrectionsFieldsToValidationModel;
	let schema = $.schema.slice(1, -1);

	let options = {
		aliases: [{
			collection: table.getIdentity(),
			isPrimary: true,
			name: 'accountingMap'
        }, {
			collection: fieldsTable.getIdentity(),
			name: 'fields'
        }, {
			collection: eefTable.getIdentity(),
			name: 'eef'
        }, {
			collection: fieldsToValidationTable.getIdentity(),
			name: 'fieldsToValidation'
        }],
		select: [],
		join: [{
			alias: 'fieldsToValidation',
			type: 'left',
			map: 'fieldsToValidation',
			on: [{
				alias: 'fieldsToValidation',
				field: 'accountingMappingForCorrectionsId',
				operator: '$eq',
				value: {
					alias: 'accountingMap',
					field: 'id'
				}
            }]
        }, {
			alias: 'fields',
			type: 'left',
			map: 'fields',
			on: [{
				alias: 'fields',
				field: 'accountingMappingForCorrectionsId',
				operator: '$eq',
				value: {
					alias: 'accountingMap',
					field: 'id'
				}
            }]
        }, {
			alias: 'eef',
			type: 'left',
			map: 'eef',
			on: [{
				alias: 'eef',
				field: 'accountingMappingForCorrectionsId',
				operator: '$eq',
				value: {
					field: 'id',
					alias: 'accountingMap'
				}
            }]
        }],
		where: [],
		orderBy: [{
			field: 'id',
			alias: 'accountingMap',
			type: 'desc'
        }]
	};

	let where = [];
	if (!_.isNil(params.company)) {
		options.where.push({
			alias: 'eef',
			field: 'companyId',
			operator: '$eq',
			value: params.company
		});
	}
	if (!_.isNil(params.branch)) {
		options.where.push({
			alias: 'eef',
			field: 'branchId',
			operator: '$eq',
			value: params.branch
		});
	}
	// if (!_.isNil(params.scenarios)) {
	//     options.where.push({
	//         alias: 'accountingMap',
	//         field: 'correctionScenario',
	//         operator: '$in',
	//         value: params.scenarios
	//     });
	// }

	if (params.scenarioHierarchyId) {
		let scenarioHierarchyModel = $.timp.bsc.server.models.tables.orm2ExternalModels.scenarioHierarchyModel;
		let scenarioByHierarchyModel = $.timp.bsc.server.models.tables.orm2ExternalModels.scenarioByHierarchyModel;
		let scenarioConfigurationModel = $.timp.bsc.server.models.tables.orm2ExternalModels.scenarioConfigurationModel;

		options.aliases = options.aliases.concat([
			{
				collection: scenarioConfigurationModel.getIdentity(),
				name: 'scenario',
            }, {
				collection: scenarioHierarchyModel.getIdentity(),
				name: 'hierarchy'
            }, {
				collection: scenarioByHierarchyModel.getIdentity(),
				name: 'item'
            }
        ]);
		options.join.push({
			alias: 'scenario',
			type: 'inner',
			on: [{
				alias: 'scenario',
				field: 'CODE',
				operator: '$eq',
				value: {
					alias: 'accountingMap',
					field: 'correctionScenario'
				}
            }]
		});
		options.join = options.join.concat([{
			alias: 'item',
			type: 'inner',
			on: [{
				alias: 'item',
				field: 'SCENARIO_CONFIGURATION_ID',
				operator: '$eq',
				value: {
					alias: 'scenario',
					field: 'ID'
				}
            }]
        }, {
			alias: 'hierarchy',
			type: 'inner',
			on: [{
					alias: 'hierarchy',
					field: 'ID',
					operator: '$eq',
					value: {
						alias: 'item',
						field: 'SCENARIO_HIERARCHY_ID'
					}
            },
				{
					alias: 'hierarchy',
					field: 'ID',
					operator: '$eq',
					value: params.scenarioHierarchyId
            }]
        }]);
	}

	return options;
}

this.getMDRDefaultCostObject = function(controller, params, fromTableWithoutJoin) {
	let response = {};
	if (controller && params && (params.company || params.idCompany) &&
		(params.branch || params.idBranch)) { //&& (params.adjustmentCode || params.codAjuste)
		response = controller.read({
			page: 1,
			company: params.company,
			branch: params.branch,
			launchIndicator: 'X',
			//adjustmentCode: params.adjustmentCode,
			orderBy: {
				field: 'id',
				type: 'desc'
			}
		}, fromTableWithoutJoin);
		if (response && response.data && response.data.length !== 0) {
			response = response.data[0];
		} else {
			response = {};
		}
	}
	return response;
};

this.getMDRDefaultAccountsStockToCorrectionsController = function(controller, params) {
	let response = {};
	if (controller && controller.read && params && (params.company || params.idCompany) &&
		(params.branch || params.idBranch)) { //&& (params.adjustmentCode || params.codAjuste)
		response = controller.read({
			page: 1,
			company: params.company,
			branch: params.branch,
			correctionScenario: params.scenarios,
			orderBy: {
				field: 'id',
				type: 'desc'
			}
		});
		if (response && response.data) {
			response = response.data;
		} else {
			response = {};
		}
	}
	return response;
};

this.refactoredGetMDRDefaultAccountsStockToCorrectionsController = function(params) {
	$.import('timp.bsc.server.models.tables', 'orm2ExternalModels');
	const table = $.timp.bsc.server.models.tables.orm2ExternalModels.defaultAccountsStockToCorrectionsModel;

	let response = {};
	let options = this.getOptionsForMDRGetDefaultAccountsStock(params);

	response.data = table.find(options).results;

	if (response && response.data && response.data.length !== 0) {
		response = response.data[0];
	} else {
		response = {};
	}

	return response;
};

this.getOptionsForMDRGetDefaultAccountsStock = function(params) {
	$.import('timp.bsc.server.models.tables', 'orm2ExternalModels');
	const defaultAccountsStockToCorrectionsModel = $.timp.bsc.server.models.tables.orm2ExternalModels.defaultAccountsStockToCorrectionsModel;
	const defaultAccountsStockToCorrectionsFieldsModel = $.timp.bsc.server.models.tables.orm2ExternalModels.defaultAccountsStockToCorrectionsFieldsModel;
	const eefTable = $.timp.bsc.server.models.tables.orm2ExternalModels.defaultAccountsStockToCorrectionsEefModel;
	const cfopTable = $.timp.bsc.server.models.tables.orm2ExternalModels.defaultAccountsStockToCorrectionsCfopModel;
	const nfCategoryTable = $.timp.bsc.server.models.tables.orm2ExternalModels.defaultAccountsStockToCorrectionsNfCategoryModel;
	let schema = $.schema.slice(1, -1);
	let options = {
		aliases: [{
			collection: defaultAccountsStockToCorrectionsModel.getIdentity(),
			isPrimary: true,
			name: 'defaultAccountsStockToCorrections'
        }, {
			collection: defaultAccountsStockToCorrectionsFieldsModel.getIdentity(),
			name: 'fields'
        }, {
			collection: eefTable.getIdentity(),
			name: 'eef'
        }, {
			collection: cfopTable.getIdentity(),
			name: 'cfop'
        }, {
			collection: nfCategoryTable.getIdentity(),
			name: 'nfCategory'
        }],
		select: [],
		join: [{
			alias: 'eef',
			type: 'left',
			map: 'eef',
			on: [{
				alias: 'eef',
				field: 'idConfiguration',
				operator: '$eq',
				value: {
					field: 'id',
					alias: 'defaultAccountsStockToCorrections'
				}
            }]
        }, {
			alias: 'fields',
			type: 'left',
			map: 'fields',
			on: [{
				alias: 'fields',
				field: 'idConfiguration',
				operator: '$eq',
				value: {
					alias: 'defaultAccountsStockToCorrections',
					field: 'id'
				}
            }]
        }, {
			alias: 'cfop',
			type: 'left',
			map: 'cfop',
			on: [{
				alias: 'cfop',
				field: 'idConfiguration',
				operator: '$eq',
				value: {
					alias: 'defaultAccountsStockToCorrections',
					field: 'id'
				}
            }]
        }, {
			alias: 'nfCategory',
			type: 'left',
			map: 'nfCategory',
			on: [{
				alias: 'nfCategory',
				field: 'idConfiguration',
				operator: '$eq',
				value: {
					alias: 'defaultAccountsStockToCorrections',
					field: 'id'
				}
            }]
        }],
		where: [{
			alias: 'defaultAccountsStockToCorrections',
			field: 'isDeleted',
			operator: '$eq',
			value: 0
        }],
		orderBy: [{
			field: 'id',
			alias: 'defaultAccountsStockToCorrections',
			type: 'desc'
        }]
	};

	if (params.scenarioHierarchyId) {
		let scenarioHierarchyModel = $.timp.bsc.server.models.tables.orm2ExternalModels.scenarioHierarchyModel;
		let scenarioByHierarchyModel = $.timp.bsc.server.models.tables.orm2ExternalModels.scenarioByHierarchyModel;
		let scenarioConfigurationModel = $.timp.bsc.server.models.tables.orm2ExternalModels.scenarioConfigurationModel;

		options.aliases = options.aliases.concat([
			{
				collection: scenarioConfigurationModel.getIdentity(),
				name: 'scenario',
            }, {
				collection: scenarioHierarchyModel.getIdentity(),
				name: 'hierarchy'
            }, {
				collection: scenarioByHierarchyModel.getIdentity(),
				name: 'item'
            }
        ]);
		options.join.push({
			alias: 'scenario',
			type: 'inner',
			on: [{
				alias: 'scenario',
				field: 'CODE',
				operator: '$eq',
				value: {
					alias: 'defaultAccountsStockToCorrections',
					field: 'correctionScenario'
				}
            }]
		});
		options.join = options.join.concat([{
			alias: 'item',
			type: 'inner',
			on: [{
				alias: 'item',
				field: 'SCENARIO_CONFIGURATION_ID',
				operator: '$eq',
				value: {
					alias: 'scenario',
					field: 'ID'
				}
            }, {
				alias: 'item',
				field: 'SCENARIO_HIERARCHY_ID',
				operator: '$eq',
				value: params.scenarioHierarchyId
            }]
        }, {
			alias: 'hierarchy',
			type: 'inner',
			on: [{
					alias: 'hierarchy',
					field: 'ID',
					operator: '$eq',
					value: {
						alias: 'item',
						field: 'SCENARIO_HIERARCHY_ID'
					}
            },
				{
					alias: 'hierarchy',
					field: 'ID',
					operator: '$eq',
					value: params.scenarioHierarchyId
            }]
        }]);
	}

	if (!_.isNil(params.id)) {
		options.where.push({
			alias: 'defaultAccountsStockToCorrections',
			field: 'id',
			operator: '$eq',
			value: Number(params.id)
		});
	}
	if (!_.isNil(params.company)) {
		options.where.push({
			alias: 'eef',
			field: 'companyId',
			operator: '$eq',
			value: params.company
		});
	}
	if (!_.isNil(params.branch)) {
		options.where.push({
			alias: 'eef',
			field: 'branchId',
			operator: '$eq',
			value: params.branch
		});
	}

	return options;
}

this.getRbco = function(controller, docnum, gjahr, item) {
	let response = {
		hasResponse: false,
		data: {}
	};
	try {
		let result = controller.find({
			where: [{
				field: 'BELNR',
				operator: '$eq',
				value: docnum
            }, {
				field: 'GJAHR',
				operator: '$eq',
				value: gjahr
            }, {
				field: 'BUZEI',
				operator: '$like',
				value: '%' + item
            }]
		});
		response.hasResponse = result && result.results && result.results.length !== 0;
		response.data = result && result.results && result.results.length !== 0 && result.results[0] || {};
	} catch (e) {
		response = {
			hasResponse: false,
			data: {}
		};
	}
	return response;
};

this.getEkkn = function(docnum, itemID, subItem, getAllSubItems) {
	$.import('timp.bsc.server.models.views', 'cvAccounting');
	let ekkn = $.timp.bsc.server.models.views.cvAccounting.CV_EKKN;
	// const ekkn = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.pb.accounting_bsc/CV_EKKN', true);
	let response = {
		hasResponse: false,
		data: {}
	};
	try {
		let where = [{
			field: 'DOCNUM',
			operator: '$eq',
			value: docnum
        }];
		let orderBy = [];
		if (itemID) {
			where.push({
				field: 'ITMNUM',
				operator: '$eq',
				value: itemID
			});
		}
		if (subItem) {
			where.push({
				field: 'ZEKKN',
				operator: '$eq',
				value: subItem
			});
		}
		if (getAllSubItems) {
			orderBy = [{
				"field": "ZEKKN"
            }];
		}
		let result = ekkn.find({
			where: where,
			orderBy: orderBy
		});
		response.hasResponse = result && result.results && result.results.length !== 0;
		if (getAllSubItems) {
			response.data = result && result.results && result.results.length !== 0 && result.results || [];
		} else {
			response.data = result && result.results && result.results.length !== 0 && result.results[0] || {};
		}
	} catch (e) {}
	return response;
};

this.getMsegFieldsForXML = function(docnum, itemID, subItem) {
	$.import('timp.bsc.server.models.views', 'cvAccounting');
	let mseg = $.timp.bsc.server.models.views.cvAccounting.CV_MSEG;
	// const mseg = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.pb.accounting_bsc/CV_MSEG', true);
	let response = {
		hasResponse: false,
		data: {}
	};
	try {
		let where = [{
			field: 'DOCNUM',
			operator: '$eq',
			value: docnum
        }];
		let orderBy = [];
		if (itemID) {
			where.push({
				field: 'ITMNUM',
				operator: '$eq',
				value: itemID
			});
		}
		let result = mseg.find({
			where: where,
			orderBy: orderBy
		});
		response.hasResponse = result && result.results && result.results.length !== 0;
		response.data = result && result.results && result.results.length !== 0 && result.results[0] || {};
	} catch (e) {}
	return response;
};

this.getPspnr = function(psPspPnr) {
	// const prps = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.pb.accounting_bsc/CV_PRPS', true);
	$.import('timp.bsc.server.models.views', 'cvAccounting');
	let prps = $.timp.bsc.server.models.views.cvAccounting.CV_PRPS;
	let pspnr = '';
	try {
		let result = prps.find({
			where: [{
				field: 'OBJNR',
				operator: '$like',
				value: '%' + psPspPnr
            }]
		});
		pspnr = result && result.results && result.results.length !== 0 && result.results[0].POSID || '';
	} catch (e) {
		$.logError(null, e);
	}
	return pspnr;
};

this.getVornr = function(docnum, itmnum) {
	// const afvc = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.pb.accounting_bsc/CV_AFVC', true);
	$.import('timp.bsc.server.models.views', 'cvAccounting');
	let afvc = $.timp.bsc.server.models.views.cvAccounting.CV_AFVC;
	let vornr = '';
	try {
		let result = afvc.find({
			where: [{
				field: 'DOCNUM',
				operator: '$eq',
				value: docnum
            }]
		});
		vornr = result && result.results && result.results.length !== 0 && (result.results[0].VORNR || result.results[0].VORNR_1) || '';
	} catch (e) {
		$.logError(null, e);
	}
	return vornr;
};

this.getMseg = function(docnum) {
	const mseg = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.pb.accounting_bsc/CV_MSEG', true);
	let hasMSEG = false;
	try {
		let result = mseg.find({
			where: [{
				field: 'DOCNUM',
				operator: '$eq',
				value: docnum
            }, {
				field: 'LGORT',
				operator: '$neq',
				value: ''
            }],
			limit: 1
		});
		hasMSEG = result && result.results && result.results.length !== 0;
	} catch (e) {
		$.logError(null, e);
	}
	return hasMSEG;
};

this.getNplnr = function(docnum) {
	$.import('timp.bsc.server.models.views', 'cvAccounting');
	let afvc = $.timp.bsc.server.models.views.cvAccounting.CV_AFVC;
	// const afvc = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.pb.accounting_bsc/CV_AFVC', true);
	let nplnr = '';
	try {
		let result = afvc.find({
			where: [{
				field: 'DOCNUM',
				operator: '$eq',
				value: docnum
            }, {
				field: 'NPLNR',
				operator: '$neq',
				value: ''

            }]
		});
		nplnr = result && result.results && result.results.length !== 0 && result.results[0].NPLNR || '';
	} catch (e) {
		$.logError(null, e);
	}
	return nplnr;
};

this.getPspnrFromEKKN = function(docnum, itemID, subItem) {
	$.import('timp.bsc.server.models.views', 'cvAccounting');
	let afvc = $.timp.bsc.server.models.views.cvAccounting.CV_AFVC;
	// const afvc = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.client.pb.accounting_bsc/CV_AFVC', true);
	let pspnr = '';
	try {
		let where = [{
			field: 'DOCNUM',
			operator: '$eq',
			value: docnum
        }, {
			field: 'PS_PSP_PNR',
			operator: '$neq',
			value: '00000000'
        }];
		if (itemID) {
			where.push({
				field: 'ITMNUM',
				operator: '$eq',
				value: itemID
			});
		}
		if (subItem) {
			where.push({
				field: 'ZEKKN',
				operator: '$eq',
				value: subItem
			});
		}
		let result = afvc.find({
			where: where
		});
		pspnr = result && result.results && result.results.length !== 0 && result.results[0].PS_PSP_PNR || '';
	} catch (e) {
		$.logError(null, e);
	}
	return pspnr;
};

this.mergeRBCOandEKKNData = function(ekknData, rbcoData, msegData) {
	rbcoData = rbcoData || {};
	ekknData = ekknData || {};
	msegData = msegData || {};
	rbcoData.PRCTR = ekknData.PRCTR || rbcoData.PRCTR; //PROFIT_CTR
	rbcoData.KOSTL = ekknData.KOSTL || rbcoData.KOSTL; //COSTCENTER
	rbcoData.SAKNR = ekknData.SAKTO || rbcoData.SAKNR; //GL_ACCOUNT
	rbcoData.AUFNR = ekknData.AUFNR || rbcoData.AUFNR; //ORDERID
	rbcoData.NPLNR = ekknData.NPLNR || rbcoData.NPLNR; //NETWORK
	rbcoData.EBELN = ekknData.EBELN; //ROUTING_NO
	rbcoData.EBELP = ekknData.EBELP; //ORDER_ITNO
	rbcoData.MENGE = ekknData.MENGE; //SEGMENT
	rbcoData.MATNR = ekknData.MATNR || rbcoData.MATNR;
	rbcoData.MEINS = ekknData.MEINS || rbcoData.MEINS;
	rbcoData.VPROZ = ekknData.VPROZ; //PARTNER_SEGMENT
	rbcoData.BWTAR = msegData.BWTAR; //ACTTYPE
	rbcoData.WERKS = msegData.WERKS; //PLANT
	return rbcoData;
};

this.getRefDocOrigBydocnum = function(controller, nfId) {
	let response = {
		hasResponse: false,
		data: {}
	};
	try {
		let result = controller.find({
			select: [
				{
					'field': 'REFKEY'
				},
				{
					'field': 'ITMNUM'
				}
            ],
			where: [{
				field: 'DOCNUM',
				operator: '$eq',
				value: nfId
            }],
			orderBy: [{
				field: 'REFKEY',
				type: 'desc'
            }]
		});
		response.hasResponse = result && result.results && result.results.length !== 0;
		response.data = result && result.results && result.results.length !== 0 && result.results[0] || {};
	} catch (e) {
		$.logError('', e);
	}
	return response.data || '';
};

this.getSequenceField = function(correction, fieldsMap) {
	let response = '';
	const bnflin = $.createBaseRuntimeModel($.viewSchema, 'timp.bsc.modeling/CV_J_1BNFLIN', true);
	const t003 = $.createBaseRuntimeModel($.viewSchema, 'timp.bsc.modeling/CV_T003', true);
	const bkpf = $.createBaseRuntimeModel($.viewSchema, 'timp.bsc.modeling/CV_BKPF', true);
	const nriv = $.createBaseRuntimeModel($.viewSchema, 'timp.bsc.modeling/CV_NRIV', true);
	try {
		if (bnflin.exists().results.exists && t003.exists().results.exists && bkpf.exists().results.exists &&
			nriv.exists().results.exists) {
			let numkr = t003.find({
				aliases: [{
					collection: t003.getIdentity(),
					name: 't003',
					isPrimary: true
                }, {
					collection: bkpf.getIdentity(),
					name: 'bkpf'
                }, {
					collection: bnflin.getIdentity(),
					name: 'bnflin'
                }],
				select: [{
					field: 'BLART',
					alias: 't003'
                }, {
					field: 'BELNR',
					alias: 'bkpf'
                }, {
					field: 'NUMKR',
					alias: 't003'
                }],
				join: [{
					alias: 'bkpf',
					type: 'inner',
					on: [{
						alias: 't003',
						field: 'BLART',
						operator: '$eq',
						value: {
							alias: 'bkpf',
							field: 'BLART'
						}
                    }]
                }, {
					alias: 'bnflin',
					type: 'inner',
					on: [{
						alias: 'bkpf',
						field: 'BELNR',
						operator: '$eq',
						value: {
							'function': {
								name: 'LEFT',
								params: [{
										alias: 'bnflin',
										field: 'REFKEY'
                                },
									10]
							}
						}
                    }]
                }],
				where: [{
					alias: 'bnflin',
					field: 'DOCNUM',
					operator: '$eq',
					value: correction.nfId
                }, {
					alias: 'bnflin',
					field: 'ITMNUM',
					operator: '$eq',
					value: correction.numItem
                }]
			});
			if (numkr.errors.length === 0 && numkr.results.length > 0) {
				let useValue = nriv.find({
					fields: [{
						field: 'FROMNUMBER'
                    }, {
						field: 'TONUMBER'
                    }, {
						field: 'EXTERNIND'
                    }],
					where: [{
						field: 'OBJECT',
						operator: '$eq',
						value: 'RF_BELEG'
                    }, {
						field: 'SUBOBJECT',
						operator: '$eq',
						value: fieldsMap.COMPANY_CODE
                    }, {
						field: 'NRRANGENR',
						operator: '$eq',
						value: numkr.results[0].NUMKR
                    }]
				});
				if (useValue.errors.length === 0 && useValue.results.length > 0) {
					if (useValue.results[0].EXTERNIND === 'X') {
						let sequenceValue = bkpf.find({
							select: [{
								field: 'BELNR'
                            }, {
								field: 'BUKRS'
                            }, {
								field: 'GJAHR'
                            }, {
								field: 'BLART'
                            }],
							where: [{
								field: 'BUKRS',
								operator: '$eq',
								value: fieldsMap.COMPANY_CODE
                            }, {
								field: 'GJAHR',
								operator: '$eq',
								value: fieldsMap.YEAR
                            }, {
								field: 'BLART',
								operator: '$eq',
								value: numkr.results[0].BLART
                            }],
							orderBy: [{
								field: 'BELNR',
								type: 'desc'
                            }]
						});
						if (sequenceValue.errors.length === 0 && sequenceValue.results.length > 0) {
							let value = sequenceValue.results[0].BELNR;
							let lastNumber = value.charAt(value.length - 1);
							response = value.substr(value.length - 1) + (Number(lastNumber) + 1);
						}
					}
				}
			}
		}
	} catch (e) {
		$.messageCodes.push({
			type: 'E',
			errorInfo: util.parseError(e)
		});
	}
	return response;
};

this.startWith3Or4 = function(textValue) {
	let firstNumberExceptZero = 0;

	if (textValue && typeof textValue === "string") {

		let arrayTextValue = textValue.split("");
		let index = 0;

		while (!firstNumberExceptZero && index < arrayTextValue.length) {
			let value = arrayTextValue[index];
			firstNumberExceptZero = Number(value);
			index++;
		}
	}

	return firstNumberExceptZero ? [3, 4].indexOf(firstNumberExceptZero) > -1 : false;
};

/*
@param correction {object}
@param headerFields {array of items}
@example headerFields = [{
    itemGroupId:<number>,
    fieldName:<string>,
    value:<string>
},...]
*/
this.getAccountingItems = function(correction, headerFields, listAccountWithouCostCenter, isGrouped) {
	var response = {
		itemsTable: [],
		vendorsTable: [],
		currencyTable: [],
		extension2Table: []
	};
	const _self = this;
	var referencesValues = isGrouped ? $.lodash.map(correction, function(item) {
		return item.itemFields;
	}).reduce(function(b, c) {
		return b.concat(c)
	}) : correction.itemFields;

	var items = [];
	var itemGroupMap = $.lodash.reduce(referencesValues, (obj, itemField) => {
		obj[itemField.itemGroupId] = obj[itemField.itemGroupId] || [];
		obj[itemField.itemGroupId].push(itemField);
		return obj;
	}, {});
	// Implementación de Usar Referencia
	$.lodash.forEach(itemGroupMap, (itemGroup) => {
		let fields = [];
		var itemObject = $.lodash.reduce(itemGroup, (obj, itemField) => {
			obj[itemField.fieldName] = itemField.value;
			if (itemField.useReference) {
				fields.push(itemField.fieldName);
			}
			return obj;
		}, {});
		if (fields.length) {
			let values = this.getReferenceFields(correction.nfId, correction.numItem, fields);
			$.lodash.forEach(values, (value, key) => {
				itemObject[key] = !$.lodash.isNil(value) && !$.lodash.isEmpty(value) ? value : itemObject[key];
			});
		}
		items.push(itemObject);
	});
	if (items.length === 0) {
		items = [{}, {}];
	}
	// Fin Implementación
	$.lodash.forEach(items, function(item, j) {
		// item.PROFIT_CTR = item.PROFIT_CTR;//_.isEmpty(item.PROFIT_CTR) ? '' : '0000000000'.substr(0, 10 - (item.PROFIT_CTR || '').toString().length) + (item.PROFIT_CTR || '').toString();
		item.COSTCENTER = _.isEmpty(item.COSTCENTER) ? '' : '0000000000'.substr(0, 10 - (item.COSTCENTER || '').toString().length) + (item.COSTCENTER ||
			'').toString();
		var postDate = headerFields.PSTNG_DATE;
		if (!_.isNil(item.PSTNG_DATE)) {
			postDate = item.PSTNG_DATE;
		}
		postDate = $.moment(postDate, 'YYYYMMDD');
		var itemNo = j < 9 ? '00' : (j < 99 ? '0' : '');
		var itemObj = {
			ITEMNO_ACC: item.ITEMNO_ACC || (itemNo + (j + 1)),
			GL_ACCOUNT: item.GL_ACCOUNT || '',
			COMP_CODE: headerFields.COMPANY_CODE || '',
			PSTNG_DATE: item.PSTNG_DATE || headerFields.PSTNG_DATE || '',
			DOC_TYPE: '',
			AC_DOC_NO: '',
			FISC_YEAR: postDate.isValid() ? postDate.format('YYYY') : (headerFields.YEAR || ''),
			FIS_PERIOD: postDate.isValid() ? postDate.format('YYYY/MM') : (headerFields.YEAR && headerFields.MONTH ? (headerFields.YEAR + '/' +
				headerFields.MONTH) : ''),
			STAT_CON: '',
			REF_KEY_1: '',
			REF_KEY_2: '',
			REF_KEY_3: '',
			CUSTOMER: '',
			VENDOR_NO: headerFields.VENDOR_NO || '',
			ALLOC_NMBR: item.REF_DOC_NO || '',
			ITEM_TEXT: item.ITEM_TEXT || headerFields.HEADER_TXT || '',
			BUS_AREA: headerFields.BRANCH || '',
			COSTCENTER: item.COSTCENTER || '',
			ACTTYPE: '',
			ORDERID: headerFields.ORDERID || '',
			ORIG_GROUP: '',
			COST_OBJ: '',
			PROFIT_CTR: item.PROFIT_CTR || '',
			PART_PRCTR: '',
			WBS_ELEMENT: headerFields.WBS_ELEMENT || '',
			NETWORK: headerFields.NETWORK || '',
			ROUTING_NO: '',
			ORDER_ITNO: '',
			ACTIVITY: '',
			PLANT: item.PLANT || '',
			SALES_ORD: '',
			S_ORD_ITEM: '',
			SEGMENT: '',
			PARTNER_SEGMENT: ''
		};

		if (!_self.startWith3Or4(itemObj.GL_ACCOUNT) || (listAccountWithouCostCenter.length && listAccountWithouCostCenter.indexOf(itemObj.GL_ACCOUNT) >
			-1)) {
			itemObj.COSTCENTER = "";
		}

		response.itemsTable.push(itemObj);

		var vendorObj = {
			ITEMNO_ACC: item.ITEMNO_ACC || (itemNo + (j + 1)),
			VENDOR_NO: (item.VENDOR_NO || '') || ((!itemObj.itemsTable || itemObj.GL_ACCOUNT !== '') && itemObj.COSTCENTER ? item.COSTCENTER : ''),
			GL_ACCOUNT: '',
			REF_KEY_1: '',
			REF_KEY_2: '',
			REF_KEY_3: '',
			COMP_CODE: headerFields.COMP_CODE || headerFields.COMPANY_CODE || '',
			BUS_AREA: headerFields.BRANCH || '',
			PMNTTRMS: '',
			BLINE_DATE: '',
			DSCT_DAYS1: '',
			DSCT_DAYS2: '',
			NETTERMS: '',
			DSCT_PCT1: '',
			DSCT_PCT2: '',
			PYMT_METH: '',
			PMTMTHSUPL: '',
			PMNT_BLOCK: '',
			SCBANK_IND: '',
			SUPCOUNTRY: '',
			SUPCOUNTRY_ISO: '',
			BLLSRV_IND: '',
			ALLOC_NMBR: item.REF_DOC_NO || '',
			ITEM_TEXT: item.ITEM_TEXT || '',
			PO_SUB_NO: '',
			PO_CHECKDG: '',
			PO_REF_NO: '',
			W_TAX_CODE: '',
			BUSINESSPLACE: headerFields.BRANCH || '',
			SECTIONCODE: '',
			INSTR1: '',
			INSTR2: '',
			INSTR3: '',
			INSTR4: '',
			BRANCH: item.BRANCH || '',
			PYMT_CUR: '',
			PYMT_AMT: '',
			PYMT_CUR_ISO: '',
			SP_GL_IND: '',
			TAX_CODE: '',
			TAX_DATE: '',
			TAXJURCODE: '',
			ALT_PAYEE: '',
			ALT_PAYEE_BANK: '',
			PARTNER_BK: (item.PARTNER_BK || '') || ((!itemObj.itemsTable || itemObj.GL_ACCOUNT !== '') && itemObj.PROFIT_CTR ? item.PROFIT_CTR :
				''),
			BANK_ID: '',
			PARTNER_GUID: '',
			PROFIT_CTR: '',
			FUND: '',
			GRANT_NBR: '',
			MEASURE: '',
			HOUSEBANKACCTID: '',
			BUDGET_PERIOD: '',
			PPA_EX_IND: '',
			PART_BUSINESSPLACE: ''
		};

		let getValue = function(number) {
			let response = $.lodash.isNumber(number) ? number : '';
			if (!$.lodash.isNil(number) && $.lodash.isString(number)) {
				if (number.lastIndexOf('.') > number.lastIndexOf(',')) {
					response = number.replace(/,/g, '');
				} else {
					response = number.replace(/\./g, '').replace(',', '.');
				}
			}
			return response;
		};
		response.currencyTable.push({
			ITEMNO_ACC: item.ITEMNO_ACC || (itemNo + (j + 1)),
			CURR_TYPE: '',
			CURRENCY: item.CURRENCY || headerFields.CURRENCY || 'BRL',
			CURRENCY_ISO: item.CURRENCY || headerFields.CURRENCY || 'BRL',
			AMT_DOCCUR: !$.lodash.isNil(item.AMT_DOCCUR) ? getValue(item.AMT_DOCCUR) : 0,
			EXCH_RATE: '',
			EXCH_RATE_V: '',
			AMT_BASE: '',
			DISC_BASE: '',
			DISC_AMT: '',
			TAX_AMT: ''
		});
		var extensionItemBase = '0000000000';
		var extensionItemNo = extensionItemBase.substr(0, extensionItemBase.length - ((j + 1) + '').length) + ((j + 1) + '');
		let clientName = _self.getClientName();
		response.extension2Table.push({
			STRUCTURE: clientName == 'GERDAU' ? 'ZST_ACC_DOCUMENT' : 'BUPLA',
			VALUEPART1: extensionItemNo,
			VALUEPART2: headerFields.BRANCH || '',
			VALUEPART3: item.QUANTITY || '',
			VALUEPART4: item.MATERIAL || '',
			VALUEPART5: item.MEASURE || ''
		});
	});
	return response;
};

this.listVariants = function() {
	const list = variantModel.READ({
		where: [{
				field: 'userId',
				oper: '=',
				value: $.session.getUsername()
            },
            [{
				field: 'isDeleted',
				oper: '=',
				value: 0
            }, {
				field: 'isDeleted',
				oper: 'IS NULL'
            }]
        ]
	});
	return list;
};

this.saveVariant = function(object) {
	if (object.id) {
		variantModel.UPDATE(object);
	} else {
		const variantToSave = {
			userId: $.session.getUsername(),
			component: 'BSC',
			name: object.name,
			description: object.description,
			filters: object.filters,
			variantDefault: false
		};
		variantModel.CREATE(variantToSave);
	}
	return true;
};

this.defaultVariant = function(object) {
	if (object.name) {
		const list = variantModel.READ({
			where: [{
					field: 'userId',
					oper: '=',
					value: $.session.getUsername()
                },
                [{
					field: 'isDeleted',
					oper: '=',
					value: 0
                }, {
					field: 'isDeleted',
					oper: 'IS NULL'
                }]
            ]
		});
		const variant = list.filter(function(currentVariant) {
			return currentVariant.name === object.name;
		})[0];
		if (!variant) {
			return false;
		}
		list.forEach(function(variant) {
			variant.variantDefault = variant.name === object.name ? true : false;
			variantModel.UPDATE(variant);
		});
	}
	return true;
};

this.deleteVariant = function(object) {
	if (object.id) {
		variantModel.UPDATEWHERE({
			isDeleted: 1
		}, [{
			field: 'id',
			oper: '=',
			value: object.id
            }]);
		return true;
	} else {
		return false;
	}
};

this.getMandtTdfFromTDF = function(company, branch) {
	let mandtTdf = '';
	let tdf_rfc_destinations_by_branch = $.createBaseRuntimeModel($.viewSchema, 'timp.atr.modeling.tdf_rfc/TDF_RFC_DESTINATIONS_BY_BRANCH',
		true);
	let result = tdf_rfc_destinations_by_branch.find({
		select: [{
			field: 'MANDT_TDF'
        }],
		where: [{
			field: 'BUKRS',
			operator: '$eq',
			value: company
        }, {
			field: 'BRANCH',
			operator: '$eq',
			value: branch
        }]
	});
	mandtTdf = result && result.results && result.results.length !== 0 && result.results[0].MANDT_TDF || '';
	return mandtTdf;
}

this.getHttpsActivity = function(serviceName) {
	let query = 'SELECT IS_HTTPS_ENABLED FROM "' + $.schema.slice(1, -1) + '"."CORE::SERVICE" AS A INNER JOIN "' + $.schema.slice(1, -1) +
		'"."CORE::SERVICE_DESTINATION" AS B ON A.ID = B.SERVICE_ID WHERE A.NAME = ?';
	let getHttpsQuery = {
		query: query,
		values: [serviceName],
		returnType: 'Array'
	};
	let response = coreApi.OrmHighPerformance.__runSelect__(getHttpsQuery)[0];
	if (Array.isArray(response)) {
		response = response[0] == '1' ? true : false;
	}
	return response;
}

this.checkCorrectionScenarioForStructureID = function(pendingList) {
	try {
		let scenariosIds = pendingList.map(function(element) {
			return (element.rulePath || '').split('-')[2];
		}).filter(function(element) {
			return !_.isNil(element);
		}).map(function(element) {
			return Number(element);
		});
		let query =
			`
            SELECT "ID", "STRUCTURE_ID" FROM "${$.schema.slice(1, -1)}"."MDR::SCENARIO_CONFIGURATION"
            WHERE "ID" IN (${scenariosIds.join(', ')});
        `;
		let response = coreApi.OrmHighPerformance.__runSelect__({
			query: query,
			returnType: 'Array'
		});
		response.forEach(function(scenario) {
			pendingList.forEach(function(correction) {
				let scenarioId = (correction.rulePath || '').split('-')[2];
				scenarioId = scenarioId ? Number(scenarioId) : scenarioId;
				if (scenario[0] == scenarioId) {
					correction.structureId = scenario[1];
				}
			});
		});
		return pendingList;
	} catch (e) {
		var x = e;
	}
}

let setEditDataToHeader = function(correction) {
	let date, month, year;
	if (isValidEditData(correction)) {
		date = correction.editData.creditReleaseDate;
		month = date.substring(4, 6);
		year = date.substring(0, 4);
	}

	let branch = correction.editData.debitBranch == correction.editData.creditBranch ? correction.editData.debitBranch : false;

	if (!branch) {
		let filtered = correction.accountingFields.filter(field => field.fieldName == 'BRANCH');
		branch = filtered && filtered[0] && filtered[0].value;
	}

	correction.headerFields.forEach(field => {
		switch (field.fieldName) {
			case 'PSTNG_DATE':
				field.value = date || field.value;
				break;
			case 'YEAR':
				field.value = year || field.value;
				break;
			case 'MONTH':
				field.value = month || field.value;
				break;
			case 'BRANCH':
				field.value = branch || field.value;
				break;
		}
	});

	return correction.headerFields;
};

let isValidEditData = function(correction) {
	return correction &&
		correction.editData &&
		correction.editData.creditReleaseDate &&
		correction.editData.debitReleaseDate &&
		(correction.editData.creditReleaseDate == correction.editData.debitReleaseDate) &&
		correction.editData.debitReleaseDate != '';
};

this.getClientName = function() {
	let coreConfigModel = $.createBaseRuntimeModel($.schema.slice(1, -1), 'CORE::CONFIGURATION', false, true);
	let configuration = coreConfigModel.find({
		select: [{
			field: 'value'
        }],
		where: [{
			field: 'key',
			operator: '$eq',
			value: 'TIMP::ClientName'
        }]
	}).results;

	let response = configuration && configuration.length ? configuration[0].value : 'ALLTAX';
	return response;
};

let correctCTRValues = function (shadowObject, META) {
    try {
        let fields = Object.keys(shadowObject);
        
        if(fields.length) {
            let query =
	    	`
                SELECT TOP 1 ${fields.join(', ')}
                FROM "${$.viewSchema}"."${SAP_NF_VIEW}" ${META.placeHolders}
                WHERE (
                    "MANDT" = '${shadowObject.MANDT}'
                    AND "NF_ID" = '${shadowObject.NF_ID}'
                    AND "NUM_ITEM" = '${shadowObject.NUM_ITEM}'
                    AND "TIPO_IMPOSTO" = '${shadowObject.TIPO_IMPOSTO}'
                    AND "SYS_IMPOSTO" = '${shadowObject.SYS_IMPOSTO}'
                )
            `;
        
	        let executionObject = {
	        	query: query,
	        	returnType: 'Array'
	        };
	        let temp = coreApi.OrmHighPerformance.__runSelect__(executionObject);
	        if(temp.length) {
	            let line = temp[0];
	            let numericFields = ['ALIQUOTA','BASE','VALOR_IMP','BASE_EXCLUIDA','OUTRAS_BASE', 'BASE_PAUTA', 'VL_ABAT_NT_ICMS', 'VL_ABAT_NT_IPI', 'FACTOR', 'ALIQ_BASE_IMP', 'ALIQUOTA_CONT_ICMS'];
	            fields.forEach((field, idx) => {
	                if(line[idx] && !isNaN(line[idx]) && numericFields.indexOf(field) != -1) {
	                    shadowObject[field] = Number(line[idx])
	                } else {
	                    shadowObject[field] = line[idx];
	                }
	            });
	        }
        }
        
        return shadowObject;
    } catch (e) {
        return shadowObject;
    }
}