$.import('timp.log.server.api','api');
var logApi = $.timp.log.server.api.api;
var Log = logApi.logController;
var LogError = logApi.logErrorController;
function Supervisor(){}
/*----------------------------BEGIN CORRECTIONS----------------------------*/
Supervisor.prototype.createCorrection = function(correction){
    var json = {
        details:[],
        properties:[{
            property:'fieldName',
            enus:"Field Name",
            ptrbr:"Nome do Campo"
        },{
            property: 'oldValue',
            enus: "Old Value to correct",
            ptrbr: "Valor Antigo a corrigir"
        },{
            property: 'newValue',
            enus: "New Value",
            ptrbr: "Valor Novo"
        }],
        nfId:correction.nfId,
        numItem: correction.numItem
    };
    if(correction && correction.createdFields){
        $.lodash.forEach(correction.createdFields,function(createdField){
            json.details.push({
                fieldName:{
                    enus:createdField.fieldName,
                    ptrbr:createdField.fieldName
                },
                oldValue:{
                    enus:createdField.oldValue,
                    ptrbr:createdField.oldValue
                },
                newValue:{
                    enus:createdField.newValue,
                    ptrbr:createdField.newValue
                }
            });
        });
        
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207000',
        objectId: correction.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.compareFields = function(oldFields,newFields){
    var response = {
        updatedFields:[]
    };
    try{
        var propertiesToCompare = ['oldValue','newValue'];
        var oldFieldsMap = $.lodash.reduce(oldFields,function(obj,f){
            obj[f.id] = f;
            return obj;
        },{});
        var newFieldsMap = $.lodash.reduce(newFields,function(obj,f){
            obj[f.id] = f;
            return obj;
        },{});
        $.lodash.forEach(propertiesToCompare,function(prop){
            $.lodash.forEach(newFieldsMap,function(field,fieldId){
                if(field[prop] != oldFieldsMap[fieldId][prop]){
                    response.updatedFields.push({
                        fieldName:field.fieldName,
                        prop:prop,
                        oldValue:oldFieldsMap[fieldId][prop],
                        newValue:field[prop]
                    });
                }
            });
        });
    }catch(e){
        response.error = e;
    }
    return response;
};
Supervisor.prototype.compareHeaderFields = function(oldFields,newFields){
    var response = {
        updatedFields:[]
    };
    try{
        var propertiesToCompare = ['value'];
        var oldFieldsMap = $.lodash.reduce(oldFields,function(obj,f){
            obj[f.id] = f;
            return obj;
        },{});
        var newFieldsMap = $.lodash.reduce(newFields,function(obj,f){
            obj[f.id] = f;
            return obj;
        },{});
        $.lodash.forEach(propertiesToCompare,function(prop){
            $.lodash.forEach(newFieldsMap,function(field,fieldId){
                if(field[prop] != oldFieldsMap[fieldId][prop]){
                    response.updatedFields.push({
                        fieldName:field.fieldName,
                        prop:prop,
                        oldValue:oldFieldsMap[fieldId][prop],
                        newValue:field[prop]
                    });
                }
            });
        });
    }catch(e){
        response.error = e;
    }
    return response;
};
Supervisor.prototype.compareItemFields = function(oldFields,newFields){
    var response = {
        updatedFields:[]
    };
    try{
        var propertiesToCompare = ['value'];
        var oldFieldsMap = $.lodash.reduce(oldFields,function(obj,f){
            obj[f.id] = f;
            return obj;
        },{});
        var newFieldsMap = $.lodash.reduce(newFields,function(obj,f){
            obj[f.id] = f;
            return obj;
        },{});
        $.lodash.forEach(propertiesToCompare,function(prop){
            $.lodash.forEach(newFieldsMap,function(field,fieldId){
                if(field[prop] != oldFieldsMap[fieldId][prop]){
                    response.updatedFields.push({
                        fieldName:field.fieldName,
                        prop:prop,
                        oldValue:oldFieldsMap[fieldId][prop],
                        newValue:field[prop]
                    });
                }
            });
        });
    }catch(e){
        response.error = e;
    }
    return response;
};
Supervisor.prototype.compareComments = function(oldComments,newComments){
    var response = {
        updatedComments:[]
    };
    try{
        if(oldComments.length === 0){
            $.lodash.forEach(newComments,function(newComment){
                response.updatedComments.push({
                    oldValue:"",
                    newValue:newComment.comment
                });
            });
        }else{
            if(oldComments[0].comment !== newComments[0].comment){
                response.updatedComments.push({
                    oldValue: oldComments[0].comment,
                    newValue: newComments[0].comment
                });
            }
        }
    }catch(e){
        response.error = e;
    }
    return response;
};
Supervisor.prototype.compareLinks = function(oldLinks,newLinks){
    var response = {
        updatedLinks:[]
    };
    try{
        if(oldLinks.length === 0){
            $.lodash.forEach(newLinks,function(newLink){
                response.updatedLinks.push({
                    oldValue:"",
                    newValue:newLink.link
                });
            });
        }else{
            if(oldLinks[0].link !== newLinks[0].link){
                response.updatedLinks.push({
                    oldValue: oldLinks[0].link,
                    newValue: newLinks[0].link
                });
            }
        }
    }catch(e){
        response.error = e;
    }
    return response;
};
Supervisor.prototype.compareCorrection = function(oldObject,newObject){
    var detailsJson = {
        details:[],
        properties:[
            {property:'message',enus:'Message',ptrbr:'Mensagem'},
            {property:'oldValue',enus:'Previous value',ptrbr:'Valor anterior'},
            {property:'newValue',enus:'New value',ptrbr:'Novo valor'}
        ],
        nfId:newObject.nfId,
        numItem: newObject.numItem
    };
    try{
        if(!oldObject || !newObject){
            return detailsJson;
        }
        var dictionary = {
            reason: {
                enus:"Reason",
                ptrbr:"Razão"
            },
            status: {
                enus: "Status",
                ptrbr: "Status"
            },
            oldValue: {
                enus: "Old Value",
                ptrbr: "Valor Antigo"
            },
            newValue: {
                enus: "New Value",
                ptrbr: "Novo Valor"
            },
            approvalType: {
                enus: "Approval Type",
                ptrbr: "Tipo de Approvação"
            },
            pendingJob: {
                enus: "Pending Job",
                ptrbr: "Job Pendente"
            },
            comment:{
                enus: "Comment",
                ptrbr: "Comentário"
            },
            link:{
                enus: "Link",
                ptrbr: "Link"
            },
            value:{
                enus: "Value",
                ptrbr: "Valor"
            },
            accountingDocument: {
                enus: "Accounting Document",
                ptrbr: "Doc Contábil"
            },
            reversedDocument: {
                enus: "Reversed Document",
                ptrbr: "Doc Estornado"
            }
        };
        var plainPropertiesToCompare = ["reason","status","justification","pendingJob","accountingDocument","reversedDocument"];
        var yesNoMap = {
            0: {
                enus: "No",
                ptrbr: "Não"
            },
            1: {
                enus: "Yes",
                ptrbr: "Sim"
            },
            null: {
                enus: "No",
                ptrbr: "Não"
            }
        };
        var statusMap = {
            PENDING:{
                enus: "Pending",
                ptrbr: "Pendente"
            },
            CORRECTED:{
                enus: "Corrected",
                ptrbr: "Corrigido"
            },
		    REJECTED:{
		        enus: "Rejected",
                ptrbr: "Rejeitado"
		    },
		    ERROR:{
		        enus: "Error",
                ptrbr: "Erro"
		    },
		    "IN PROGRESS":{
		        enus: "In Progress",
                ptrbr: "Em andamento"
		    }
        };
        //compare plain properties
        $.lodash.forEach(plainPropertiesToCompare,function(prop){
            if(prop === "pendingJob" && [0,null].indexOf(oldObject[prop]) !== -1 && [0,null].indexOf(newObject[prop]) !== -1){
                return;
            }
            if(newObject[prop] !== undefined && oldObject[prop] != newObject[prop]){//is different
                var detail = {
                    message:{
                        enus:'The field ' + dictionary[prop].enus + ' changed',
                        ptrbr:'O campo ' + dictionary[prop].ptrbr + ' alterou'
                    },
                    oldValue:{
                        enus:oldObject[prop],
                        ptrbr:oldObject[prop]
                    },
                    newValue:{
                        enus:newObject[prop],
                        ptrbr:newObject[prop]
                    }
                };
                switch(prop){
                    case "pendingJob":{
                        if(yesNoMap[oldObject[prop]]){
                            detail.oldValue = {
                                ptrbr: yesNoMap[oldObject[prop]].ptrbr,
                                enus: yesNoMap[oldObject[prop]].enus
                            };
                        }
                        if(yesNoMap[newObject[prop]]){
                            detail.newValue = {
                                ptrbr: yesNoMap[newObject[prop]].ptrbr,
                                enus: yesNoMap[newObject[prop]].enus
                            };
                        }
                        break;
                    }
                    case "status":{
                        detail.oldValue = {
                            ptrbr: statusMap[oldObject[prop]].ptrbr,
                            enus: statusMap[oldObject[prop]].enus
                        };
                        detail.newValue = {
                            ptrbr: statusMap[newObject[prop]].ptrbr,
                            enus: statusMap[newObject[prop]].enus
                        };
                        break;
                    }
                }
                detailsJson.details.push(detail);
            }
        });
        //compare fields
        var processedFields = this.compareFields(oldObject.fields,newObject.fields);
        $.lodash.forEach(processedFields.updatedFields,function(element){
            detailsJson.details.push({
                message:{
                    enus:'The ' + dictionary[element.prop].enus + ' from field ' + element.fieldName + ' changed',
                    ptrbr:'O ' + dictionary[element.prop].ptrbr + ' do campo ' + element.fieldName + ' alterou'
                },
                oldValue:{
                    enus:element.oldValue,
                    ptrbr:element.oldValue
                },
                newValue:{
                    enus:element.newValue,
                    ptrbr:element.newValue
                }
            });
        });
        //compare comments
        var processedComments = this.compareComments(oldObject.comments,newObject.comments);
        $.lodash.forEach(processedComments.updatedComments,function(comment){
            detailsJson.details.push({
                message:{
                    enus:'The field ' + dictionary.comment.enus + ' changed',
                    ptrbr:'O campo ' + dictionary.comment.ptrbr + ' alterou'
                },
                oldValue:{
                    enus:comment.oldValue,
                    ptrbr:comment.oldValue
                },
                newValue:{
                    enus:comment.newValue,
                    ptrbr:comment.newValue
                }
            });
        });
        //compare links
        var processedLinks = this.compareLinks(oldObject.links,newObject.links);
        $.lodash.forEach(processedLinks.updatedLinks,function(link){
            detailsJson.details.push({
                message:{
                    enus:'The field ' + dictionary.link.enus + ' changed',
                    ptrbr:'O campo ' + dictionary.link.ptrbr + ' alterou'
                },
                oldValue:{
                    enus:link.oldValue,
                    ptrbr:link.oldValue
                },
                newValue:{
                    enus:link.newValue,
                    ptrbr:link.newValue
                }
            });
        });
        //compare header fields
        var processedHeaderFields = this.compareHeaderFields(oldObject.headerFields,newObject.headerFields);
        $.lodash.forEach(processedHeaderFields.updatedFields,function(element){
            detailsJson.details.push({
                message:{
                    enus:'The ' + dictionary[element.prop].enus + ' from header field ' + element.fieldName + ' changed',
                    ptrbr:'O ' + dictionary[element.prop].ptrbr + ' do campo de cabeçalho ' + element.fieldName + ' alterou'
                },
                oldValue:{
                    enus:element.oldValue,
                    ptrbr:element.oldValue
                },
                newValue:{
                    enus:element.newValue,
                    ptrbr:element.newValue
                }
            });
        });
        //compare item fields
        var processedItemFields = this.compareItemFields(oldObject.itemFields,newObject.itemFields);
        $.lodash.forEach(processedItemFields.updatedFields,function(element){
            detailsJson.details.push({
                message:{
                    enus:'The ' + dictionary[element.prop].enus + ' from the item field ' + element.fieldName + ' changed',
                    ptrbr:'O ' + dictionary[element.prop].ptrbr + ' do campo de item ' + element.fieldName + ' alterou'
                },
                oldValue:{
                    enus:element.oldValue,
                    ptrbr:element.oldValue
                },
                newValue:{
                    enus:element.newValue,
                    ptrbr:element.newValue
                }
            });
        });
    }catch(e){
        detailsJson.error = e;
    }
    return detailsJson;
};
Supervisor.prototype.updateCorrection = function(oldObject,newObject){
    var json = this.compareCorrection(oldObject,newObject);
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207001',
        objectId: oldObject.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.deleteCorrection = function(correctionId){
    var json = {
        details:[],
        properties:[{
            property:'id',
            enus:"ID",
            ptrbr:"ID"
        }]
    };
    if(correctionId){
        json.details.push({
            id:{
                enus:correctionId,
                ptrbr:correctionId
            }
        });
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207002',
        objectId: correctionId,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.getCorrectionDetails = function(correction){
    var json = {
        details:[],
        properties:[{
            property:'reason',
            enus:"Reason",
            ptrbr:"Razão de Correção"
        },{
            property: 'justification',
            enus:"Justification",
            ptrbr:"Justificativa"
        }],
        nfId:correction.nfId,
        numItem:correction.numItem
    };
    if(correction){
        json.details.push({
            reason:{
                ptrbr: correction.reason,
                enus: correction.reason
            },
            justification:{
                ptrbr: correction.justification,
                enus: correction.justification
            }
        });
    }
    return json;
};
Supervisor.prototype.rejectCorrection = function(correction){
    var json = {
        details:[],
        properties:[]
    };
    if(correction){
        json = this.getCorrectionDetails(correction);
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207003',
        objectId: correction.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.approveCorrection = function(correction){
    var json = {
        details:[],
        properties:[]
    };
    if(correction){
        json = this.getCorrectionDetails(correction);
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207004',
        objectId: correction.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.accountingCorrection = function(correction){
    var json = {
        details:[],
        properties:[]
    };
    if(correction){
        json = this.getCorrectionDetails(correction);
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207028',
        objectId: correction.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.accountingCorrectionError = function(correction){
    var json = {
        details:[],
        properties:[]
    };
    if(correction){
        json = this.getCorrectionDetails(correction);
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207028',
        objectId: correction.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.approveCorrectionByShadow = function(correction){
    var json = {
        details:[],
        properties:[]
    };
    if(correction){
        json = this.getCorrectionDetails(correction);
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207024',
        objectId: correction.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.callCorrectionService = function(correctionId, sentXML, obtainedXML){
    var json = {
        details:[],
        properties:[
            {property:'service',enus:'Service',ptrbr:'Serviço'},
            {property:'sentXML',enus:'Sent XML',ptrbr:'XML Enviado'},
            {property:'obtainedXML',enus:'Obtained XML',ptrbr:'XML Obtido'}
        ]
    };
    if($.lodash.isString(sentXML) && $.lodash.isString(obtainedXML) ){
        json.details.push({
            service: {enus:'TMFNotaFiscalCorrectionsSyncIn',ptrbr:'TMFNotaFiscalCorrectionsSyncIn'},
            sentXML: sentXML.replace(/\</g,'< '),
            obtainedXML: obtainedXML.replace(/\</g,'< ')
        });
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207022',
        objectId: correctionId,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.callReversalService = function(correctionId, sentXML, obtainedXML){
    var json = {
        details:[],
        properties:[
            {property:'service',enus:'Service',ptrbr:'Serviço'},
            {property:'sentXML',enus:'Sent XML',ptrbr:'XML Enviado'},
            {property:'obtainedXML',enus:'Obtained XML',ptrbr:'XML Obtido'}
        ]
    };
    if($.lodash.isString(sentXML) && $.lodash.isString(obtainedXML) ){
        json.details.push({
            service: {enus:'TMFFIReversalPostSyncIn',ptrbr:'TMFFIReversalPostSyncIn'},
            sentXML: sentXML.replace(/\</g,'< '),
            obtainedXML: obtainedXML.replace(/\</g,'< ')
        });
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207025',
        objectId: correctionId,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.callCorrectionLetterCreation = function(correctionId, sentXML, obtainedXML){
    var json = {
        details:[],
        properties:[
            {property:'service',enus:'Service',ptrbr:'Serviço'},
            {property:'sentXML',enus:'Sent XML',ptrbr:'XML Enviado'},
            {property:'obtainedXML',enus:'Obtained XML',ptrbr:'XML Obtido'}
        ]
    };
    if($.lodash.isString(sentXML) && $.lodash.isString(obtainedXML) ){
        json.details.push({
            service: {enus:'TMFNFCorrectionLetterCreationSyncIn',ptrbr:'TMFNFCorrectionLetterCreationSyncIn'},
            sentXML: sentXML.replace(/\</g,'< '),
            obtainedXML: obtainedXML.replace(/\</g,'< ')
        });
    }
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207027',
        objectId: correctionId,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.callAccountingService = function(correctionId, sentXML, obtainedXML, isPeriodError){
    var json = {
        details:[],
        properties:[
            {property:'service',enus:'Service',ptrbr:'Serviço'},
            {property:'sentXML',enus:'Sent XML',ptrbr:'XML Enviado'},
            {property:'obtainedXML',enus:'Obtained XML',ptrbr:'XML Obtido'}
        ]
    };
    if($.lodash.isString(sentXML) && $.lodash.isString(obtainedXML) ){
        json.details.push({
            service: {enus:'TMFFIPostingPostSyncIn',ptrbr:'TMFFIPostingPostSyncIn'},
            sentXML: sentXML.replace(/\</g,'< '),
            obtainedXML: obtainedXML.replace(/\</g,'< ')
        });
    }
    var event = {
        componentName: 'BSC',
        messageCode: isPeriodError ? 'LOG207030' : 'LOG207026',
        objectId: correctionId,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.createCorrectionError = function(trace){
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207005',
        objectId: null,
        json:trace
    };
    LogError.createEvent(event);
};
Supervisor.prototype.updateCorrectionError = function(id,trace){
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207006',
        objectId: id,
        json:trace
    };
    LogError.createEvent(event);
};
Supervisor.prototype.deleteCorrectionError = function(id,trace){
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207007',
        objectId: id,
        json:trace
    };
    LogError.createEvent(event);
};
Supervisor.prototype.rejectCorrectionError = function(id,trace){
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207008',
        objectId: id,
        json:trace
    };
    LogError.createEvent(event);
};
Supervisor.prototype.approveCorrectionError = function(id, trace, code){
    var event = {
        componentName: 'BSC',
        messageCode: code || 'LOG207009',
        objectId: id,
        json:trace
    };
    LogError.createEvent(event);
};
Supervisor.prototype.callCorrectionServiceError = function(id,trace,sentXML,obtainedXML){
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207023',
        objectId: id,
        json:{trace:trace,sentXML:sentXML,obtainedXML:obtainedXML}
    };
    return LogError.createEvent(event);
};
/*-----------------------------END CORRECTIONS-----------------------------*/

Supervisor.prototype.createBlockConfiguration = function(blockConfiguration){
    var json = {};
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207010',
        objectId: blockConfiguration.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.updateBlockConfiguration = function(idConfiguration){
    var json = {};
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207011',
        objectId: idConfiguration,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.deleteBlockConfiguration = function(idConfiguration){
    var json = {};
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207012',
        objectId: idConfiguration,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.createShadowConfiguration = function(shadowConfiguration){
    var json = {};
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207013',
        objectId: shadowConfiguration.id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.updateShadowConfiguration = function(id){
    var json = {};
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207014',
        objectId: id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.deleteShadowConfiguration = function(id){
    var json = {};
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207015',
        objectId: id,
        json:json
    };
    return Log.createEvent(event);
};
Supervisor.prototype.createBlockConfigurationError = function(blockConfiguration,trace){
    var json = trace;
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207016',
        objectId: blockConfiguration.id,
        json:json
    };
    return LogError.createEvent(event);
};
Supervisor.prototype.updateBlockConfigurationError = function(idConfiguration,trace){
    var json = trace;
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207017',
        objectId: idConfiguration,
        json:json
    };
    return LogError.createEvent(event);
};
Supervisor.prototype.deleteBlockConfigurationError = function(idConfiguration,trace){
    var json = trace;
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207018',
        objectId: idConfiguration,
        json:json
    };
    return LogError.createEvent(event);
};
Supervisor.prototype.createShadowConfigurationError = function(shadowConfiguration,trace){
    var json = trace;
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207019',
        objectId: shadowConfiguration.id,
        json:json
    };
    return LogError.createEvent(event);
};
Supervisor.prototype.updateShadowConfigurationError = function(id,trace){
    var json = trace;
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207020',
        objectId: id,
        json:json
    };
    return LogError.createEvent(event);
};
Supervisor.prototype.deleteShadowConfigurationError = function(id,trace){
    var json = trace;
    var event = {
        componentName: 'BSC',
        messageCode: 'LOG207021',
        objectId: id,
        json:json
    };
    return LogError.createEvent(event);
};
this.Supervisor = new Supervisor();